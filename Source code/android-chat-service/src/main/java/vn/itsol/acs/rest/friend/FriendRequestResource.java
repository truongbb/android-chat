package vn.itsol.acs.rest.friend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import vn.itsol.acs.dto.friend.FriendRequestDto;
import vn.itsol.acs.service.friend.FriendRequestService;

@RestController
@RequestMapping("${spring.data.rest.base-path}/friend")
public class FriendRequestResource {

	@Autowired
	private FriendRequestService friendRequestService;

	@PostMapping("/all-request")
	public ResponseEntity<FriendRequestDto> getAllRequestOfAnUser(@RequestBody FriendRequestDto friendRequestDto) {
		FriendRequestDto requests = this.friendRequestService.getAllRequestOfAnUser(friendRequestDto);
		return new ResponseEntity<>(requests, HttpStatus.OK);
	}

	@PostMapping("/send-request")
	public ResponseEntity<Boolean> sendRequest(@RequestBody FriendRequestDto friendRequestDto) {
		boolean result = this.friendRequestService.sendRequest(friendRequestDto);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@PostMapping("/cancel-request")
	public ResponseEntity<Boolean> cancelRequest(@RequestBody FriendRequestDto friendRequestDto) {
		boolean result = this.friendRequestService.cancelRequest(friendRequestDto);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
}
