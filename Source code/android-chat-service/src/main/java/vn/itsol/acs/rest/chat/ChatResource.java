package vn.itsol.acs.rest.chat;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import vn.itsol.acs.dto.chat.ConversationDto;
import vn.itsol.acs.dto.chat.MessageDto;
import vn.itsol.acs.dto.chat.ConversationHistoryDto;
import vn.itsol.acs.dto.chat.CreateConversationDto;
import vn.itsol.acs.dto.user.UserDto;
import vn.itsol.acs.service.chat.ChatService;
import vn.itsol.acs.utils.FileUtils;

@RestController
@RequestMapping("${spring.data.rest.base-path}/chat")
public class ChatResource {

	@Autowired
	private ChatService chatService;

	@PostMapping("/create-conversation")
	public ResponseEntity<CreateConversationDto> createConversation(
			@RequestBody CreateConversationDto createConversationDto) {
		CreateConversationDto dto = this.chatService.createConversation(createConversationDto);
		if (dto == null) {
			dto = new CreateConversationDto();
			dto.setStatus(CreateConversationDto.ERROR);
		}
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@PostMapping("/delete-conversation")
	public ResponseEntity<Boolean> deleteConversation(@RequestBody ConversationDto conversationDto) {
		return new ResponseEntity<>(this.chatService.deleteConversation(conversationDto), HttpStatus.OK);
	}

	@PostMapping("/send-message")
	public ResponseEntity<Boolean> sendMessage(@RequestBody MessageDto message) {
		return new ResponseEntity<>(this.chatService.sendMessage(message), HttpStatus.OK);
	}

	@PostMapping("/conversation-history")
	public ResponseEntity<List<ConversationHistoryDto>> getConversationHistory(
			@RequestBody ConversationDto conversationDto) {
		return new ResponseEntity<>(this.chatService.getConversationHistory(conversationDto), HttpStatus.OK);
	}

	@PostMapping("/all-conversation")
	public ResponseEntity<List<ConversationDto>> getAllConversationOfUser(@RequestBody UserDto user) {
		return new ResponseEntity<>(this.chatService.getAllConversationOfUser(user), HttpStatus.OK);
	}

	@PostMapping("/send-file")
	public ResponseEntity<Integer> sendFiles(@RequestParam("uploadfiles") MultipartFile[] uploadfiles,
			@RequestParam("conversationId") Long conversationId, @RequestParam("senderId") Long senderId) {
		int sendFiles = this.chatService.sendFiles(Arrays.asList(uploadfiles), conversationId, senderId);
		if (sendFiles == FileUtils.UPLOAD_SUCESSFULLY) {
			return new ResponseEntity<>(FileUtils.UPLOAD_SUCESSFULLY, HttpStatus.OK);
		}
		if (sendFiles == FileUtils.UPLOAD_FAIL) {
			return new ResponseEntity<>(FileUtils.UPLOAD_FAIL, HttpStatus.OK);
		}
		return new ResponseEntity<>(FileUtils.EMPTY_FILE, HttpStatus.OK);
	}

	@PostMapping("/download-file")
	public ResponseEntity<InputStreamResource> downloadFile(@RequestBody MessageDto message) {
		File file = new File(new FileUtils().getFullPath() + message.getAttatchmenUrl());
		InputStreamResource resource;
		try {
			resource = new InputStreamResource(new FileInputStream(file));

			return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName())
					.contentLength(file.length()).contentType(MediaType.IMAGE_JPEG)
					.body(resource);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

}
