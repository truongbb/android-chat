package vn.itsol.acs.service.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import vn.itsol.acs.dto.user.UserDto;
import vn.itsol.acs.repository.user.UserRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private static final int REGISTER_SUCCESSFULLY = 0;
    private static final int EXISTED_ACCOUNT = 1;
    private static final int EXISTED_PHONE_OR_EMAIL = 2;
    private static final int INVALID_PASSWORD = 3;
    private static final int ERROR = 4;

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDto login(UserDto user) {
        // when logging in success -> set online state
        UserDto login = new UserDto();
        try {
            login = this.userRepository.login(user.getUsername(), user.getPassword());
            if (login != null && login.getUserId() != null) {
                login.setIsActive(true);
                boolean changeActiveState = this.userRepository.changeActiveState(login);
                if (changeActiveState) {
                    return login;
                } else {
                    throw new Exception();
                }
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return login;
    }

    @Override
    public boolean setOffline(UserDto user) {
        user.setLastOnlineTime(new Date());
        user.setIsActive(false);
        return this.userRepository.setOffline(user);
    }

    @Override
    public int register(UserDto user) {

        user.setUsername(user.getUsername().trim());
        try {
            user.setBirthday(new SimpleDateFormat("yyyy-MM-dd").parse(user.getBirthdayStr()));
        } catch (ParseException e1) {
            e1.printStackTrace();
            logger.error(e1.getMessage());
            return ERROR;
        }

        // <editor-fold desc="validate">
        UserDto userVM = new UserDto();
        userVM.setPhone(user.getPhone());
        userVM.setEmail(user.getEmail());
        List<UserDto> users = this.userRepository.findByEmailOrPhone(userVM);
        if (users != null && users.size() > 0) {
            return EXISTED_PHONE_OR_EMAIL;
        }

        UserDto accountVM = new UserDto();
        accountVM.setUsername(user.getUsername());
        List<UserDto> accounts = this.userRepository.doSearch(accountVM);
        if (accounts != null && accounts.size() > 0) {
            return EXISTED_ACCOUNT;
        }

        if (user.getPassword().length() < 8 || user.getPassword().length() > 16) {
            return INVALID_PASSWORD;
        }
        // <editor-fold>

        // <editor-fold desc="insert into account and users table">
        try {
            boolean insertIntoAccount = this.userRepository.insertIntoAccount(user);
            if (insertIntoAccount) {
                UserDto userDto = this.userRepository.login(user.getUsername(), user.getPassword());
                if (userDto != null) {
                    user.setAccountId(userDto.getAccountId());
                    boolean insertIntoUser = this.userRepository.insertIntoUser(user);
                    if (insertIntoUser) {
                        return REGISTER_SUCCESSFULLY;
                    } else {
                        throw new Exception();
                    }
                } else {
                    throw new Exception();
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        // <editor-fold>
        return ERROR;
    }

    @Override
    public boolean updateAccountInfor(UserDto user) {
        if (user != null) {
            return this.userRepository.updateAccountInfor(user);
        }
        return false;
    }

    @Override
    public boolean updateUserInfor(UserDto user) {
        if (user != null) {
            return this.userRepository.updateUserInfor(user);
        }
        return false;
    }

    @Override
    public boolean delete(UserDto user) {
        if (user != null) {
            return this.userRepository.delete(user);
        }
        return false;
    }

    @Override
    public List<UserDto> doSearch(UserDto user) {
        return this.userRepository.doSearch(user);
    }

    @Override
    public List<UserDto> searchByName(UserDto user) {
        return user != null ? this.userRepository.searchByName(user.getFirstName(), user.getUserId()) : null;
    }
}
