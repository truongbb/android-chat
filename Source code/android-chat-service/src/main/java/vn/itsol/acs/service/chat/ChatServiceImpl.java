package vn.itsol.acs.service.chat;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import vn.itsol.acs.dto.chat.ConversationDto;
import vn.itsol.acs.dto.chat.MessageDto;
import vn.itsol.acs.dto.chat.ConversationHistoryDto;
import vn.itsol.acs.dto.chat.CreateConversationDto;
import vn.itsol.acs.dto.chat.ParticipantDto;
import vn.itsol.acs.dto.user.UserDto;
import vn.itsol.acs.repository.chat.ChatRepository;
import vn.itsol.acs.repository.chat.conversation.ConversationRepository;
import vn.itsol.acs.repository.chat.message.MessageRepository;
import vn.itsol.acs.repository.chat.participant.ParticipantRepository;
import vn.itsol.acs.utils.FileUtils;

@Service
@Transactional
public class ChatServiceImpl implements ChatService {

    private static final Logger logger = LoggerFactory.getLogger(ChatServiceImpl.class);

    @Autowired
    private ConversationRepository conversationRepository;

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private ChatRepository chatRepository;

    @Override
    public CreateConversationDto createConversation(CreateConversationDto createConversationDto) {
        Long userId = createConversationDto.getCreatorId();
        List<String> participantIds = createConversationDto.getParticipants().stream()
                .sorted(Comparator.comparingLong(ParticipantDto::getUserId)).map(t -> t.getUserId() + "").collect(Collectors.toList());
        String userList = String.join(",", participantIds);
        List<CreateConversationDto> tempDtos = this.conversationRepository.searchWhenCreateConversation(userId.intValue(), userList);

        CreateConversationDto tempDto = null;
        if (tempDtos != null && !tempDtos.isEmpty()) {
            if (tempDtos.size() == 1) {
                tempDto = tempDtos.get(0);
            } else {
                tempDtos = tempDtos.stream().sorted(Comparator.comparing(CreateConversationDto::getCreatorId)).collect(Collectors.toList());
                String title = tempDtos.stream().map(t -> t.getTitle()).collect(Collectors.joining(", "));
                tempDto = new CreateConversationDto();
                tempDto.setConversationId(tempDtos.get(0).getConversationId());
                tempDto.setTitle(title);
                tempDto.setChatTypeId(tempDtos.get(0).getChatTypeId());
            }
        }

        if (tempDto != null) {
            ConversationDto dto = new ConversationDto();
            dto.setId(Long.valueOf(tempDto.getConversationId()));
            List<ConversationHistoryDto> conversationHistory = getConversationHistory(dto);
            createConversationDto.setConversationId(Long.valueOf(tempDto.getConversationId()));
            createConversationDto.setTitle(tempDto.getTitle());
            createConversationDto.setChatTypeId(tempDto.getChatTypeId());
            createConversationDto.setConversationHistory(conversationHistory);
            createConversationDto.setStatus(CreateConversationDto.EXSITED);
        } else {
            ConversationDto dto = new ConversationDto();
            dto.setCreatorId(createConversationDto.getCreatorId());
            dto.setTitle(createConversationDto.getTitle() == null ? null : createConversationDto.getTitle());
            Date now = new Date();
            dto.setCreatedTime(now);
            dto.setUpdatedTime(now);
            createConversationDto.setParticipants(createConversationDto.getParticipants().stream().map(t -> {
                t.setChatTypeId(createConversationDto.getParticipants().size() > 1 ? 2L : 1L);
                return t;
            }).collect(Collectors.toList()));
            boolean creationResult = createNewConversation(dto, createConversationDto.getParticipants());
            if (creationResult) {
                List<ConversationDto> conversations = this.conversationRepository.doSearch(dto);
                if (conversations != null && conversations.size() == 1) {
                    createConversationDto.setConversationId(conversations.get(0).getId());
                    createConversationDto.setChatTypeId(createConversationDto.getParticipants().size() == 1 ? 1L : 2L);
                    createConversationDto.setStatus(CreateConversationDto.NEW_OK);
                } else {
                    createConversationDto.setStatus(CreateConversationDto.NEW_NOT_OK);
                }
            } else {
                createConversationDto.setStatus(CreateConversationDto.NEW_NOT_OK);
            }
        }
        return createConversationDto;
    }

    @Override
    public boolean createNewConversation(ConversationDto conversationDto, List<ParticipantDto> participantDtos) {
        try {
            boolean insertConversation = this.conversationRepository.insert(conversationDto);
            if (insertConversation) {
                List<ConversationDto> conversations = this.conversationRepository.doSearch(conversationDto);
                if (conversations != null && conversations.size() == 1) {
                    Long conversationId = conversations.get(0).getId();
                    for (ParticipantDto participantDto : participantDtos) {
                        participantDto.setConversationId(conversationId);
                        boolean insertParticipant = this.participantRepository.insert(participantDto);
                        if (!insertParticipant) {
                            throw new Exception();
                        }
                    }
                    return true;
                } else {
                    throw new Exception();
                }
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return false;
    }

    @Override
    public boolean deleteConversation(ConversationDto conversationDto) {
        return this.conversationRepository.delete(conversationDto.getId());
    }

    @Override
    public boolean sendMessage(MessageDto message) {
        // insert into message table and update updatedTime at conversation table
        try {
            Date sentTime = new Date(message.getSentTimeLong());
            message.setSentTime(sentTime);
            ConversationDto conversationDto = new ConversationDto();
            conversationDto.setId(message.getConversationId());
            boolean insert = this.messageRepository.insert(message);
            if (insert) {
                List<ConversationDto> conversations = this.conversationRepository.doSearch(conversationDto);
                if (conversations != null && conversations.size() == 1) {
                    conversationDto = conversations.get(0);
                    conversationDto.setUpdatedTime(sentTime);
                    boolean update = this.conversationRepository.update(conversationDto);
                    if (update) {
                        return true;
                    } else {
                        throw new Exception();
                    }
                } else {
                    throw new Exception();
                }
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return false;
    }

    @Override
    public List<ConversationHistoryDto> getConversationHistory(ConversationDto conversationDto) {
        if (conversationDto != null) {
            List<ConversationHistoryDto> historyOfPairConversations = this.chatRepository
                    .getHistoryOfPairConversation(conversationDto.getId());

            // <editor-fold desc="cut attachtment url to get file name">
            historyOfPairConversations = historyOfPairConversations.stream().map(t -> {
                t.setSentTimeLong(t.getSentTime().getTime());
                if (t.getMessageTypeId() == MessageDto.FILE) {
                    String[] urls = t.getAttachtmentUrl().split("/");
                    t.setFileName(urls[urls.length - 1]);
                }
                return t;
            }).collect(Collectors.toList());
            // </editor-fold>
            return historyOfPairConversations;
        }
        return null;
    }

    @Override
    public List<ConversationDto> getAllConversationOfUser(UserDto user) {
        if (user != null) {
            List<ConversationDto> allConversationOfUser = this.chatRepository
                    .getAllConversationOfUser(user.getUserId());
            // <editor-fold desc="cut attachment url to get file name">
            allConversationOfUser = allConversationOfUser.stream().map(t -> {
                t.setUpdatedTimeLong(t.getUpdatedTime().getTime());
                if (t.getLastAttachment() != null) {
                    String[] urls = t.getLastAttachment().split("/");
                    t.setFileName(urls[urls.length - 1]);
                }
                return t;
            }).collect(Collectors.toList());
            // </editor-fold>
            return allConversationOfUser;
        }
        return null;
    }

    @Override
    public int sendFiles(List<MultipartFile> uploadfiles, Long conversationId, Long senderId) {
        // send file to server and insert into message
        String uploadedFileName = uploadfiles.stream().map(t -> t.getOriginalFilename())
                .filter(t -> !StringUtils.isEmpty(t)).collect(Collectors.joining(", "));
        if (StringUtils.isEmpty(uploadedFileName)) {
            return FileUtils.EMPTY_FILE;
        }
        try {
            FileUtils fileUtils = new FileUtils();
            fileUtils.saveUploadedFiles(uploadfiles);
            // when send file to server success -> insert into message table
            MessageDto message = new MessageDto();
            message.setConversationId(conversationId);
            message.setSenderId(senderId);
            message.setMessageTypeId(MessageDto.FILE);
            message.setAttatchmenUrl(new FileUtils().getFullPath() + uploadedFileName);
            message.setSentTimeLong(new Date().getTime());
            boolean sendMessage = sendMessage(message);
            if (sendMessage) {
                return FileUtils.UPLOAD_SUCESSFULLY;
            } else {
                throw new IOException();
            }
        } catch (IOException e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return FileUtils.UPLOAD_FAIL;
    }

    @Override
    public byte[] downloadFile(MessageDto message) {
        try {
            String path = new FileUtils().getFullPath();
            String fullPath = path + message.getAttatchmenUrl();
            message.setAttatchmenUrl(fullPath); // file name để tạm trong attachmentUrl
            MessageDto messageDto = this.messageRepository.findFile(message);
            if (messageDto != null) {
                byte[] contents = IOUtils.toByteArray(new FileInputStream(fullPath));
                return contents;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
