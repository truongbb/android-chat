package vn.itsol.acs.repository.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import vn.itsol.acs.dto.user.UserDto;
import vn.itsol.acs.repository.BaseRepository;
import vn.itsol.acs.utils.SQLBuilder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class UserRepositoryImpl extends BaseRepository implements UserRepository {

    private final Logger logger = LoggerFactory.getLogger(UserRepositoryImpl.class);

    @Override
    public UserDto login(String username, String password) {
        UserDto account = new UserDto();
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_USERS, "login");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("p_username", username);
            parameters.put("p_password", password);
            account = getNamedParameterJdbcTemplate().queryForObject(sql, parameters, new AccountMapper());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return account;
    }

    @Override
    public boolean changeActiveState(UserDto user) {
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_USERS, "change_active_state");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("p_is_active", user.getIsActive());
            parameters.put("p_account_id", user.getAccountId());
            return getNamedParameterJdbcTemplate().update(sql, parameters) > 0;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    @Override
    public boolean setOffline(UserDto user) {
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_USERS, "set_offline");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("p_is_active", user.getIsActive());
            parameters.put("p_last_online_time",
                    new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(user.getLastOnlineTime()));
            parameters.put("p_account_id", user.getAccountId());
            return getNamedParameterJdbcTemplate().update(sql, parameters) > 0;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    @Override
    public List<UserDto> findByEmailOrPhone(UserDto user) {
        List<UserDto> users = null;
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_USERS, "find_by_email_or_phone");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("p_email", user.getEmail());
            parameters.put("p_phone", user.getPhone());
            users = getNamedParameterJdbcTemplate().query(sql, parameters, new AccountMapper());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return users;
    }

    @Override
    public boolean insertIntoAccount(UserDto user) {
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_USERS, "insert_into_account");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("p_username", user.getUsername());
            parameters.put("p_password", user.getPassword());
            parameters.put("p_is_active", user.getIsActive());
            if (user.getLastOnlineTime() != null) {
                parameters.put("p_last_online_time", new SimpleDateFormat("yyyyMMdd").format(user.getLastOnlineTime()));
            } else {
                parameters.put("p_last_online_time", null);
            }
            return getNamedParameterJdbcTemplate().update(sql, parameters) > 0;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    @Override
    public boolean insertIntoUser(UserDto user) {
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_USERS, "insert_into_user");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("p_first_name", user.getFirstName());
            parameters.put("p_last_name", user.getLastName());
            parameters.put("p_address", user.getAddress());
            parameters.put("p_email", user.getEmail());
            parameters.put("p_phone", user.getPhone());
            if (user.getGender().trim().equals("Nam")) {
                parameters.put("p_gender_id", UserDto.MALE);
            } else if (user.getGender().trim().equals("Nữ")) {
                parameters.put("p_gender_id", UserDto.FEMALE);
            } else {
                parameters.put("p_gender_id", UserDto.UNKNOWN);
            }
            parameters.put("p_birthday", new SimpleDateFormat("yyyyMMdd").format(user.getBirthday()));
            parameters.put("p_account_id", user.getAccountId());
            return getNamedParameterJdbcTemplate().update(sql, parameters) > 0;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    @Override
    public boolean updateAccountInfor(UserDto user) {
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_USERS, "update_account_infor");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("p_username", user.getUsername());
            parameters.put("p_password", user.getPassword());
            parameters.put("p_account_id", user.getAccountId());
            return getNamedParameterJdbcTemplate().update(sql, parameters) > 0;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    @Override
    public boolean updateUserInfor(UserDto user) {
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_USERS, "update_user_infor");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("p_first_name", user.getFirstName());
            parameters.put("p_last_name", user.getLastName());
            parameters.put("p_address", user.getAddress());
            parameters.put("p_email", user.getEmail());
            parameters.put("p_phone", user.getPhone());
            if (user.getGenderId() == null) {
                if ("Nam".equals(user.getGender().trim())) {
                    parameters.put("p_gender_id", UserDto.MALE);
                } else if ("Nữ".equals(user.getGender().trim())) {
                    parameters.put("p_gender_id", UserDto.FEMALE);
                } else {
                    parameters.put("p_gender_id", UserDto.UNKNOWN);
                }
            } else {
                parameters.put("p_gender_id", user.getGenderId());
            }
            if (user.getBirthday() != null) {
                parameters.put("p_birthday", new SimpleDateFormat("YYYYMMDD").format(user.getBirthday()));
            } else {
                parameters.put("p_birthday", new SimpleDateFormat("YYYYMMDD")
                        .format(new SimpleDateFormat("dd-MM-yyyy").parse(user.getBirthdayStr().trim())));
            }
            parameters.put("p_user_id", user.getUserId());
            return getNamedParameterJdbcTemplate().update(sql, parameters) > 0;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    @Override
    public boolean delete(UserDto user) {
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_USERS, "delete");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("p_account_id", user.getAccountId());
            return getNamedParameterJdbcTemplate().update(sql, parameters) > 0;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    @Override
    public List<UserDto> doSearch(UserDto user) {
        List<UserDto> lstResult = new ArrayList<>();
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_USERS, "do_search");
            Map<String, Object> parameters = new HashMap<>();
            if (user.getFirstName() != null) {
                sql += "\n and u.first_name like '%' || :p_first_name || '%'";
                parameters.put("p_first_name", user.getFirstName());
            }
            if (user.getLastName() != null) {
                sql += "\n and u.last_name like '%' || :p_last_name || '%'";
                parameters.put("p_last_name", user.getLastName());
            }
            if (user.getAddress() != null) {
                sql += "\n and u.address like '%' || :p_address || '%'";
                parameters.put("p_address", user.getAddress());
            }
            if (user.getEmail() != null) {
                sql += "\n and u.email like '%' || :p_email || '%'";
                parameters.put("p_email", user.getEmail());
            }
            if (user.getEmail() != null) {
                sql += "\n and u.phone like '%' || :p_phone || '%'";
                parameters.put("p_phone", user.getEmail());
            }
            if (user.getGender() != null) {
                sql += "\n and g.gender like '%' || :p_gender || '%'";
                parameters.put("p_gender", user.getGender());
            }
            if (user.getBirthday() != null) {
                sql += "\n and to_char(u.birthday, 'YYYYMMDD') = :p_birthday";
                parameters.put("p_birthday", new SimpleDateFormat("yyyyMMdd").format(user.getBirthday()));
            }
            if (user.getUsername() != null) {
                sql += "\n and a.username like '%' || :p_username || '%'";
                parameters.put("p_username", user.getUsername());
            }
            if (user.getPassword() != null) {
                sql += "\n and a.password like '%' || :p_password || '%'";
                parameters.put("p_password", user.getPassword());
            }
            if (user.getIsActive() != null) {
                sql += "\n and a.is_active = :p_is_active";
                if (user.getIsActive()) {
                    parameters.put("p_is_active", 1);
                } else {
                    parameters.put("p_is_active", 0);
                }
            }
            if (user.getLastOnlineTime() != null) {
                sql += "\n and to_char(a.last_online_time, 'YYYYMMDD') = :p_last_online_time";
                parameters.put("p_last_online_time", new SimpleDateFormat("yyyyMMdd").format(user.getLastOnlineTime()));
            }
            sql += "\n order by u.id";
            lstResult = getNamedParameterJdbcTemplate().query(sql, parameters, new AccountMapper());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return lstResult;
    }

    @Override
    public List<UserDto> searchByName(String name, Long userId) {
        List<UserDto> lstResult = new ArrayList<>();
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_USERS, "search_by_name");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("p_name", name);
            parameters.put("p_user_id", userId.intValue());
            lstResult = getNamedParameterJdbcTemplate().query(sql, parameters, (rs, rowNum) -> {
                UserDto user = new UserDto();
                user.setAccountId(rs.getLong("account_id"));
                user.setUsername(rs.getString("username"));
                user.setIsActive(rs.getBoolean("is_active"));
                user.setLastOnlineTime(rs.getDate("last_online_time"));
                user.setUserId(rs.getLong("user_id"));
                user.setFirstName(rs.getString("first_name"));
                user.setLastName(rs.getString("last_name"));
                user.setAddress(rs.getString("address"));
                user.setEmail(rs.getString("email"));
                user.setPhone(rs.getString("phone"));
                user.setGenderId(rs.getLong("gender_id"));
                user.setGender(rs.getString("gender"));
                user.setBirthday(rs.getDate("birthday"));
                user.setIsContact(rs.getLong("is_contact"));
                user.setIsSent(rs.getLong("is_sent"));
                user.setIsSender(rs.getLong("is_sender"));
                return user;
            });
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return lstResult;
    }

    // <editor-fold desc="mapper classes">
    class AccountMapper implements RowMapper<UserDto> {
        @Override
        public UserDto mapRow(ResultSet rs, int i) throws SQLException {
            UserDto user = new UserDto();
            user.setAccountId(rs.getLong("account_id"));
            user.setUsername(rs.getString("username"));
            user.setPassword(rs.getString("password"));
            user.setIsActive(rs.getBoolean("is_active"));
            user.setLastOnlineTime(rs.getDate("last_online_time"));
            user.setUserId(rs.getLong("user_id"));
            user.setFirstName(rs.getString("first_name"));
            user.setLastName(rs.getString("last_name"));
            user.setAddress(rs.getString("address"));
            user.setEmail(rs.getString("email"));
            user.setPhone(rs.getString("phone"));
            user.setGenderId(rs.getLong("gender_id"));
            user.setGender(rs.getString("gender"));
            user.setBirthday(rs.getDate("birthday"));
            return user;
        }
    }
    // </editor-fold>
}
