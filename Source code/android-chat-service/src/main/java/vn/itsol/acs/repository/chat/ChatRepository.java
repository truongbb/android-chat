package vn.itsol.acs.repository.chat;

import java.util.List;

import org.springframework.stereotype.Repository;

import vn.itsol.acs.dto.chat.ConversationDto;
import vn.itsol.acs.dto.chat.ConversationHistoryDto;

@Repository
public interface ChatRepository {

	List<ConversationHistoryDto> getHistoryOfPairConversation(Long conversationId);

	List<ConversationDto> getAllConversationOfUser(Long userId);

}
