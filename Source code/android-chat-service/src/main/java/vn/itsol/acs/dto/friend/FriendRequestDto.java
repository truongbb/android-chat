package vn.itsol.acs.dto.friend;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import vn.itsol.acs.dto.user.UserDto;

@Getter
@Setter
public class FriendRequestDto {

	private Long userId;
	private Long accountId;
	private String username;
	private Long senderId;
	private String requestedTime;

	// <editor-fold desc="sub-fields">
	private String requestedListString;
	private List<UserDto> requestedList;
	// </editor-fold>

}
