package vn.itsol.acs.repository.chat.message;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import vn.itsol.acs.dto.chat.MessageDto;
import vn.itsol.acs.repository.BaseRepository;
import vn.itsol.acs.utils.SQLBuilder;

@Repository
@Transactional
public class MessageRepositoryImpl extends BaseRepository implements MessageRepository {

	private static final Logger logger = LoggerFactory.getLogger(MessageRepositoryImpl.class);

	@Override
	public boolean insert(MessageDto message) {
		try {
			String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_CHAT_MESSAGE, "insert");
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("p_conversation_id", message.getConversationId());
			parameters.put("p_sender_id", message.getSenderId());
			parameters.put("p_message_type_id", message.getMessageTypeId());
			parameters.put("p_message_content", message.getMessageContent());
			parameters.put("p_attachtment_url", message.getAttatchmenUrl());
			parameters.put("p_sent_time", new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(message.getSentTime()));
			return getNamedParameterJdbcTemplate().update(sql, parameters) > 0;
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return false;
	}

	@Override
	public MessageDto findFile(MessageDto message) {
		try {
			String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_CHAT_MESSAGE, "find_file");
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("p_conversation_id", message.getConversationId());
			parameters.put("p_attachment_url", message.getAttatchmenUrl());
			Date sentTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(message.getSentTimeString());
			parameters.put("p_sent_time", new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(sentTime));
			MessageDto messageDto = getNamedParameterJdbcTemplate().queryForObject(sql, parameters,
					(resultSet, rownum) -> {
						MessageDto dto = new MessageDto();
						dto.setId(resultSet.getLong("id"));
						dto.setConversationId(resultSet.getLong("conversation_id"));
						dto.setSenderId(resultSet.getLong("sender_id"));
						dto.setMessageTypeId(resultSet.getLong("message_type_id"));
						dto.setMessageContent(resultSet.getString("message_content"));
						dto.setAttatchmenUrl(resultSet.getString("attachment_url"));
						dto.setSentTime(resultSet.getDate("sent_time"));
						return dto;
					});
			return messageDto;
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}
}
