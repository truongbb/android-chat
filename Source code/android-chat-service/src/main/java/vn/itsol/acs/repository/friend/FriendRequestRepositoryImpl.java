package vn.itsol.acs.repository.friend;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import vn.itsol.acs.dto.friend.FriendRequestDto;
import vn.itsol.acs.repository.BaseRepository;
import vn.itsol.acs.utils.SQLBuilder;

@Repository
@Transactional
public class FriendRequestRepositoryImpl extends BaseRepository implements FriendRequestRepository {

    private static final Logger logger = LoggerFactory.getLogger(FriendRequestRepositoryImpl.class);

    @Override
    public FriendRequestDto getAllRequestOfAnUser(Long userId) {
        FriendRequestDto requestOfAnUserDto = null;
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_FRIEND_REQUEST, "get_all_request_of_an_user");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("p_user_id", userId);
            requestOfAnUserDto = getNamedParameterJdbcTemplate().queryForObject(sql, parameters,
                    (resultSet, numRows) -> {
                        FriendRequestDto request = new FriendRequestDto();
                        request.setUserId(resultSet.getLong("user_id"));
                        request.setAccountId(resultSet.getLong("account_id"));
                        request.setUsername(resultSet.getString("username"));
                        request.setRequestedListString(resultSet.getString("requested_infor"));
                        return request;
                    });
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return requestOfAnUserDto;
    }

    @Override
    public boolean insertIntoFriendRequest(Long senderId, String requestedTime) {
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_FRIEND_REQUEST, "insert_into_friend_request");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("p_sender_id", senderId);
            parameters.put("p_requested_time", requestedTime);
            return getNamedParameterJdbcTemplate().update(sql, parameters) > 0;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    @Override
    public boolean insertIntoUsersFriendRequest(Long userId, Long friendRequestId) {
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_FRIEND_REQUEST, "insert_into_users_friend_request");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("p_user_id", userId);
            parameters.put("p_friend_request_id", friendRequestId);
            return getNamedParameterJdbcTemplate().update(sql, parameters) > 0;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    @Override
    public Long getRequestId(Long senderId, Long receiverId) {
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_FRIEND_REQUEST, "get_friend_request_id_by_id");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("p_sender_id", senderId);
            parameters.put("p_receiver_id", receiverId);
            // dùng tạm biến userId do không muốn tạo thêm thuộc tính
            FriendRequestDto dto = getNamedParameterJdbcTemplate().queryForObject(sql, parameters,
                    (resultSet, numRows) -> {
                        FriendRequestDto request = new FriendRequestDto();
                        request.setUserId(resultSet.getLong("friend_request_id"));
                        return request;
                    });
            return dto != null ? dto.getUserId() : null;
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Long getRequestId(Long senderId, String requestedTime) {
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_FRIEND_REQUEST, "get_friend_request_id_by_time");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("p_sender_id", senderId);
            parameters.put("p_requested_time", requestedTime);
            // dùng tạm biến userId do không muốn tạo thêm thuộc tính
            FriendRequestDto dto = getNamedParameterJdbcTemplate().queryForObject(sql, parameters,
                    (resultSet, numRows) -> {
                        FriendRequestDto request = new FriendRequestDto();
                        request.setUserId(resultSet.getLong("friend_request_id"));
                        return request;
                    });
            return dto != null ? dto.getUserId() : null;
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteRequest(Long friendRequestId) {
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_FRIEND_REQUEST, "delete");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("p_friend_request_id", friendRequestId);
            return getNamedParameterJdbcTemplate().update(sql, parameters) > 0;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return false;
    }
}
