package vn.itsol.acs.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class SQLBuilder {

	public static final Logger logger = LoggerFactory.getLogger(SQLBuilder.class);

	// <code-fold desc="sql modules">
	public static final String SQL_MODULE_USERS = "users";
	public static final String SQL_MODULE_FRIEND_REQUEST = "friend";
	public static final String SQL_MODULE_CONTACT = "contact";
	public static final String SQL_MODULE_CHAT = "chat";
	public static final String SQL_MODULE_CHAT_CONVERSATION = "chat/conversation";
	public static final String SQL_MODULE_CHAT_PARTICIPANT = "chat/participant";
	public static final String SQL_MODULE_CHAT_MESSAGE = "chat/message";
	// </code-fold>

	public static String getSqlQueryById(String module, String queryId) {
		File folder = null;
		try {
			folder = new ClassPathResource("sql" + File.separator + module + File.separator + queryId + ".sql")
					.getFile();
			// Read file
			if (folder.isFile()) {
				String sql = new String(Files.readAllBytes(Paths.get(folder.getAbsolutePath())));
				return sql;
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return null;
	}
}
