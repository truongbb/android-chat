package vn.itsol.acs.dto.chat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParticipantDto {

	public static final int PAIR = 1;
	public static final int GROUP = 2;

	private Long id;
	private Long conversationId;
	private Long userId;
	private Long chatTypeId;

	private String chatType;

}
