package vn.itsol.acs.repository.chat.message;

import org.springframework.stereotype.Repository;

import vn.itsol.acs.dto.chat.MessageDto;

@Repository
public interface MessageRepository {

	boolean insert(MessageDto message);

	MessageDto findFile(MessageDto message);

}
