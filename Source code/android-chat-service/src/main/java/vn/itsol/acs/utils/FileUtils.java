package vn.itsol.acs.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

public class FileUtils {

	private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);

	public static final int UPLOAD_SUCESSFULLY = 0;
	public static final int UPLOAD_FAIL = 1;
	public static final int EMPTY_FILE = 2;

	public void saveUploadedFiles(List<MultipartFile> files) throws IOException {
		for (MultipartFile file : files) {
			if (file.isEmpty()) {
				continue;
			}
			byte[] bytes = file.getBytes();
			Path path = Paths.get(new FileUtils().getFullPath() + file.getOriginalFilename());
			Files.write(path, bytes);
		}

	}

	public String getFullPath() {
		try {
			String basePath = this.getClass().getClassLoader().getResource("").getPath().replaceAll("/D:/", "D://");
			String[] pathArr = basePath.split("target/classes");
			basePath = pathArr[0] + "META-INF/Sent files/";
			String fullPath;
			fullPath = URLDecoder.decode(basePath, "UTF-8");
			return fullPath;
		} catch (UnsupportedEncodingException e) {
			logger.error(e.getMessage());
		}
		return null;
	}

}
