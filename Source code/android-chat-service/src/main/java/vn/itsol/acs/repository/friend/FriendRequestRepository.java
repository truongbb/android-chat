package vn.itsol.acs.repository.friend;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import vn.itsol.acs.dto.friend.FriendRequestDto;

@Repository
@Transactional
public interface FriendRequestRepository {

	FriendRequestDto getAllRequestOfAnUser(Long userId);

	boolean insertIntoFriendRequest(Long senderId, String requestedTime);

	boolean insertIntoUsersFriendRequest(Long userId, Long friendRequestId);

	Long getRequestId(Long senderId, Long receiverId);

	Long getRequestId(Long senderId, String requestedTime);

	// <editor-fold desc="delete friend_request -> a record in users_friend_request
	// will be delete">
	boolean deleteRequest(Long friendRequestId);
	// </editor-fold>

}
