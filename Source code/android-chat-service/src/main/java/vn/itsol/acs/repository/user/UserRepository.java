package vn.itsol.acs.repository.user;

import org.springframework.stereotype.Repository;

import vn.itsol.acs.dto.user.UserDto;

import java.util.List;

@Repository
public interface UserRepository {

    UserDto login(String username, String password);

    boolean changeActiveState(UserDto user);

    boolean setOffline(UserDto user);

    List<UserDto> findByEmailOrPhone(UserDto user);

    // <editor-fold desc="insert into account table">
    boolean insertIntoAccount(UserDto account);
    // </editor-fold>

    // <editor-fold desc="insert into users table">
    boolean insertIntoUser(UserDto account);
    // </editor-fold>

    // <editor-fold desc="update account table">
    boolean updateAccountInfor(UserDto account);
    // </editor-fold>

    // <editor-fold desc="update users table">
    boolean updateUserInfor(UserDto account);
    // </editor-fold>

    // <editor-fold desc="delete account = delete account -> user having this
    // account will be delete">
    boolean delete(UserDto account);
    // </editor-fold>

    // <editor-fold desc="search user and account">
    List<UserDto> doSearch(UserDto account);
    // </editor-fold>

    // <editor-fold desc="search user and account">
    List<UserDto> searchByName(String name, Long userId);
    // </editor-fold>
}
