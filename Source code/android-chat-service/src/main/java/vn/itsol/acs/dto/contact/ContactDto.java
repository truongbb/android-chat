package vn.itsol.acs.dto.contact;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import vn.itsol.acs.dto.user.UserDto;

@Getter
@Setter
public class ContactDto {
	
	private Long userId;
	private Long accountId;
	private String username;
	private Long memberId;
	private String addedTime;

	// <editor-fold desc="sub-fields">
	private String contactListString;
	private List<UserDto> contactList;
	// </editor-fold>
	
}
