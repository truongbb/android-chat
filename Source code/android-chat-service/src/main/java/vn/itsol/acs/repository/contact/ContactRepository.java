package vn.itsol.acs.repository.contact;

import org.springframework.stereotype.Repository;

import vn.itsol.acs.dto.contact.ContactDto;

@Repository
public interface ContactRepository {

	ContactDto getAllContactOfAnUser(Long userId);

	boolean insertIntoContact(Long userId, String addedTime);

	boolean insertIntoUsersContact(Long userId, Long contactTableId);

	Long getContactId(Long userId, String addedTime);

	Long getContactId(Long userId, Long memberId);

	// <editor-fold desc="delete in contact table -> record in users_contact will be
	// delete">
	boolean deleteContact(Long contactId);
	// </editor-fold>

}
