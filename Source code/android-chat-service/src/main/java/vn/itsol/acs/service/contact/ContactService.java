package vn.itsol.acs.service.contact;

import org.springframework.stereotype.Service;

import vn.itsol.acs.dto.contact.ContactDto;

@Service
public interface ContactService {

	ContactDto getAllContactOfAnUser(ContactDto contact);

	boolean addToContact(ContactDto contact);

	boolean deleteFromContact(ContactDto contact);

}
