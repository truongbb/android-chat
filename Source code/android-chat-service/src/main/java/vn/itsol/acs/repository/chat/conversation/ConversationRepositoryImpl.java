package vn.itsol.acs.repository.chat.conversation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import vn.itsol.acs.dto.chat.ConversationDto;
import vn.itsol.acs.dto.chat.CreateConversationDto;
import vn.itsol.acs.repository.BaseRepository;
import vn.itsol.acs.utils.SQLBuilder;

@Repository
@Transactional
public class ConversationRepositoryImpl extends BaseRepository implements ConversationRepository {

    private static final Logger logger = LoggerFactory.getLogger(ConversationRepositoryImpl.class);

    @Override
    public boolean insert(ConversationDto conversation) {
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_CHAT_CONVERSATION, "insert");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("p_title", conversation.getTitle());
            parameters.put("p_creator_id", conversation.getCreatorId());
            parameters.put("p_created_time", conversation.getCreatedTime());
            parameters.put("p_updated_time", conversation.getUpdatedTime());
            return getNamedParameterJdbcTemplate().update(sql, parameters) > 0;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    @Override
    public boolean delete(Long conversationId) {
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_CHAT_CONVERSATION, "delete");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("p_conversation_id", conversationId);
            return getNamedParameterJdbcTemplate().update(sql, parameters) > 0;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    @Override
    public boolean update(ConversationDto conversation) {
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_CHAT_CONVERSATION, "update");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("p_title", conversation.getTitle());
            parameters.put("p_creator_id", conversation.getCreatorId());
            parameters.put("p_created_time", conversation.getCreatedTime());
            parameters.put("p_updated_time", conversation.getUpdatedTime());
            parameters.put("p_conversation_id", conversation.getId());
            return getNamedParameterJdbcTemplate().update(sql, parameters) > 0;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    @Override
    public List<ConversationDto> doSearch(ConversationDto conversation) {
        List<ConversationDto> conversationDtos = new ArrayList<>();
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_CHAT_CONVERSATION, "do_search");
            Map<String, Object> parameters = new HashMap<>();
            if (conversation.getId() != null) {
                sql += "\n and id = :p_conversation_id";
                parameters.put("p_conversation_id", conversation.getId());
            }
            if (conversation.getTitle() != null) {
                sql += "\n and title like '%' || :p_title || '%'";
                parameters.put("p_title", conversation.getTitle());
            }
            if (conversation.getCreatorId() != null) {
                sql += "\n and creator_id = :p_creator_id";
                parameters.put("p_creator_id", conversation.getCreatorId());
            }
            if (conversation.getCreatedTime() != null) {
                sql += "\n and to_char(created_time, 'YYYYMMDD hh24:mi') = :p_created_time";
                parameters.put("p_created_time",
                        new SimpleDateFormat("yyyyMMdd HH:mm").format(conversation.getCreatedTime()));
            }
            if (conversation.getUpdatedTime() != null) {
                sql += "\n and to_char(updated_time, 'YYYYMMDD hh24:mi') = :p_updated_time";
                parameters.put("p_updated_time",
                        new SimpleDateFormat("yyyyMMdd HH:mm").format(conversation.getUpdatedTime()));
            }
            sql += "\n order by id";
            conversationDtos = getNamedParameterJdbcTemplate().query(sql, parameters, (resultSet, rowNum) -> {
                ConversationDto conversationDto = new ConversationDto();
                conversationDto.setId(resultSet.getLong("id"));
                conversationDto.setTitle(resultSet.getString("title"));
                conversationDto.setCreatorId(resultSet.getLong("creator_id"));
                conversationDto.setCreatedTime(resultSet.getDate("created_time"));
                conversationDto.setUpdatedTime(resultSet.getDate("updated_time"));
                return conversationDto;
            });
            return conversationDtos;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    @Override
    public List<CreateConversationDto> searchWhenCreateConversation(int userId, String userList) {
        List<CreateConversationDto> createConversationDtos = new ArrayList<>();
        try {
            String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_CHAT_CONVERSATION, "search_when_create");
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("p_user_id", Integer.valueOf(userId));
            parameters.put("p_user_list", userList);
            createConversationDtos = getNamedParameterJdbcTemplate().query(sql, parameters,
                    (resultSet, rowNum) -> {
                        CreateConversationDto dto = new CreateConversationDto();
                        dto.setConversationId(resultSet.getLong("conversation_id"));
                        dto.setCreatorId(resultSet.getLong("user_id"));
                        dto.setChatTypeId(resultSet.getLong("chat_type"));
                        dto.setTitle(resultSet.getString("title"));
                        return dto;
                    });
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return createConversationDtos;
    }

}
