package vn.itsol.acs.service.chat;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import vn.itsol.acs.dto.chat.ConversationDto;
import vn.itsol.acs.dto.chat.MessageDto;
import vn.itsol.acs.dto.chat.ConversationHistoryDto;
import vn.itsol.acs.dto.chat.CreateConversationDto;
import vn.itsol.acs.dto.chat.ParticipantDto;
import vn.itsol.acs.dto.user.UserDto;

@Service
public interface ChatService {

	CreateConversationDto createConversation(CreateConversationDto createConversationDto);

	boolean createNewConversation(ConversationDto conversationDto, List<ParticipantDto> participantDtos);

	boolean deleteConversation(ConversationDto conversationDto);

	boolean sendMessage(MessageDto message);

	List<ConversationHistoryDto> getConversationHistory(ConversationDto conversationDto);

	List<ConversationDto> getAllConversationOfUser(UserDto user);

	int sendFiles(List<MultipartFile> files, Long conversationId, Long senderId);

	byte[] downloadFile(MessageDto message);
}
