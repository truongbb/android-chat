package vn.itsol.acs.dto.chat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConversationDto {

	private Long id;
	private String title;
	private Long creatorId;
	private java.util.Date createdTime;
	private java.util.Date updatedTime;
	private Long updatedTimeLong;

	// <editor-fold desc="last message/ attachment to show in the conversation list
	// when login successfully">
	private String lastMessage;
	private String lastAttachment;
	private String fileName;
	// </editor-fold>

	// <editor-fold desc="pair = 1 group = 2">
	private Long chatTypeId;
	// </editor-fold>

	// <editor-fold desc="when pair, we need gender id to set avatar">
	private Long friendGenderId;
	// </editor-fold>

}
