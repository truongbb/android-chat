package vn.itsol.acs.dto.chat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageDto {

	public static final Long TEXT = 1L;
	public static final Long FILE = 2L;

	private Long id;
	private Long conversationId;
	private Long senderId;
	private Long messageTypeId;
	private String messageContent;
	private String attatchmenUrl;
	private java.util.Date sentTime;

	// <editor-fold desc="sent time string">
	private String sentTimeString;
	private Long sentTimeLong;
	// </editor-fold>

}
