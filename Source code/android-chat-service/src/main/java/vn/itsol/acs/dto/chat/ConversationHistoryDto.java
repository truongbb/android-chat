package vn.itsol.acs.dto.chat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConversationHistoryDto {

	private Long conversationId;
	private Long chatTypeId;
	private Long messageTypeId;
	private Long senderId;
	private Long receiverId;
	private String messageContent;
	private String attachtmentUrl;
	private java.util.Date sentTime;
	private Long sentTimeLong;

	// <editor-fold desc="sent file name">
	private String fileName;
	// </editor-fold>

}
