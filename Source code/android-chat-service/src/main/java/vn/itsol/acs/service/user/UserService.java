package vn.itsol.acs.service.user;

import org.springframework.stereotype.Service;

import vn.itsol.acs.dto.user.UserDto;

import java.util.List;

@Service
public interface UserService {

	UserDto login(UserDto user);

	boolean setOffline(UserDto user);

	// <editor-fold desc="insert into account table">
	int register(UserDto user);
	// </editor-fold>

	// <editor-fold desc="update account table">
	boolean updateAccountInfor(UserDto account);
	// </editor-fold>

	// <editor-fold desc="update users table">
	boolean updateUserInfor(UserDto account);
	// </editor-fold>

	// <editor-fold desc="delete account = delete user and this user's account">
	boolean delete(UserDto account);
	// </editor-fold>

	// <editor-fold desc="search user and account">
	List<UserDto> doSearch(UserDto account);
	// </editor-fold>

	// <editor-fold desc="search user and account">
	List<UserDto> searchByName(UserDto user);
	// </editor-fold>
}
