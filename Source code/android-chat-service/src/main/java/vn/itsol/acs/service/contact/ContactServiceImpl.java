package vn.itsol.acs.service.contact;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import vn.itsol.acs.dto.contact.ContactDto;
import vn.itsol.acs.dto.friend.FriendRequestDto;
import vn.itsol.acs.dto.user.UserDto;
import vn.itsol.acs.repository.contact.ContactRepository;
import vn.itsol.acs.service.friend.FriendRequestService;
import vn.itsol.acs.utils.StringUtils;

@Service
@Transactional
public class ContactServiceImpl implements ContactService {

    private static final Logger logger = LoggerFactory.getLogger(ContactServiceImpl.class);

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private FriendRequestService friendRequestService;

    @Override
    public ContactDto getAllContactOfAnUser(ContactDto contact) {
        ContactDto contactDto = null;
        if (contact != null) {
            contactDto = this.contactRepository.getAllContactOfAnUser(contact.getUserId());
            if (contactDto != null && contactDto.getContactListString() != null
                    && !"".equals(contactDto.getContactListString())) {
                List<UserDto> memberList = new ArrayList<>();
                String[] memberListString = contactDto.getContactListString().split(";");
                for (String memberUserString : memberListString) {
                    if (memberUserString != null && !memberUserString.trim().equals("")) {
                        String[] senderInfor = StringUtils.removeNullAndSpace(memberUserString).split("\\*");
                        UserDto sender = new UserDto();
                        sender.setUserId(Long.valueOf(senderInfor[0]));
                        sender.setFirstName(senderInfor[1]);
                        sender.setLastName(senderInfor[2]);
                        sender.setAddress(senderInfor[3]);
                        sender.setEmail(senderInfor[4]);
                        sender.setPhone(senderInfor[5]);
                        sender.setGender(senderInfor[6]);
                        try {
//                            sender.setBirthday(new SimpleDateFormat("dd-MM-yyyy").parse(senderInfor[7]));
                            sender.setBirthday(new SimpleDateFormat("dd-MMM-yy").parse(senderInfor[7]));
                        } catch (ParseException e) {
                            logger.error(e.getMessage());
                        }
                        sender.setAddedTimeString(senderInfor[8]);
                        String[] split = senderInfor[8].split("#");
                        try {
                            sender.setAddedTime(new SimpleDateFormat("yyyyMMdd").parse(split[0]));
                        } catch (ParseException e) {
                            logger.error(e.getMessage());
                        }
                        sender.setIsActive("0".equals(senderInfor[9]) ? false : true);
                        memberList.add(sender);
                    }
                }
                contactDto.setContactList(memberList);
            } else {
                contactDto = new ContactDto();
            }
        }
        return contactDto;
    }

    @Override
    public boolean addToContact(ContactDto contact) {
        if (contact != null) {
            // <editor-fold desc=" add B into A's contact">
            try {
                // time#userId#friendId
                String addedTime = contact.getAddedTime();
                contact.setAddedTime(addedTime + "#" + contact.getUserId() + "#" + contact.getMemberId());
                boolean insertIntoContact = this.contactRepository.insertIntoContact(contact.getMemberId(),
                        contact.getAddedTime());
                if (insertIntoContact) {
                    Long contactId = this.contactRepository.getContactId(contact.getMemberId(), contact.getAddedTime());
                    if (contactId != null) {
                        boolean insertIntoUsersContact = this.contactRepository
                                .insertIntoUsersContact(contact.getUserId(), contactId);
                        if (insertIntoUsersContact) {
                            // <editor-fold desc=" add A into B's contact">
                            contact.setAddedTime(addedTime + "#" + contact.getMemberId() + "#" + contact.getUserId());
                            boolean insertIntoContact1 = this.contactRepository.insertIntoContact(contact.getUserId(),
                                    contact.getAddedTime());
                            if (insertIntoContact1) {
                                Long contactId1 = this.contactRepository.getContactId(contact.getUserId(),
                                        contact.getAddedTime());
                                if (contactId1 != null) {
                                    boolean insertIntoUsersContact1 = this.contactRepository
                                            .insertIntoUsersContact(contact.getMemberId(), contactId1);
                                    if (insertIntoUsersContact1) {
                                        // <editor-fold desc="remove friend request">
                                        FriendRequestDto friendRequestDto = new FriendRequestDto();
                                        friendRequestDto.setSenderId(contact.getMemberId());
                                        friendRequestDto.setUserId(contact.getUserId());
                                        boolean cancelRequest = friendRequestService.cancelRequest(friendRequestDto);
                                        if (cancelRequest) {
                                            return true;
                                        } else {
                                            throw new Exception();
                                        }
                                        // </editor-fold>
                                    } else {
                                        throw new Exception();
                                    }
                                } else {
                                    throw new Exception();
                                }
                            } else {
                                throw new Exception();
                            }
                            // </editor-fold>
                        } else {
                            throw new Exception();
                        }
                    } else {
                        throw new Exception();
                    }
                } else {
                    throw new Exception();
                }
            } catch (Exception e) {
                logger.error("Error when adding contact");
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return false;
            }
            // </editor-fold>
        }
        return false;
    }

    @Override
    public boolean deleteFromContact(ContactDto contact) {
        try {
            if (contact != null) {
                Long requestId = this.contactRepository.getContactId(contact.getUserId(), contact.getMemberId());
                if (requestId != null) {
                    boolean deleteRequest = this.contactRepository.deleteContact(requestId);
                    if (deleteRequest) {
                        return true;
                    } else {
                        throw new Exception();
                    }
                } else {
                    throw new Exception();
                }
            }
        } catch (Exception e) {
            logger.debug("Error when removing contact");
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return false;
    }

}
