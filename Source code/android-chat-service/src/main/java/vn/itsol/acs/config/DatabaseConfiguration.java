package vn.itsol.acs.config;

import oracle.jdbc.pool.OracleDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
@ConfigurationProperties("oracle")
public class DatabaseConfiguration {

	// @NotNull
	// private String username;
	// @NotNull
	// private String password;
	// @NotNull
	// private String url;
	//
	// public void setUsername(String username) {
	// this.username = username;
	// }
	//
	// public void setPassword(String password) {
	// this.password = password;
	// }
	//
	// public void setUrl(String url) {
	// this.url = url;
	// }

	private final Logger logger = LoggerFactory.getLogger(DatabaseConfiguration.class);

	@Bean
	DataSource dataSource() {
		try {
			OracleDataSource dataSource = new OracleDataSource();
			dataSource.setUser("truongbb");
			dataSource.setPassword("admin");
			dataSource.setURL("jdbc:oracle:thin:@localhost:1521:xe");
			dataSource.setImplicitCachingEnabled(true);
			dataSource.setFastConnectionFailoverEnabled(true);
			return dataSource;
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return null;
	}
}
