package vn.itsol.acs.rest.contact;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import vn.itsol.acs.dto.contact.ContactDto;
import vn.itsol.acs.service.contact.ContactService;

@RestController
@RequestMapping("${spring.data.rest.base-path}/contact")
public class ContactResource {

	@Autowired
	private ContactService contactService;

	@PostMapping("/all-contact")
	public ResponseEntity<ContactDto> getAllContactOfAnUser(@RequestBody ContactDto account) {
		ContactDto contacts = this.contactService.getAllContactOfAnUser(account);
		return new ResponseEntity<>(contacts, HttpStatus.OK);
	}

	@PostMapping("/add-contact")
	public ResponseEntity<Boolean> addContact(@RequestBody ContactDto account) {
		boolean result = this.contactService.addToContact(account);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@PostMapping("/remove-contact")
	public ResponseEntity<Boolean> removeContact(@RequestBody ContactDto account) {
		boolean result = this.contactService.deleteFromContact(account);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

}
