package vn.itsol.acs.repository.chat.participant;

import java.util.List;

import org.springframework.stereotype.Repository;

import vn.itsol.acs.dto.chat.ParticipantDto;

@Repository
public interface ParticipantRepository {

	boolean insert(ParticipantDto participant);

	List<ParticipantDto> doSearch(ParticipantDto participant);

}
