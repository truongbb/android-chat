package vn.itsol.acs.service.friend;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vn.itsol.acs.dto.friend.FriendRequestDto;

@Service
@Transactional
public interface FriendRequestService {

	FriendRequestDto getAllRequestOfAnUser(FriendRequestDto friendRequestDto);

	boolean sendRequest(FriendRequestDto friendRequestDto);

	boolean cancelRequest(FriendRequestDto friendRequestDto);

}
