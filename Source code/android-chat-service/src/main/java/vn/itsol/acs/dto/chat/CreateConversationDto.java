package vn.itsol.acs.dto.chat;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateConversationDto {

	public static int EXSITED = 1;
	public static int NEW_OK = 2;
	public static int NEW_NOT_OK = 3;
	public static int ERROR = 4;

	private Long creatorId;
	private List<ParticipantDto> participants;
	private String title;
	private java.util.Date createdTime;
	private String createdTimeString;

	private Long conversationId;
	private Long chatTypeId;
	
	private int status;
	private List<ConversationHistoryDto> conversationHistory;
}
