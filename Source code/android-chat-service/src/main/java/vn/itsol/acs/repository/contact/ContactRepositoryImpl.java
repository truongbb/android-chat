package vn.itsol.acs.repository.contact;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import vn.itsol.acs.dto.contact.ContactDto;
import vn.itsol.acs.repository.BaseRepository;
import vn.itsol.acs.utils.SQLBuilder;

@Repository
@Transactional
public class ContactRepositoryImpl extends BaseRepository implements ContactRepository {

	private static final Logger logger = LoggerFactory.getLogger(ContactRepositoryImpl.class);

	@Override
	public ContactDto getAllContactOfAnUser(Long userId) {
		ContactDto contactOfAnUserDto = null;
		try {
			String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_CONTACT, "get_all_contact_of_an_user");
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("p_user_id", userId);
			contactOfAnUserDto = getNamedParameterJdbcTemplate().queryForObject(sql, parameters,
					(resultSet, numRows) -> {
						ContactDto contact = new ContactDto();
						contact.setUserId(resultSet.getLong("user_id"));
						contact.setAccountId(resultSet.getLong("account_id"));
						contact.setUsername(resultSet.getString("username"));
						contact.setContactListString(resultSet.getString("member_infor"));
						return contact;
					});
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return contactOfAnUserDto;
	}

	@Override
	public boolean insertIntoContact(Long userId, String addedTime) {
		try {
			String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_CONTACT, "insert_into_contact");
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("p_user_id", userId);
			parameters.put("p_added_time", addedTime);
			return getNamedParameterJdbcTemplate().update(sql, parameters) > 0;
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return false;
	}

	@Override
	public boolean insertIntoUsersContact(Long userId, Long contactTableId) {
		try {
			String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_CONTACT, "insert_into_users_contact");
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("p_user_id", userId);
			parameters.put("p_contact_id", contactTableId);
			return getNamedParameterJdbcTemplate().update(sql, parameters) > 0;
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return false;
	}

	@Override
	public Long getContactId(Long userId, String addedTime) {
		try {
			String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_CONTACT, "get_contact_id_by_time");
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("p_member_id", userId);
			parameters.put("p_added_time", addedTime);
			// dùng tạm biến userId do không muốn tạo thêm thuộc tính
			ContactDto dto = getNamedParameterJdbcTemplate().queryForObject(sql, parameters, (resultSet, numRows) -> {
				ContactDto contact = new ContactDto();
				contact.setUserId(resultSet.getLong("contact_id"));
				return contact;
			});
			return dto != null ? dto.getUserId() : null;
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Long getContactId(Long userId, Long memberId) {
		try {
			String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_CONTACT, "get_contact_id_by_id");
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("p_user_id", userId);
			parameters.put("p_member_id", memberId);
			// dùng tạm biến userId do không muốn tạo thêm thuộc tính
			ContactDto dto = getNamedParameterJdbcTemplate().queryForObject(sql, parameters, (resultSet, numRows) -> {
				ContactDto contact = new ContactDto();
				contact.setUserId(resultSet.getLong("contact_id"));
				return contact;
			});
			return dto != null ? dto.getUserId() : null;
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean deleteContact(Long contactId) {
		try {
			String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_CONTACT, "delete");
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("p_contact_id", contactId);
			return getNamedParameterJdbcTemplate().update(sql, parameters) > 0;
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return false;
	}

}
