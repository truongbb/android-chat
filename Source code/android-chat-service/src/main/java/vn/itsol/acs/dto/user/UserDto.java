package vn.itsol.acs.dto.user;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {

	// <editor-fold desc="constant">
	public static final int MALE = 1;
	public static final int FEMALE = 2;
	public static final int UNKNOWN = 3;

	public static final int ONLINE = 1;
	public static final int OFFLINE = 2;
	// <editor-fold>

	// <editor-fold desc="user-properties">
	private Long userId;
	private String firstName;
	private String lastName;
	private String address;
	private String email;
	private String phone;
	private Long genderId;

	private String birthdayStr;
	private java.util.Date birthday;
	// </editor-fold>

	// <editor-fold desc="gender-properties">
	private String gender;
	// </editor-fold>

	// <editor-fold desc="account-properties">
	private Long accountId;
	private String username;
	private String password;
	private Boolean isActive;
	private java.util.Date lastOnlineTime;
	// </editor-fold>

	// <editor-fold desc="for friend-request properties">
	private String requestedTimeString;
	private java.util.Date requestedTime;
	// </editor-fold>

	// <editor-fold desc="for account properties">
	private String addedTimeString;
	private java.util.Date addedTime;
	// </editor-fold>

	private Long isContact;
	private Long isSent;
	private Long isSender;
}
