package vn.itsol.acs.repository.chat.participant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import vn.itsol.acs.dto.chat.ParticipantDto;
import vn.itsol.acs.repository.BaseRepository;
import vn.itsol.acs.utils.SQLBuilder;

@Repository
@Transactional
public class ParticipantRepositoryImpl extends BaseRepository implements ParticipantRepository {

	private static final Logger logger = LoggerFactory.getLogger(ParticipantRepositoryImpl.class);

	@Override
	public boolean insert(ParticipantDto participant) {
		try {
			String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_CHAT_PARTICIPANT, "insert");
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("p_conversation_id", participant.getConversationId());
			parameters.put("p_user_id", participant.getUserId());
			parameters.put("p_chat_type_id", participant.getChatTypeId());
			return getNamedParameterJdbcTemplate().update(sql, parameters) > 0;
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return false;
	}

	@Override
	public List<ParticipantDto> doSearch(ParticipantDto participant) {
		List<ParticipantDto> participantDtos = new ArrayList<>();
		try {
			String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_CHAT_PARTICIPANT, "do_search");
			Map<String, Object> parameters = new HashMap<>();
			if (participant.getConversationId() != null) {
				sql += "\n and converastion_id = :p_converastion_id ";
				parameters.put("p_conversation_id", participant.getConversationId());
			}
			if (participant.getUserId() != null) {
				sql += "\n and user_id = :p_user_id";
				parameters.put("p_user_id", participant.getUserId());
			}
			if (participant.getChatTypeId() != null) {
				sql += "\n and chat_type_id = :p_chat_type_id";
				parameters.put("p_chat_type_id", participant.getChatTypeId());
			}
			sql += "\n order by id";
			participantDtos = getNamedParameterJdbcTemplate().query(sql, parameters, (resultSet, rowNum) -> {
				ParticipantDto participantDto = new ParticipantDto();
				participantDto.setId(resultSet.getLong("id"));
				participantDto.setConversationId(resultSet.getLong("conversation_id"));
				participantDto.setUserId(resultSet.getLong("user_id"));
				participantDto.setChatTypeId(resultSet.getLong("chat_type_id"));
				return participantDto;
			});
			return participantDtos;
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

}
