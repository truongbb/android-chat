package vn.itsol.acs.rest.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import vn.itsol.acs.dto.user.UserDto;
import vn.itsol.acs.service.user.UserService;

@RestController
@RequestMapping("${spring.data.rest.base-path}/user")
public class UserResource {

	@Autowired
	private UserService userService;

	@PostMapping("/login")
	public ResponseEntity<UserDto> login(@RequestBody UserDto user) {
		return new ResponseEntity<>(this.userService.login(user), HttpStatus.OK);
	}

	@PostMapping("/set-offline")
	public ResponseEntity<Boolean> setOffline(@RequestBody UserDto user) {
		return new ResponseEntity<>(this.userService.setOffline(user), HttpStatus.OK);
	}

	@PostMapping("/register")
	public ResponseEntity<UserDto> register(@RequestBody UserDto user) {
		UserDto userDto = new UserDto();
		userDto.setUserId(Long.valueOf(this.userService.register(user)));
		return new ResponseEntity<UserDto>(userDto, HttpStatus.OK);
	}

	@PostMapping("/update-account")
	public ResponseEntity<Boolean> updateAccountInfor(@RequestBody UserDto user) {
		return new ResponseEntity<>(userService.updateAccountInfor(user), HttpStatus.OK);
	}

	@PostMapping("/update-user")
	public ResponseEntity<Boolean> updateUserInfor(@RequestBody UserDto user) {
		return new ResponseEntity<>(userService.updateUserInfor(user), HttpStatus.OK);
	}

	@PostMapping("/delete")
	public ResponseEntity<Boolean> delete(@RequestBody UserDto user) {
		return new ResponseEntity<>(userService.delete(user), HttpStatus.OK);
	}

	@PostMapping("/search")
	public ResponseEntity<List<UserDto>> doSearch(@RequestBody UserDto user) {
		return new ResponseEntity<>(userService.doSearch(user), HttpStatus.OK);
	}

	@PostMapping("/search-by-name")
	public ResponseEntity<List<UserDto>> searchByName(@RequestBody UserDto user) {
		return new ResponseEntity<>(userService.searchByName(user), HttpStatus.OK);
	}

}