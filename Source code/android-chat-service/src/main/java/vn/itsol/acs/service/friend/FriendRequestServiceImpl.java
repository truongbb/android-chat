package vn.itsol.acs.service.friend;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import vn.itsol.acs.dto.friend.FriendRequestDto;
import vn.itsol.acs.dto.user.UserDto;
import vn.itsol.acs.repository.friend.FriendRequestRepository;
import vn.itsol.acs.utils.StringUtils;

@Service
@Transactional
public class FriendRequestServiceImpl implements FriendRequestService {

    private static final Logger logger = LoggerFactory.getLogger(FriendRequestServiceImpl.class);

    @Autowired
    private FriendRequestRepository friendRequestRepository;

    @Override
    public FriendRequestDto getAllRequestOfAnUser(FriendRequestDto friendRequestDto) {
        FriendRequestDto resultDto = null;
        if (friendRequestDto != null) {
            resultDto = this.friendRequestRepository.getAllRequestOfAnUser(friendRequestDto.getUserId());
            List<UserDto> requestedList = new ArrayList<>();
            if (resultDto != null && resultDto.getRequestedListString() != null
                    && !"".equals(resultDto.getRequestedListString())) {
                String[] requestListString = resultDto.getRequestedListString().split(";");
                for (String requestedUserString : requestListString) {
                    if (requestedUserString != null && !requestedUserString.trim().equals("")) {
                        String[] senderInfor = StringUtils.removeNullAndSpace(requestedUserString).split("\\*");
                        UserDto sender = new UserDto();
                        sender.setUserId(Long.valueOf(senderInfor[0]));
                        sender.setFirstName(senderInfor[1]);
                        sender.setLastName(senderInfor[2]);
                        sender.setAddress(senderInfor[3]);
                        sender.setEmail(senderInfor[4]);
                        sender.setPhone(senderInfor[5]);
                        sender.setGender(senderInfor[6]);
                        try {
//                            sender.setBirthday(new SimpleDateFormat("dd-MM-yyyy").parse(senderInfor[7]));
                            sender.setBirthday(new SimpleDateFormat("dd-MMM-yy").parse(senderInfor[7]));
                        } catch (ParseException e) {
                            logger.error(e.getMessage());
                        }
                        sender.setRequestedTimeString(senderInfor[8]);
                        String[] split = senderInfor[8].split("#");
                        try {
                            sender.setRequestedTime(new SimpleDateFormat("yyyyMMdd").parse(split[0]));
                        } catch (ParseException e) {
                            logger.error(e.getMessage());
                        }
                        requestedList.add(sender);
                    }
                }
                resultDto.setRequestedList(requestedList);
            } else {
                resultDto = new FriendRequestDto();
            }
        }
        return resultDto;
    }

    @Override
    public boolean sendRequest(FriendRequestDto friendRequestDto) {
        if (friendRequestDto != null) {
            try {
                boolean insertIntoFriendRequest = this.friendRequestRepository.insertIntoFriendRequest(friendRequestDto.getSenderId(), friendRequestDto.getRequestedTime());
                if (insertIntoFriendRequest) {
                    Long requestId = this.friendRequestRepository.getRequestId(friendRequestDto.getSenderId(),
                            friendRequestDto.getRequestedTime());
                    if (requestId != null) {
                        boolean insertIntoUsersFriendRequest = this.friendRequestRepository.insertIntoUsersFriendRequest(friendRequestDto.getUserId(), requestId);
                        if (insertIntoUsersFriendRequest) {
                            return true;
                        } else {
                            throw new Exception();
                        }
                    } else {
                        throw new Exception();
                    }
                } else {
                    throw new Exception();
                }
            } catch (Exception e) {
                logger.error("Error when send friend request");
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            }
        }
        return false;
    }

    @Override
    public boolean cancelRequest(FriendRequestDto friendRequestDto) {
        try {
            if (friendRequestDto != null) {
                Long requestId = this.friendRequestRepository.getRequestId(friendRequestDto.getSenderId(),
                        friendRequestDto.getUserId());
                if (requestId != null) {
                    boolean deleteRequest = this.friendRequestRepository.deleteRequest(requestId);
                    if (deleteRequest) {
                        return true;
                    } else {
                        throw new Exception();
                    }
                } else {
                    throw new Exception();
                }
            }
        } catch (Exception e) {
            logger.debug("Error when cancel request");
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return false;
    }

}