package vn.itsol.acs.repository.chat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import vn.itsol.acs.dto.chat.ConversationDto;
import vn.itsol.acs.dto.chat.ConversationHistoryDto;
import vn.itsol.acs.repository.BaseRepository;
import vn.itsol.acs.utils.SQLBuilder;

@Repository
@Transactional
public class ChatRepositoryImpl extends BaseRepository implements ChatRepository {

	private static final Logger logger = LoggerFactory.getLogger(ChatRepositoryImpl.class);

	@Override
	public List<ConversationHistoryDto> getHistoryOfPairConversation(Long conversationId) {
		try {
			String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_CHAT, "get_conversation_history");
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("p_conversation_id", conversationId);
			List<ConversationHistoryDto> pairConversationHistoryDtos = getNamedParameterJdbcTemplate().query(sql,
					parameters, (resultSet, rowNum) -> {
						ConversationHistoryDto dto = new ConversationHistoryDto();
						dto.setConversationId(resultSet.getLong("conversation_id"));
						dto.setChatTypeId(resultSet.getLong("chat_type_id"));
						dto.setMessageTypeId(resultSet.getLong("message_type_id"));
						dto.setSenderId(resultSet.getLong("sender_id"));
//						dto.setReceiverId(resultSet.getLong("receiver_id"));
						dto.setMessageContent(resultSet.getString("message_content"));
						dto.setAttachtmentUrl(resultSet.getString("attachment_url"));
						dto.setSentTime(resultSet.getDate("sent_time"));
						return dto;
					});
			return pairConversationHistoryDtos;
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@Override
	public List<ConversationDto> getAllConversationOfUser(Long userId) {
		try {
			List<ConversationDto> conversationDtos = new ArrayList<>();
			String sql = SQLBuilder.getSqlQueryById(SQLBuilder.SQL_MODULE_CHAT, "get_all_conversation_of_an_user");
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("p_user_id", userId);
			conversationDtos = getNamedParameterJdbcTemplate().query(sql, parameters, (resultSet, rowNum) -> {
				ConversationDto conversationDto = new ConversationDto();
				conversationDto.setId(resultSet.getLong("conversation_id"));
				conversationDto.setChatTypeId(resultSet.getLong("chat_type"));
				conversationDto.setFriendGenderId(resultSet.getLong("friend_gender_id"));
				conversationDto.setTitle(resultSet.getString("title"));
				conversationDto.setUpdatedTime(resultSet.getDate("updated_time"));
				conversationDto.setLastMessage(resultSet.getString("last_message"));
				conversationDto.setLastAttachment(resultSet.getString("last_attachment"));
				return conversationDto;
			});
			return conversationDtos;
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

}
