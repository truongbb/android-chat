package vn.itsol.acs.repository.chat.conversation;

import java.util.List;

import org.springframework.stereotype.Repository;

import vn.itsol.acs.dto.chat.ConversationDto;
import vn.itsol.acs.dto.chat.CreateConversationDto;

@Repository
public interface ConversationRepository {

	boolean insert(ConversationDto conversation);

	boolean delete(Long conversationId);

	// <editor-fold desc="update title, updated_time">
	boolean update(ConversationDto conversation);
	// </editor-fold>

	List<ConversationDto> doSearch(ConversationDto conversation);

	List<CreateConversationDto> searchWhenCreateConversation(int userId, String userList);

}
