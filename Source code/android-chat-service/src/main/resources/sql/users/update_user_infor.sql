update users set first_name = :p_first_name, last_name = :p_last_name, address = :p_address,
email = :p_email, phone = :p_phone, gender_id = :p_gender_id, birthday = to_date(:p_birthday, 'YYYYMMDD')
where id = :p_user_id