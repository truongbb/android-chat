with search_data as(
    select a.id account_id, a.username, a.is_active, a.last_online_time, u.id user_id, 
        u.first_name, u.last_name, u.address, u.email, u.phone, u.gender_id, g.gender, u.birthday
    from account a
    join users u on a.id = u.account_id
    join gender g on g.id = u.gender_id
    where 
    upper(u.first_name) like '%' || upper(:p_name) || '%' or upper(u.last_name) like '%' || upper(:p_name) || '%'
    and u.id <> :p_user_id
), contact_data as(
    select c.user_id contact_id
    from contact c
    join users_contact uc on uc.contact_id = c.id
    join users u on u.id = uc.user_id
    where u.id = :p_user_id
), sent_request_data as(
    select fr.sender_id sender_id, case when fr.sender_id = :p_user_id then ufr.user_id else :p_user_id end receiver_id
    from friend_request fr
    join users_friend_request ufr on ufr.friend_request_id = fr.id
    join users u on u.id = ufr.user_id
    where ufr.user_id = :p_user_id or fr.sender_id = :p_user_id
)
select distinct s.account_id, s.username, s.is_active, s.last_online_time, s.user_id,
    s.first_name, s.last_name, s.address, s.email, s.phone, s.gender_id, s.gender, s.birthday,
    case when s.user_id = c.contact_id then 1 else 0 end is_contact,
    case when (s.user_id = f.sender_id or f.sender_id = :p_user_id) then 1 else 0 end is_sent,
    case when f.sender_id = s.user_id then 1 else 0 end is_sender
from search_data s
left join contact_data c on c.contact_id = s.user_id
left join sent_request_data f on f.sender_id = s.user_id or f.receiver_id = s.user_id