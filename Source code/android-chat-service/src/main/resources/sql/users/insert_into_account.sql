insert into account
	(id, username, password, is_active, last_online_time)
values
	(account_seq.nextval, :p_username, :p_password, :p_is_active, 
		case when nvl(:p_last_online_time, 0) <> 0 then to_date(:p_last_online_time, 'YYYYMMMDD') else null end)