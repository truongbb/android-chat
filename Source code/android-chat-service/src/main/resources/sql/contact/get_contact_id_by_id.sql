select c.id contact_id
from contact c
join users_contact uc on uc.contact_id = c.id
where c.user_id = :p_member_id
and uc.user_id = :p_user_id