with raw_data as(
    select u.id user_id, a.id account_id, a.username, c.user_id member_id, c.added_time
    from account a
    join users u on u.account_id = a.id
    join gender g on g.id = u.gender_id
    left join users_contact uc on u.id = uc.user_id
    left join contact c on uc.contact_id = c.id
    where (case when ((nvl(:p_user_id, 0) <> 0 and u.id = :p_user_id) or nvl(:p_user_id, 0) = 0) then 1 else 0 end ) = 1
), temp_data as(
    select t.user_id, t.account_id, t.username, t.member_id, 
        case when nvl(t.member_id, 0) <> 0
        then t.member_id || '*' || u.first_name || '*' || u.last_name || '*' || u.address || '*'
            || u.email || '*' || u.phone || '*' || g.gender || '*' || u.birthday || '*' || t.added_time || '*' || nvl(a.is_active, 0)
        else null
        end member_infor
    from raw_data t 
    join users u on t.member_id = u.id
    join gender g on g.id = u.gender_id
    join account a on a.id = u.account_id
)
select user_id, account_id, username, listagg(to_char(member_infor), ';') within group(order by member_id) member_infor
from temp_data t
group by user_id, account_id, username