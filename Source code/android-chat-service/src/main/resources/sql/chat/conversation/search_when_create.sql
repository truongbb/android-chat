with split_data as(
    select trim(regexp_substr(:p_user_list, '[^,]+', 1, level)) user_id
    from dual
    connect by level <= regexp_count(:p_user_list, ',') + 1
),user_id_temp_list as(
        select trim(regexp_substr(:p_user_list, '[^,]+', 1, level)) user_id
        from dual
        connect by level <= regexp_count(:p_user_list, ',') + 1
    union
        select to_char(:p_user_id) user_id
        from dual
), user_id_list as(
    select listagg(user_id, ',') within group (order by user_id) user_id_list
    from user_id_temp_list
), raw_data as(
        select c.id conversation_id, c.creator_id user_id
        from conversation c
    union
        select c.id conversation_id, p.user_id
        from conversation c
        join participant p on c.id = p.conversation_id
), temp as(
    select conversation_id, user_id
    from raw_data
    order by conversation_id, user_id
), temp_data as(
    select r.conversation_id, listagg(r.user_id, ',') within group (order by r.conversation_id) user_id_list
    from temp r
    group by r.conversation_id
), conversation_id as(
    select conversation_id
    from temp_data t
    join user_id_list uil on t.user_id_list = uil.user_id_list
)
select distinct fd.conversation_id, u.id user_id, p.chat_type_id chat_type,
    case
        when p.chat_type_id = 2 and c.title is not null then c.title
        when p.chat_type_id = 2 and c.title is null then u.first_name || ' ' || u.last_name
        when p.chat_type_id = 1 then u.first_name || ' ' || u.last_name
        else null
    end title
from conversation_id fd
join conversation c on c.id = fd.conversation_id
join participant p on p.conversation_id = c.id
join users u on u.id in (select * from split_data)
order by u.id