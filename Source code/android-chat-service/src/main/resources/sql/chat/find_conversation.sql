select c.id conversation_id, c.title, c.creator_id, c.created_time, c.updated_time, 
    p.id participant_id, p.user_id, p.chat_type_id, ct.type
from conversation c
join participant p on c.id = p.conversation_id
join chat_type ct on ct.id = p.chat_type_id
where 
(
    (c.creator_id = :p_creator_id and p.user_id = :p_user_id)
    or
    (c.creator_id = :p_user_id and p.user_id = :p_creator_id)
)
and chat_type_id = :p_chat_type_id