select id, conversation_id, sender_id, message_type_id, message_content, attachment_url, sent_time
from message
where conversation_id = :p_conversation_id
and to_char(sent_time, 'YYYYMMDD HH24:MI:SS') = :p_sent_time
and attachment_url = :p_attachment_url
and message_type_id = 2