select a.id account_id, a.username, a.password, a.is_active, a.last_online_time, u.id user_id, 
    u.first_name, u.last_name, u.address, u.email, u.phone, u.gender_id, g.gender, u.birthday
from account a
left join users u on a.id = u.account_id
left join gender g on g.id = u.gender_id
where a.username = :p_username
and a.password = :p_password