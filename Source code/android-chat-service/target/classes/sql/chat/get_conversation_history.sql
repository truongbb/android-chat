select distinct c.id conversation_id, p.chat_type_id, m.message_type_id, m.sender_id, 
--        case when m.sender_id = c.creator_id then p.user_id else c.creator_id end receiver_id,
        m.message_content, m.attachment_url, m.sent_time
from message m 
join conversation c on c.id = m.conversation_id
join participant p on c.id = p.conversation_id
join chat_type ct on ct.id = p.chat_type_id
join message_type mt on mt.id = m.message_type_id
join users u on u.id = m.sender_id or u.id = p.user_id or u.id = c.creator_id
where c.id = :p_conversation_id
order by m.sent_time