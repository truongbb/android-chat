insert into message
	(id, conversation_id, sender_id, message_type_id, message_content, attachment_url, sent_time)
values
	(message_seq.nextval, :p_conversation_id, :p_sender_id, :p_message_type_id, :p_message_content, :p_attachtment_url, to_date(:p_sent_time, 'yyyymmdd hh24:mi:ss'))