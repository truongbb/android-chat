with first_data as(
    select distinct c.id conversation_id
    from conversation c
    join participant p on p.conversation_id = c.id
    where c.creator_id = :p_user_id or p.user_id = :p_user_id
), raw_data as(
    select distinct fd.conversation_id, p.chat_type_id chat_type, u.id friend_id,
        case when p.chat_type_id = 2 then null else g.id end friend_gender_id,
        case
            when p.chat_type_id = 2 and c.title is not null then c.title
            when p.chat_type_id = 2 and c.title is null then u.first_name || ' ' || u.last_name
            when p.chat_type_id = 1 then u.first_name || ' ' || u.last_name
            else null
        end title,
        c.updated_time, m.message_content last_message, m.attachment_url last_attachment
    from first_data fd
    join conversation c on c.id = fd.conversation_id
    join participant p on p.conversation_id = c.id
    join message m on m.conversation_id = c.id
    join users u on (u.id = c.creator_id or u.id = p.user_id)
    join gender g on g.id = u.gender_id
    where u.id <> :p_user_id
    and m.sent_time = c.updated_time
), temp_data as(
    select r.conversation_id, r.chat_type,
        listagg(r.friend_id, ',') within group (order by r.conversation_id) friend_id,
        friend_gender_id, r.title, r.updated_time,  r.last_message, r.last_attachment
    from raw_data r
    group by r.conversation_id, r.chat_type, r.title, r.friend_gender_id, r.updated_time, r.last_message, r.last_attachment
)
select t.conversation_id, t.chat_type,
    listagg(to_char(t.friend_id), ',') within group (order by t.conversation_id) friend_id,
    t.friend_gender_id,
    listagg(to_char(t.title), ',') within group (order by t.conversation_id) title,
    t.updated_time, t.last_message, t.last_attachment
from temp_data t
group by t.conversation_id, t.chat_type, t.friend_gender_id, t.updated_time, t.last_message, t.last_attachment
order by t.updated_time desc