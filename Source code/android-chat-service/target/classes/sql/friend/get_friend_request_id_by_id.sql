select fr.id friend_request_id
from friend_request fr
join users_friend_request ufr on ufr.friend_request_id = fr.id
where fr.sender_id = :p_sender_id
and ufr.user_id = :p_receiver_id