with raw_data as(
    select u.id user_id, a.id account_id, a.username, fr.sender_id, fr.requested_time
    from account a
    join users u on u.account_id = a.id
    join gender g on g.id = u.gender_id
    left join users_friend_request ufr on u.id = ufr.user_id
    left join friend_request fr on ufr.friend_request_id = fr.id
    where (case when ((nvl(:p_user_id, 0) <> 0 and u.id = :p_user_id) or nvl(:p_user_id, 0) = 0) then 1 else 0 end ) = 1
), temp_data as(
    select t.user_id, t.account_id, t.username, t.sender_id, 
        case when nvl(t.sender_id, 0) <> 0
        then t.sender_id || '*' || u.first_name || '*' || u.last_name || '*' || u.address || '*'
            || u.email || '*' || u.phone || '*' || g.gender || '*' || u.birthday || '*' || t.requested_time
        else null
        end requested_infor
    from raw_data t 
    join users u on t.sender_id = u.id
    join gender g on g.id = u.gender_id
)
select user_id, account_id, username, listagg(to_char(requested_infor), ';') within group(order by sender_id) requested_infor
from temp_data t
group by user_id, account_id, username