package com.thucpn.ttsp.service;

import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.model.ContactDto;
import com.thucpn.ttsp.model.ConversationDto;
import com.thucpn.ttsp.model.ConversationHistoryDto;
import com.thucpn.ttsp.model.CreateConversationDto;
import com.thucpn.ttsp.model.FriendRequestDto;
import com.thucpn.ttsp.model.MessageDto;
import com.thucpn.ttsp.model.UserDto;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by thucpn on 15/03/2018.
 */

public interface ChatAPI {

    //user
    //login
    @POST(Constant.linkUser + "login")
    Call<UserDto> login(@Body UserDto userDto);

    //logout
    @POST(Constant.linkUser + "set-offline")
    Call<Boolean> logout(@Body UserDto userDto);

    //register
    @POST(Constant.linkUser + "register")
    Call<UserDto> register(@Body UserDto userDto);

    //change profile
    @POST(Constant.linkUser + "update-user")
    Call<Boolean> changeProfile(@Body UserDto userDto);

    //change password
    @POST(Constant.linkUser + "update-account")
    Call<Boolean> changePassword(@Body UserDto userDto);

    //search user
    @POST(Constant.linkUser + "search-by-name")
    Call<List<UserDto>> searchUser(@Body UserDto userDto);


    //conversation
    //get all conversation
    @POST(Constant.linkChat + "all-conversation")
    Call<List<ConversationDto>> getAllConversationOfUser(@Body UserDto userDto);

    //delete conversation
    @POST(Constant.linkChat + "delete-conversation")
    Call<List<ConversationDto>> deleteConversation(@Body ConversationDto conversationDto);

    //creat conversation
    @POST(Constant.linkChat + "create-conversation")
    Call<CreateConversationDto> creatConversation(@Body CreateConversationDto createConversationDto);

    //contact
    //get all contact
    @POST(Constant.linkContact + "all-contact")
    Call<ContactDto> getAllContact(@Body UserDto userDto);

    //delete contact
    @POST(Constant.linkContact + "remove-contact")
    Call<Boolean> deleteContact(@Body ContactDto contactDto);


    //friend request
    //get all friend request
    @POST(Constant.linkFriendRequest + "all-request")
    Call<FriendRequestDto> getAllFriendRequest(@Body UserDto userDto);

    //accept friend request
    @POST(Constant.linkContact + "add-contact")
    Call<Boolean> acceptFriendRequest(@Body ContactDto contactDto);

    //not accept friend request
    @POST(Constant.linkFriendRequest + "cancel-request")
    Call<Boolean> cancelFriendRequest(@Body FriendRequestDto friendRequestDto);

    //send friend request
    @POST(Constant.linkFriendRequest + "send-request")
    Call<Boolean> sendFriendRequest(@Body FriendRequestDto friendRequestDto);


    //message
    //get all message
    @POST(Constant.linkChat + "conversation-history")
    Call<List<ConversationHistoryDto>> getAllMessage(@Body ConversationDto conversationDto);

    //send message
    @POST(Constant.linkChat + "send-message")
    Call<Boolean> sendMessage(@Body MessageDto messageDto);

    //send file
    @Multipart
    @POST(Constant.linkChat + "send-file")
    Call<Integer> sendFile(@Part MultipartBody.Part file, @Part("conversationId") Long conversationId, @Part("senderId") Long senderId);

    //download file
    @POST(Constant.linkChat + "download-file")
    Call<InputStream> downloadFile(@Body MessageDto messageDto);
}
