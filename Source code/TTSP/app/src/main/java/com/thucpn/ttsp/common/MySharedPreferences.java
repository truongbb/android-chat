package com.thucpn.ttsp.common;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by thucpn on 23/02/2018.
 */

public class MySharedPreferences {
    private SharedPreferences mSharedPreferences;

    public enum KEY {
        USERNAME, PASSWORD, SHORT_PASSWORD, AUTO_LOGIN
    }

    public MySharedPreferences(Context context) {
        this.mSharedPreferences = context.getSharedPreferences("ACCOUNT", Context.MODE_PRIVATE);
    }

    public String getData(KEY key) {
        return mSharedPreferences.getString(key.toString(), "");
    }

    public void putData(KEY key, String value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key.toString(), value);
        editor.commit();
    }
}
