package com.thucpn.ttsp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.thucpn.ttsp.fragment.ListContactFragment;
import com.thucpn.ttsp.fragment.ListConversationFragment;
import com.thucpn.ttsp.fragment.ListFriendRequestFragment;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PagerAdapter extends FragmentStatePagerAdapter {
    private ListConversationFragment listConversationFragment;
    private ListContactFragment listContactFragment;
    private ListFriendRequestFragment listFriendRequestFragment;

    public PagerAdapter(FragmentManager fm) {
        super(fm);
        listConversationFragment = new ListConversationFragment();
        listContactFragment = new ListContactFragment();
        listFriendRequestFragment = new ListFriendRequestFragment();
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = listConversationFragment;
                break;
            case 1:
                fragment = listContactFragment;
                break;
            case 2:
                fragment = listFriendRequestFragment;
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
