package com.thucpn.ttsp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.thucpn.ttsp.R;

import de.hdodenhof.circleimageview.CircleImageView;
import lombok.Getter;

@Getter
public class MessageRightViewHolder extends RecyclerView.ViewHolder {
    private CircleImageView imvAvatar_item_message_right;
    private TextView tvContent_item_message_right;
    private TextView tvStatus_item_message_right;
    private TextView tvTime_item_message_right;

    public MessageRightViewHolder(View itemView) {
        super(itemView);
        imvAvatar_item_message_right = itemView.findViewById(R.id.imvAvatar_item_message_right);
        tvContent_item_message_right = itemView.findViewById(R.id.tvContent_item_message_right);
        tvStatus_item_message_right = itemView.findViewById(R.id.tvStatus_item_message_right);
        tvTime_item_message_right = itemView.findViewById(R.id.tvTime_item_message_right);
        imvAvatar_item_message_right.setVisibility(View.GONE);
    }
}
