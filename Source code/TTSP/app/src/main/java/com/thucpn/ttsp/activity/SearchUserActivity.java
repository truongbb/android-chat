package com.thucpn.ttsp.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.RelativeLayout;

import com.mikepenz.itemanimators.SlideUpAlphaAnimator;
import com.thucpn.ttsp.R;
import com.thucpn.ttsp.adapter.SearchUserAdapter;
import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.common.ResourceUtil;
import com.thucpn.ttsp.model.UserDto;

import java.util.List;

public class SearchUserActivity extends AppCompatActivity {
    private ResourceUtil resourceUtil = (ResourceUtil) ResourceUtil.getContext();
    private List<UserDto> arrUserDtoResult;
    private RelativeLayout layout_none_result;
    private RecyclerView recyclerView_Search_user_result;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_search_user);
        init();
    }

    private void init() {
        getSupportActionBar().setTitle("Tìm bạn");
        arrUserDtoResult = getIntent().getParcelableArrayListExtra(Constant.SEARCH_USER_RESULT);
        recyclerView_Search_user_result = findViewById(R.id.recyclerView_Search_user_result1);
        layout_none_result = findViewById(R.id.layout_none_result);
//        arrUserDtoResult = resourceUtil.arrSearchUserResult;
        if (arrUserDtoResult == null || arrUserDtoResult.size() <= 0) {
            layout_none_result.setVisibility(View.VISIBLE);
            recyclerView_Search_user_result.setVisibility(View.GONE);
        } else {
            layout_none_result.setVisibility(View.GONE);
            recyclerView_Search_user_result.setVisibility(View.VISIBLE);
            SearchUserAdapter searchUserAdapter = new SearchUserAdapter();
            searchUserAdapter.setArrUserDto(arrUserDtoResult);
            searchUserAdapter.setContext(this);
            StaggeredGridLayoutManager gridLayoutManager =
                    new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
            recyclerView_Search_user_result.setLayoutManager(gridLayoutManager);
            recyclerView_Search_user_result.setAdapter(searchUserAdapter);
            recyclerView_Search_user_result.setHasFixedSize(true);
            recyclerView_Search_user_result.setItemAnimator(new SlideUpAlphaAnimator());
        }
    }

}
