package com.thucpn.ttsp.fragment;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.RelativeLayout;

import com.mikepenz.itemanimators.SlideUpAlphaAnimator;
import com.thucpn.ttsp.R;
import com.thucpn.ttsp.activity.MainActivity;
import com.thucpn.ttsp.adapter.ContactAdapter;
import com.thucpn.ttsp.common.ResourceUtil;
import com.thucpn.ttsp.model.ContactDto;
import com.thucpn.ttsp.model.UserDto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by thucpn on 23/03/2018.
 */
public class ListContactFragment extends BaseFragment {
    private RecyclerView recyclerViewContact;
    private RelativeLayout layout_none_contact;
    private List<UserDto> arrContactList;
    private ContactDto contactDto;

    public ListContactFragment() {
        initData();
    }

    private void initData() {
        contactDto = resourceUtil.contactDto;
        if (resourceUtil.contactDto != null) {
            arrContactList = resourceUtil.contactDto.getContactList();
        }
    }

    @Override
    protected int getViewID() {
        return R.layout.demo_list_contact;
    }

    @Override
    protected void init() {
        initData();
        layout_none_contact = getActivity().findViewById(R.id.layout_none_contact);
        recyclerViewContact = getActivity().findViewById(R.id.recyclerViewContact);

        if (arrContactList == null || arrContactList.isEmpty()) {
            layout_none_contact.setVisibility(View.VISIBLE);
            recyclerViewContact.setVisibility(View.GONE);
        } else {
            layout_none_contact.setVisibility(View.GONE);
            recyclerViewContact.setVisibility(View.VISIBLE);
            ContactAdapter contactAdapter = new ContactAdapter();
            contactAdapter.setContactDto(contactDto);
            contactAdapter.setContext(getActivity());
            StaggeredGridLayoutManager gridLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
            recyclerViewContact.setLayoutManager(gridLayoutManager);
            recyclerViewContact.setAdapter(contactAdapter);
            recyclerViewContact.setItemAnimator(new SlideUpAlphaAnimator());
            recyclerViewContact.setHasFixedSize(true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        init();
    }
}
