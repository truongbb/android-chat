package com.thucpn.ttsp.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.thucpn.ttsp.R;
import com.thucpn.ttsp.adapter.PagerAdapter;
import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.common.DatabaseUtil;
import com.thucpn.ttsp.common.MySharedPreferences;
import com.thucpn.ttsp.common.ResourceUtil;
import com.thucpn.ttsp.fragment.ListContactFragment;
import com.thucpn.ttsp.fragment.ListConversationFragment;
import com.thucpn.ttsp.fragment.ListFriendRequestFragment;
import com.thucpn.ttsp.model.ContactDto;
import com.thucpn.ttsp.model.ConversationDto;
import com.thucpn.ttsp.model.FriendRequestDto;
import com.thucpn.ttsp.model.UserDto;
import com.thucpn.ttsp.service.ChatAPI;
import com.thucpn.ttsp.service.ChatService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private PagerAdapter pagerAdapter;
    private TextView tvName_navigation_header;
    private TextView tvEmail_navigation_header;
    private ImageView imvAvatar_activity_main;
    private LinearLayout tvProfile_navigation;
    private LinearLayout tvChangePassword_navigation;
    private LinearLayout tvSetting;
    private LinearLayout tvLogout_navigation;
    private LinearLayout tvAbout_navigation;
    private Dialog dialogChangePassword;

    private ResourceUtil resourceUtil = (ResourceUtil) ResourceUtil.getContext();
    private UserDto userDto;
    private DatabaseUtil databaseUtil = DatabaseUtil.getInstance(this);

    private ChatAPI chatAPI;
    private ListConversationFragment listConversationFragment;
    private ListContactFragment listContactFragment;
    private ListFriendRequestFragment listFriendRequestFragment;

    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            setData();
            handler.postDelayed(this, 1000);
        }
    };
    private Intent intentService;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        intentService = new Intent(this, ChatService.class);
//        bindService(intentService, connection, BIND_AUTO_CREATE);
        startService(intentService);
        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        init();
    }

//    private ServiceConnection connection = new ServiceConnection() {
//        @Override
//        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
//            ChatService.MyBinder myBinder = (ChatService.MyBinder) iBinder;
//            chatService = myBinder.getService();
//        }
//
//        @Override
//        public void onServiceDisconnected(ComponentName componentName) {
//
//        }
//    };

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        drawerLayout = findViewById(R.id.drawerLayout);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);

        //Navigation
        tvName_navigation_header = findViewById(R.id.tvName_navigation_header);
        imvAvatar_activity_main = findViewById(R.id.imvAvatar_activity_main);
        tvProfile_navigation = findViewById(R.id.tvProfile_navigation);
        tvChangePassword_navigation = findViewById(R.id.tvChangePassword_navigation);
        tvEmail_navigation_header = findViewById(R.id.tvEmail_navigation_header);
        tvLogout_navigation = findViewById(R.id.tvLogout_navigation);
        tvAbout_navigation = findViewById(R.id.tvAbout_navigation);
        tvSetting = findViewById(R.id.tvSetting);
        tvProfile_navigation.setOnClickListener(this);
        tvChangePassword_navigation.setOnClickListener(this);
        tvSetting.setOnClickListener(this);
        tvLogout_navigation.setOnClickListener(this);
        tvAbout_navigation.setOnClickListener(this);

        listConversationFragment = new ListConversationFragment();
        listContactFragment = new ListContactFragment();
        listFriendRequestFragment = new ListFriendRequestFragment();
        pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        pagerAdapter.setListContactFragment(listContactFragment);
        pagerAdapter.setListConversationFragment(listConversationFragment);
        pagerAdapter.setListFriendRequestFragment(listFriendRequestFragment);

        setData();
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPaper);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(3);

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(getResources().getDrawable(R.drawable.ic_conversation));
        tabLayout.getTabAt(1).setIcon(getResources().getDrawable(R.drawable.ic_contact));
        tabLayout.getTabAt(2).setIcon(getResources().getDrawable(R.drawable.ic_person_add));
        viewPager.addOnPageChangeListener(this);

        handler.postDelayed(runnable, 1000);

    }

    private void setData() {
        if (databaseUtil.getLastUser() != null) {
            resourceUtil.userDto = databaseUtil.getLastUser();
        } else {
            Intent intent = new Intent(this, BeginActivity.class);
            startActivity(intent);
            this.finish();
        }
        userDto = resourceUtil.userDto;
        chatAPI = Constant.getChatAPI();
        UserDto dto = new UserDto();
        dto.setUserId(userDto.getUserId());
        Call<List<ConversationDto>> callArrConversations = chatAPI.getAllConversationOfUser(dto);
        callArrConversations.enqueue(new Callback<List<ConversationDto>>() {
            @Override
            public void onResponse(Call<List<ConversationDto>> call, Response<List<ConversationDto>> response) {
                if (response.isSuccessful()) {
                    List<ConversationDto> arrConversationDto = response.body();
                    resourceUtil.arrConversationDto = arrConversationDto;
                } else {
                    Log.e("Call all conversation", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<ConversationDto>> call, Throwable t) {
                Log.e("ConnectAPI all conver", new Exception(t).getMessage());
            }
        });

        Call<ContactDto> callAllContact = chatAPI.getAllContact(dto);
        callAllContact.enqueue(new Callback<ContactDto>() {
            @Override
            public void onResponse(Call<ContactDto> call, Response<ContactDto> response) {
                if (response.isSuccessful()) {
                    ContactDto contactDto = response.body();
                    resourceUtil.contactDto = contactDto;
                } else {
                    Log.e("GetAllCont onResponse", "not successful");
                }
            }

            @Override
            public void onFailure(Call<ContactDto> call, Throwable t) {
                Log.e("GetAllCont onFailure", new Exception(t).getMessage());
            }
        });

        Call<FriendRequestDto> callFriendRequest = chatAPI.getAllFriendRequest(dto);
        callFriendRequest.enqueue(new Callback<FriendRequestDto>() {
            @Override
            public void onResponse(Call<FriendRequestDto> call, Response<FriendRequestDto> response) {
                if (response.isSuccessful()) {
                    FriendRequestDto friendRequestDto = response.body();
                    resourceUtil.friendRequestDto = friendRequestDto;
                } else {
                    Log.e("GetAllFriend onResponse", "not successful");
                }
            }

            @Override
            public void onFailure(Call<FriendRequestDto> call, Throwable t) {
                Log.e("GetAllFriend onFailure", new Exception(t).getMessage());
            }
        });

        if (userDto != null) {
//            getSupportActionBar().setTitle(userDto.getFirstName() + " " + userDto.getLastName());
//            getSupportActionBar().setHomeButtonEnabled(true);
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            tvName_navigation_header.setText(userDto.getFirstName() + " " + userDto.getLastName());
            tvEmail_navigation_header.setText(userDto.getEmail());
            if (userDto.getGenderId() == Constant.UNKNOWN) {
                //
            } else {
                if (userDto.getGenderId() == Constant.MALE) {
                    imvAvatar_activity_main.setImageResource(R.drawable.img_male_avatar);
                } else {
                    imvAvatar_activity_main.setImageResource(R.drawable.img_female_avatar);
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        init();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
//        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
//        unbindService(connection);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                int intentData = data.getIntExtra(Constant.REQUEST_ACTION, Constant.REQUEST_RESULT_CHANGE_PROFILE);
                switch (intentData) {
                    case Constant.REQUEST_RESULT_CHANGE_PROFILE:
                        viewPager.setCurrentItem(0);
                        break;
                    case Constant.REQUEST_RESULT_CONTACT:
                        viewPager.setCurrentItem(1);
                        break;
                    case Constant.REQUEST_RESULT_FRIEND_REQUEST:
                        viewPager.setCurrentItem(2);
                        break;
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
//            case R.id.fab:
//                switch (viewPager.getCurrentItem()) {
//                    case 0:
//                        intent = new Intent(this, CreateNewConversation.class);
//                        startActivity(intent);
//                        break;
//                    case 2:
//                        intent = new Intent(this, SearchUserActivity.class);
//                        startActivity(intent);
//                        break;
//                }
//                break;
            case R.id.tvProfile_navigation:
                drawerLayout.closeDrawers();
                intent = new Intent(this, ProfileActivity.class);
                intent.putExtra(Constant.ACTION, Constant.ACTION_CHANGE_PROFILE);
                intent.putExtra(Constant.ACTIVITY_PARENT, Constant.MAIN_ACTIVITY);
                startActivity(intent);
//                this.finish();
                break;
            case R.id.tvChangePassword_navigation:
                drawerLayout.closeDrawers();
                dialogChangePassword = new Dialog(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth);
                dialogChangePassword.setContentView(R.layout.layout_change_password);
                dialogChangePassword.setCancelable(true);
                dialogChangePassword.show();

                final TextInputLayout tilCurrentPassword = dialogChangePassword.findViewById(R.id.tilCurrentPassword);
                final EditText edtCurrentPassword = dialogChangePassword.findViewById(R.id.edtCurrentPassword);
                final TextInputLayout tilNewPasswoord = dialogChangePassword.findViewById(R.id.tilNewPasswoord);
                final EditText edtNewPasswoord = dialogChangePassword.findViewById(R.id.edtNewPasswoord);
                final TextInputLayout tilReenterPasswoord = dialogChangePassword.findViewById(R.id.tilReenterPasswoord);
                final EditText edtReenterPasswoord = dialogChangePassword.findViewById(R.id.edtReenterPasswoord);
                Button btnCancel_ChangePassword = dialogChangePassword.findViewById(R.id.btnCancel_ChangePassword);
                Button btnSave_ChangePassword = dialogChangePassword.findViewById(R.id.btnSave_ChangePassword);
                tilNewPasswoord.setHint(getResources().getString(R.string.password) + " " + getResources().getString(R.string.requirement_password));
                btnCancel_ChangePassword.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogChangePassword.dismiss();
                    }
                });
                btnSave_ChangePassword.setOnClickListener(new View.OnClickListener() {
                    String currentPass;
                    String newPass;
                    String reenterPass;

                    @Override
                    public void onClick(View v) {
                        if (checkInput()) {
                            userDto.setPassword(newPass);
                            loadAnswersChangePassword();
                        }
                    }

                    private void loadAnswersChangePassword() {
                        chatAPI = Constant.getChatAPI();
                        UserDto dto = new UserDto();
                        dto.setAccountId(userDto.getAccountId());
                        dto.setUsername(userDto.getUsername());
                        dto.setPassword(userDto.getPassword());
                        Call<Boolean> call = chatAPI.changePassword(dto);
                        call.enqueue(new Callback<Boolean>() {
                            @Override
                            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                if (response.isSuccessful()) {
                                    Log.e("changePass", response.body().toString());
                                    if (response.body()) {
                                        resourceUtil.userDto.setPassword(userDto.getPassword());
                                        MySharedPreferences mySharedPreferences = new MySharedPreferences(MainActivity.this);
                                        mySharedPreferences.putData(MySharedPreferences.KEY.PASSWORD, userDto.getPassword());
                                        DatabaseUtil databaseUtil = DatabaseUtil.getInstance(MainActivity.this);
                                        databaseUtil.updateUser(resourceUtil.userDto);
                                        dialogChangePassword.dismiss();
                                        Snackbar snackbar = Snackbar.make(drawerLayout, getResources().getText(R.string.success), Snackbar.LENGTH_SHORT);
                                        snackbar.show();
                                    } else {
                                        dialogChangePassword.dismiss();
                                        Snackbar snackbar = Snackbar.make(drawerLayout, getResources().getText(R.string.not_success), Snackbar.LENGTH_SHORT);
                                        snackbar.show();
                                    }
                                } else {
                                    Snackbar snackbar = Snackbar.make(drawerLayout, getResources().getText(R.string.not_success), Snackbar.LENGTH_SHORT);
                                    snackbar.show();
                                    Log.e("changePass onResponse", "not successful");
                                }
                            }

                            @Override
                            public void onFailure(Call<Boolean> call, Throwable t) {
                                Snackbar snackbar = Snackbar.make(drawerLayout, getResources().getText(R.string.not_success), Snackbar.LENGTH_SHORT);
                                snackbar.show();
                                Log.e("changePass onFailure", new Exception(t).getMessage());
                            }
                        });
                    }

                    private boolean checkInput() {
                        currentPass = edtCurrentPassword.getText().toString().trim();
                        newPass = edtNewPasswoord.getText().toString().trim();
                        reenterPass = edtReenterPasswoord.getText().toString().trim();
                        if (currentPass == null || currentPass.length() == 0 || currentPass.equals(userDto.getPassword()) == false) {
                            tilCurrentPassword.setError(getResources().getString(R.string.current_password) + " " + getResources().getString(R.string.not_valid));
                            return false;
                        }
                        if (newPass == null || newPass.length() < 8 || newPass.length() > 16) {
                            tilNewPasswoord.setError(getResources().getString(R.string.password) + " " + getResources().getString(R.string.requirement_password));
                            return false;
                        }
                        if (newPass.equals(userDto.getPassword())) {
                            tilNewPasswoord.setError(getResources().getString(R.string.like_current_pass));
                            return false;
                        }
                        if (reenterPass == null || reenterPass.length() == 0 || reenterPass.equals(newPass) == false) {
                            tilReenterPasswoord.setError(getResources().getString(R.string.password) + " " + getResources().getString(R.string.not_valid));
                            return false;
                        }
                        return true;
                    }
                });
                break;
            case R.id.tvSetting:
                drawerLayout.closeDrawers();
                Toast.makeText(this, "setting", Toast.LENGTH_LONG).show();
                break;
            case R.id.tvLogout_navigation:
                drawerLayout.closeDrawers();
                chatAPI = Constant.getChatAPI();
                UserDto dto = new UserDto();
                dto.setAccountId(userDto.getAccountId());
                Call<Boolean> call = chatAPI.logout(dto);
                call.enqueue(new Callback<Boolean>() {
                    @Override
                    public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                        if (response.isSuccessful()) {
                            MySharedPreferences mySharedPreferences = new MySharedPreferences(MainActivity.this);
                            mySharedPreferences.putData(MySharedPreferences.KEY.AUTO_LOGIN, Constant.AUTO_LOGIN_FALSE);
                            databaseUtil.deleteUser();
                            databaseUtil.deleteAllLastSeen();
                            intentService = new Intent(mContext, ChatService.class);
                            stopService(intentService);
                            Intent intent = new Intent(MainActivity.this, BeginActivity.class);
                            startActivity(intent);
                            MainActivity.this.finish();
                        } else {
                            Toast.makeText(MainActivity.this, "khong thanh cong", Toast.LENGTH_SHORT).show();
                            Log.e("logout onRespone", "not successful");
                        }
                    }

                    @Override
                    public void onFailure(Call<Boolean> call, Throwable t) {
                        Log.e("logout onFailure", new Exception(t).getMessage());
                    }
                });
                break;
            case R.id.tvAbout_navigation:
                drawerLayout.closeDrawers();
                Toast.makeText(this, "about", Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0:
                getSupportActionBar().setTitle(getResources().getString(R.string.message));
                break;
            case 1:
                getSupportActionBar().setTitle(getResources().getString(R.string.contact));
                break;
            case 2:
                getSupportActionBar().setTitle(getResources().getString(R.string.friend_request));
                break;
        }
        pagerAdapter.getItem(position).onResume();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

}
