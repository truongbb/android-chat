package com.thucpn.ttsp.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.thucpn.ttsp.R;
import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.common.DatabaseUtil;
import com.thucpn.ttsp.common.ResourceUtil;
import com.thucpn.ttsp.model.ContactDto;
import com.thucpn.ttsp.model.ConversationDto;
import com.thucpn.ttsp.model.CreateConversationDto;
import com.thucpn.ttsp.model.FriendRequestDto;
import com.thucpn.ttsp.model.ParticipantDto;
import com.thucpn.ttsp.model.UserDto;
import com.thucpn.ttsp.service.ChatAPI;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.thucpn.ttsp.common.ResourceUtil.getContext;

public class ProfileActivity extends AppCompatActivity {
    private AppCompatActivity appCompatActivity;
    private ResourceUtil resourceUtil;
    private UserDto userDto_resourceUtil;
    private UserDto userDto;
    private int actionUser;
    private String activityParent;

    private Toolbar toolbarProfile;
    private CircleImageView imvAvatar_Profile;
    private TextView tvName_Profile;
    private TextView tvSex_Profile;
    private TextView tvBirthday_Profile;
    private TextView tvPhone_Profile;
    private TextView tvEmail_Profile;
    private TextView tvAddress_Profile;
    private Button btnAction_Profile;
    private Button btnCancel_Profile;

    private ChatAPI chatAPI;

    static {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_profile);
        init();
    }

    private void init() {
        appCompatActivity = ProfileActivity.this;
        resourceUtil = (ResourceUtil) getContext();
        userDto_resourceUtil = resourceUtil.userDto;
        actionUser = getIntent().getIntExtra(Constant.ACTION, Constant.ACTION_CHAT);
        activityParent = getIntent().getStringExtra(Constant.ACTIVITY_PARENT);

        toolbarProfile = findViewById(R.id.toolbarProfile);
        imvAvatar_Profile = findViewById(R.id.imvAvatar_Profile);
        tvName_Profile = findViewById(R.id.tvName_Profile);
        tvSex_Profile = findViewById(R.id.tvSex_Profile);
        tvBirthday_Profile = findViewById(R.id.tvBirthday_Profile);
        tvPhone_Profile = findViewById(R.id.tvPhone_Profile);
        tvEmail_Profile = findViewById(R.id.tvEmail_Profile);
        tvAddress_Profile = findViewById(R.id.tvAddress_Profile);
        btnAction_Profile = findViewById(R.id.btnAction_Profile);
        btnCancel_Profile = findViewById(R.id.btnCancel_Profile);

        setSupportActionBar(toolbarProfile);
        getSupportActionBar().setTitle(getResources().getString(R.string.profile));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        chatAPI = Constant.getChatAPI();

        switch (actionUser) {
            case Constant.ACTION_NONE:
                userDto = getIntent().getParcelableExtra(Constant.USER_SENT);
                initData(userDto);
                btnAction_Profile.setVisibility(View.INVISIBLE);
                btnAction_Profile.setClickable(false);
                break;
            case Constant.ACTION_CHAT:
                userDto = getIntent().getParcelableExtra(Constant.USER_SENT);
                initData(userDto);
                btnCancel_Profile.setClickable(false);
                btnCancel_Profile.setVisibility(View.GONE);
                btnAction_Profile.setText(getResources().getText(R.string.send_message));
                btnAction_Profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        List<ParticipantDto> arrParticipantDto = new ArrayList<>();
                        ParticipantDto participantDto = new ParticipantDto();
                        participantDto.setUserId(userDto.getUserId());
                        arrParticipantDto.add(participantDto);
                        CreateConversationDto createConversationDto = new CreateConversationDto();
                        createConversationDto.setCreatorId(userDto_resourceUtil.getUserId());
                        createConversationDto.setParticipants(arrParticipantDto);
                        createConversationDto.setTitle(null);
                        ChatAPI chatAPI = Constant.getChatAPI();
                        Call<CreateConversationDto> call = chatAPI.creatConversation(createConversationDto);
                        call.enqueue(new Callback<CreateConversationDto>() {
                            @Override
                            public void onResponse(Call<CreateConversationDto> call, Response<CreateConversationDto> response) {
                                if (response.isSuccessful()) {
                                    CreateConversationDto dto = response.body();
                                    Intent intent;
                                    ConversationDto conversationDto;
                                    switch (dto.getStatus()) {
                                        case Constant.EXSITED:
                                            resourceUtil.arrConversationHistorie = dto.getConversationHistory();
                                            intent = new Intent(appCompatActivity, ChatActivity.class);
                                            conversationDto = new ConversationDto();
                                            conversationDto.setTitle(dto.getTitle());
                                            conversationDto.setChatTypeId(dto.getChatTypeId());
                                            conversationDto.setId(dto.getConversationId());
                                            intent.putExtra(Constant.CONVERSATION, conversationDto);
                                            appCompatActivity.startActivity(intent);
                                            appCompatActivity.finish();
                                            break;
                                        case Constant.NEW_OK:
                                            resourceUtil.arrConversationHistorie = null;
                                            intent = new Intent(appCompatActivity, ChatActivity.class);
                                            conversationDto = new ConversationDto();
                                            conversationDto.setTitle(dto.getTitle());
                                            conversationDto.setChatTypeId(dto.getChatTypeId());
                                            conversationDto.setId(dto.getConversationId());
                                            intent.putExtra(Constant.CONVERSATION, conversationDto);
                                            appCompatActivity.startActivity(intent);
                                            appCompatActivity.finish();
                                            break;
                                        case Constant.NEW_NOT_OK:
                                            Toast.makeText(getApplicationContext(), "Khong thanh cong", Toast.LENGTH_SHORT).show();
                                            break;
                                        case Constant.ERROR:
                                            Toast.makeText(getApplicationContext(), "Khong thanh cong", Toast.LENGTH_SHORT).show();
                                            break;
                                    }

                                } else {
                                    Toast.makeText(getApplicationContext(), "Khong thanh cong", Toast.LENGTH_SHORT).show();
                                    Log.e("creatConv onResponse", "not successful");
                                }
                            }

                            @Override
                            public void onFailure(Call<CreateConversationDto> call, Throwable t) {
                                Toast.makeText(getApplicationContext(), "Khong thanh cong", Toast.LENGTH_SHORT).show();
                                Log.e("creatConv onFailure", new Exception(t).getMessage());
                            }
                        });
                    }
                });
                break;
            case Constant.ACTION_FRIEND_REQUEST:
                userDto = getIntent().getParcelableExtra(Constant.USER_SENT);
                initData(userDto);
                btnCancel_Profile.setClickable(false);
                btnCancel_Profile.setVisibility(View.GONE);
                btnAction_Profile.setText(getResources().getText(R.string.send_friend_request));
                btnAction_Profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        FriendRequestDto friendRequestDto = new FriendRequestDto();
                        friendRequestDto.setUserId(userDto.getUserId());
                        friendRequestDto.setSenderId(userDto_resourceUtil.getUserId());
                        friendRequestDto.setUsername(userDto_resourceUtil.getUsername());
                        Date currentDate = new Date(System.currentTimeMillis());
                        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                        friendRequestDto.setRequestedTime(dateFormat.format(currentDate) + "#" + userDto_resourceUtil.getUserId() + "#" + userDto.getUserId());
                        ChatAPI chatAPI = Constant.getChatAPI();
                        Call<Boolean> call = chatAPI.sendFriendRequest(friendRequestDto);
                        call.enqueue(new Callback<Boolean>() {
                            @Override
                            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                if (response.isSuccessful()) {
                                    Boolean result = response.body();
                                    if (result) {
                                        Toast.makeText(v.getContext(), "Gửi thành công", Toast.LENGTH_SHORT).show();
                                        btnAction_Profile.setText(getResources().getText(R.string.sent_friend_request));
                                        btnAction_Profile.setClickable(false);
                                        List<UserDto> requestedList = resourceUtil.friendRequestDto.getRequestedList();
                                        if (requestedList != null) {
                                            requestedList.add(userDto);
                                        } else {
                                            requestedList = new ArrayList<>();
                                            requestedList.add(userDto);
                                        }
                                    } else {
                                        Toast.makeText(v.getContext(), "Gửi không thành công", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Log.e("send request onResponse", "not successful");
                                }
                            }

                            @Override
                            public void onFailure(Call<Boolean> call, Throwable t) {
                                Log.e("send request onFailure", new Exception(t).getMessage());
                            }
                        });
                    }
                });
                break;
            case Constant.ACTION_WAIT_FRIEND_REQUEST:
                userDto = getIntent().getParcelableExtra(Constant.USER_SENT);
                initData(userDto);
                btnCancel_Profile.setVisibility(View.VISIBLE);
                btnCancel_Profile.setClickable(true);
                btnCancel_Profile.setVisibility(View.VISIBLE);
                btnAction_Profile.setText(getResources().getText(R.string.accept));
                btnAction_Profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        ContactDto contactDto = new ContactDto();
                        contactDto.setUserId(userDto_resourceUtil.getUserId());
                        contactDto.setAccountId(userDto_resourceUtil.getAccountId());
                        contactDto.setUsername(userDto_resourceUtil.getUsername());
                        contactDto.setMemberId(userDto.getUserId());
                        Date currentDate = new Date(System.currentTimeMillis());
                        DateFormat dateFormat = new SimpleDateFormat("YYYYMMDD HH:MM:SS");
                        contactDto.setAddedTime(dateFormat.format(currentDate));
                        ChatAPI chatAPI = Constant.getChatAPI();
                        Call<Boolean> call = chatAPI.acceptFriendRequest(contactDto);
                        call.enqueue(new Callback<Boolean>() {
                            @Override
                            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                if (response.isSuccessful()) {
                                    Boolean result = response.body();
                                    if (result) {
                                        Toast.makeText(v.getContext(), "Thanh cong", Toast.LENGTH_SHORT).show();
                                        btnAction_Profile.setClickable(false);
                                        btnAction_Profile.setVisibility(View.INVISIBLE);
                                        for (int i = 0; i < resourceUtil.friendRequestDto.getRequestedList().size(); i++) {
                                            if (resourceUtil.friendRequestDto.getRequestedList().get(i).getUserId() == userDto.getUserId()) {
                                                resourceUtil.friendRequestDto.getRequestedList().remove(i);
                                                resourceUtil.contactDto.getContactList().add(userDto);
                                            }
                                        }
                                        btnCancel_Profile.setVisibility(View.GONE);
                                        btnCancel_Profile.setClickable(false);
                                    } else {
                                        Toast.makeText(v.getContext(), "Khong thanh cong", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Log.e("accept_pro onResponse", "not successful");
                                }
                            }

                            @Override
                            public void onFailure(Call<Boolean> call, Throwable t) {
                                Log.e("accept_pro onFailure", new Exception(t).getMessage());
                            }
                        });
                    }
                });
                btnCancel_Profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        FriendRequestDto friendRequestDto = new FriendRequestDto();
                        friendRequestDto.setSenderId(userDto_resourceUtil.getUserId());
                        friendRequestDto.setUserId(userDto.getUserId());
                        friendRequestDto.setAccountId(userDto_resourceUtil.getAccountId());
                        ChatAPI chatAPI = Constant.getChatAPI();
                        Call<Boolean> call = chatAPI.cancelFriendRequest(friendRequestDto);
                        call.enqueue(new Callback<Boolean>() {
                            @Override
                            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                if (response.isSuccessful()) {
                                    Boolean result = response.body();
                                    if (result) {
                                        Toast.makeText(v.getContext(), "thanh cong", Toast.LENGTH_SHORT).show();
                                        btnCancel_Profile.setVisibility(View.GONE);
                                        btnCancel_Profile.setClickable(false);
                                        for (int i = 0; i < resourceUtil.friendRequestDto.getRequestedList().size(); i++) {
                                            if (resourceUtil.friendRequestDto.getRequestedList().get(i).getUserId() == userDto.getUserId()) {
                                                resourceUtil.friendRequestDto.getRequestedList().remove(i);
                                            }
                                        }
                                        btnAction_Profile.setClickable(false);
                                        btnAction_Profile.setVisibility(View.INVISIBLE);
                                    } else {
                                        Toast.makeText(v.getContext(), "Khong thanh cong", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Log.e("cancelFriend onResponse", "not successful");
                                }
                            }

                            @Override
                            public void onFailure(Call<Boolean> call, Throwable t) {
                                Log.e("cancelFriend onFailure", new Exception(t).getMessage());
                            }
                        });
                    }
                });
                break;
            case Constant.ACTION_CHANGE_PROFILE:
                initData(userDto_resourceUtil);
                btnAction_Profile.setText(getResources().getText(R.string.change_info));
                btnAction_Profile.setOnClickListener(new View.OnClickListener() {
                    Dialog dialogChangeProfile;
                    DatePickerDialog datePickerDialog;
                    Calendar calendar = Calendar.getInstance();
                    String textBirthDay;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyy");
                    Date birthDay;
                    ChatAPI chatAPI = Constant.getChatAPI();

                    @Override
                    public void onClick(final View v) {
                        dialogChangeProfile = new Dialog(v.getContext(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth);
                        dialogChangeProfile.setContentView(R.layout.layout_change_info);
                        dialogChangeProfile.setCancelable(true);
                        dialogChangeProfile.show();

                        final EditText edtLastName_ChangeInfo = dialogChangeProfile.findViewById(R.id.edtLastName_ChangeInfo);
                        final EditText edtFirstName_ChangeInfo = dialogChangeProfile.findViewById(R.id.edtFirstName_ChangeInfo);
                        final RadioButton rbMale_ChangeInfo = dialogChangeProfile.findViewById(R.id.rbMale_ChangeInfo);
                        final RadioButton rbFemale_ChangeInfo = dialogChangeProfile.findViewById(R.id.rbFemale_ChangeInfo);
                        final RadioButton rbUnknow_ChangeInfo = dialogChangeProfile.findViewById(R.id.rbUnknow_ChangeInfo);
                        final TextView tvBirthday_ChangeInfo = dialogChangeProfile.findViewById(R.id.tvBirthday_ChangeInfo);
                        ImageView imgBirthdayPicker_ChangeInfo = dialogChangeProfile.findViewById(R.id.imgBirthdayPicker_ChangeInfo);
                        final EditText edtEmail_ChangeInfo = dialogChangeProfile.findViewById(R.id.edtEmail_ChangeInfo);
                        final EditText edtPhone_ChangeInfo = dialogChangeProfile.findViewById(R.id.edtPhone_ChangeInfo);
                        final EditText edtAddress_ChangeInfo = dialogChangeProfile.findViewById(R.id.edtAddress_ChangeInfo);
                        Button btnCancel_ChangeInfo = dialogChangeProfile.findViewById(R.id.btnCancel_ChangeInfo);
                        Button btnSave_ChangeInfo = dialogChangeProfile.findViewById(R.id.btnSave_ChangeInfo);
                        final TextInputLayout tilLastname_ChangeInfo = dialogChangeProfile.findViewById(R.id.tilLastname_ChangeInfo);
                        final TextInputLayout tilFirstname_ChangeInfo = dialogChangeProfile.findViewById(R.id.tilFirstname_ChangeInfo);
                        final TextInputLayout tilEmail_ChangeInfo = dialogChangeProfile.findViewById(R.id.tilEmail_ChangeInfo);
                        final TextInputLayout tilPhone_ChangeInfo = dialogChangeProfile.findViewById(R.id.tilPhone_ChangeInfo);
                        final TextInputLayout tilAddress_ChangeInfo = dialogChangeProfile.findViewById(R.id.tilAddress_ChangeInfo);

                        if (userDto_resourceUtil != null) {
                            edtLastName_ChangeInfo.setText(userDto_resourceUtil.getLastName());
                            edtFirstName_ChangeInfo.setText(userDto_resourceUtil.getFirstName());
                            switch (userDto_resourceUtil.getGenderId() + "") {
                                case Constant.MALE + "":
                                    rbMale_ChangeInfo.setChecked(true);
                                    break;
                                case Constant.FEMALE + "":
                                    rbFemale_ChangeInfo.setChecked(true);
                                    break;
                                case Constant.UNKNOWN + "":
                                    rbUnknow_ChangeInfo.setChecked(true);
                                    break;
                            }
                            tvBirthday_ChangeInfo.setText(getResources().getText(R.string.birthday) + ": \t" + userDto_resourceUtil.getBirthdayStr());
                            edtEmail_ChangeInfo.setText(userDto_resourceUtil.getEmail());
                            edtPhone_ChangeInfo.setText(userDto_resourceUtil.getPhone());
                            edtAddress_ChangeInfo.setText(userDto_resourceUtil.getAddress());
                            tvBirthday_ChangeInfo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    datePickerDialog = new DatePickerDialog(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                                        @Override
                                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                            calendar.set(year, month, dayOfMonth);
                                            birthDay = calendar.getTime();
                                            textBirthDay = dateFormat.format(birthDay);
                                            tvBirthday_ChangeInfo.setText(getResources().getText(R.string.birthday) + ": " + textBirthDay);
                                        }
                                    }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
                                    datePickerDialog.show();
                                }
                            });
                            imgBirthdayPicker_ChangeInfo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    datePickerDialog = new DatePickerDialog(v.getContext(), (view, year, month, dayOfMonth) -> {
                                        calendar.set(year, month, dayOfMonth);
                                        birthDay = calendar.getTime();
                                        textBirthDay = dateFormat.format(birthDay);
                                        tvBirthday_ChangeInfo.setText(getResources().getText(R.string.birthday) + ": " + textBirthDay);
                                    }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
                                    datePickerDialog.show();
                                }
                            });
                        }
                        btnCancel_ChangeInfo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogChangeProfile.dismiss();
                            }
                        });
                        btnSave_ChangeInfo.setOnClickListener(new View.OnClickListener() {
                            String lastName;
                            String firstName;
                            String email;
                            String phone;
                            String address;

                            @Override
                            public void onClick(View v) {
                                if (checkInput()) {
                                    UserDto dto = userDto_resourceUtil;
                                    dto.setLastName(lastName);
                                    dto.setFirstName(firstName);
                                    dto.setBirthday(null);
                                    if (rbUnknow_ChangeInfo.isChecked()) {
                                        dto.setGender(Constant.UNKNOWN_STRING);
                                        dto.setGenderId(Constant.UNKNOWN);
                                    }
                                    if (rbFemale_ChangeInfo.isChecked()) {
                                        dto.setGender(Constant.FEMALE_STRING);
                                        dto.setGenderId(Constant.FEMALE);
                                    } else {
                                        dto.setGender(Constant.MALE_STRING);
                                        dto.setGenderId(Constant.MALE);
                                    }
                                    dto.setEmail(email);
                                    dto.setPhone(phone);
                                    dto.setAddress(address);
                                    String[] splitBirthday = textBirthDay.split(":");
                                    dto.setBirthdayStr(splitBirthday[splitBirthday.length - 1]);
                                    loadAnswersChangeProfile(dto);
                                }
                            }

                            private void loadAnswersChangeProfile(UserDto dto) {
                                Call<Boolean> call = chatAPI.changeProfile(dto);
                                call.enqueue(new Callback<Boolean>() {
                                    @Override
                                    public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                        if (response.isSuccessful()) {
                                            Boolean result = response.body();
                                            if (result) {
                                                String[] birthdayStrList = tvBirthday_ChangeInfo.getText().toString().split(":");
                                                int lenght = birthdayStrList.length;
                                                try {
                                                    birthDay = new SimpleDateFormat("dd-MM-yyyy").parse(birthdayStrList[lenght - 1].trim());
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }
                                                dto.setBirthday(birthDay);
                                                resourceUtil.userDto.setFirstName(dto.getFirstName());
                                                resourceUtil.userDto.setLastName(dto.getLastName());
                                                resourceUtil.userDto.setBirthday(dto.getBirthday());
                                                resourceUtil.userDto.setGender(dto.getGender());
                                                resourceUtil.userDto.setGenderId(dto.getGenderId());
                                                resourceUtil.userDto.setEmail(dto.getEmail());
                                                resourceUtil.userDto.setPhone(dto.getPhone());
                                                resourceUtil.userDto.setAddress(dto.getAddress());
                                                resourceUtil.userDto.setBirthdayStr(dto.getBirthdayStr());
                                                DatabaseUtil databaseUtil = DatabaseUtil.getInstance(ProfileActivity.this);
                                                databaseUtil.updateUser(resourceUtil.userDto);
                                                initData(dto);
                                                dialogChangeProfile.dismiss();
                                                Toast.makeText(v.getContext(), "Thay đổi thành công", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(v.getContext(), "Thay đổi không thành công", Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            Toast.makeText(v.getContext(), "Thay đổi không thành công", Toast.LENGTH_SHORT).show();
                                            Log.e("changeInfo onResponse", "not successful");
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Boolean> call, Throwable t) {
                                        Toast.makeText(v.getContext(), "Thay đổi không thành công", Toast.LENGTH_SHORT).show();
                                        Log.e("changeInfo onFailure", new Exception(t).getMessage());
                                    }
                                });
                            }

                            private boolean checkInput() {
                                firstName = edtFirstName_ChangeInfo.getText().toString().trim();
                                lastName = edtLastName_ChangeInfo.getText().toString().trim();
                                email = edtEmail_ChangeInfo.getText().toString().trim();
                                phone = edtPhone_ChangeInfo.getText().toString().trim();
                                address = edtAddress_ChangeInfo.getText().toString().trim();
                                textBirthDay = tvBirthday_ChangeInfo.getText().toString().trim();
                                if (lastName == null || lastName.length() == 0) {
                                    tilLastname_ChangeInfo.setError(getResources().getString(R.string.last_name) + " " + getResources().getString(R.string.not_valid));
                                    return false;
                                }
                                if (firstName == null || firstName.length() == 0) {
                                    tilFirstname_ChangeInfo.setError(getResources().getString(R.string.first_name) + " " + getResources().getString(R.string.not_valid));
                                    return false;
                                }
                                if (textBirthDay == null || textBirthDay.length() == 0 || textBirthDay.equals(getResources().getString(R.string.birthday))) {
                                    tvBirthday_ChangeInfo.setText(getResources().getText(R.string.birthday));
                                    tvBirthday_ChangeInfo.setTextColor(getResources().getColor(R.color.color_error_edittext));
                                    return false;
                                }
                                //Email
                                if (!checkInputEmail(email)) {
                                    tilEmail_ChangeInfo.setError(getResources().getString(R.string.email) + " " + getResources().getString(R.string.not_valid));
                                    return false;
                                }

                                //phone
                                if (!checkInputPhoneNumber(phone)) {
                                    tilPhone_ChangeInfo.setError(getResources().getString(R.string.phone) + " " + getResources().getString(R.string.not_valid));
                                    return false;
                                }
                                if (address == null || address.length() == 0) {
                                    tilAddress_ChangeInfo.setError(getResources().getString(R.string.address) + " " + getResources().getString(R.string.not_valid));
                                    return false;
                                }
                                return true;
                            }

                            private boolean checkInputEmail(String email) {
                                if (email == null || email.length() == 0) {
                                    return false;
                                }
                                String emailPattern = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
                                Pattern regex = Pattern.compile(emailPattern);
                                Matcher matcher = regex.matcher(email);
                                if (matcher.find()) {
                                    return true;
                                } else {
                                    return false;
                                }
                            }

                            private boolean checkInputPhoneNumber(String phone) {
                                Pattern pattern = Pattern.compile("^[0-9]*$");
                                Matcher matcher = pattern.matcher(phone);
                                if (!matcher.matches()) {
                                    return false;
                                } else if (phone.length() == 10 || phone.length() == 11) {
                                    if (phone.length() == 10) {
                                        if (phone.substring(0, 2).equals("09")) {
                                            return true;
                                        } else {
                                            return false;
                                        }
                                    } else if (phone.substring(0, 2).equals("01")) {
                                        return true;
                                    } else {
                                        return false;
                                    }
                                } else {
                                    return false;
                                }
                            }
                        });
                    }
                });
                break;
            case Constant.ACTION_SENT_FRIEND_REQUEST:
                userDto = getIntent().getParcelableExtra(Constant.USER_SENT);
                initData(userDto);
                btnCancel_Profile.setVisibility(View.GONE);
                btnAction_Profile.setText(getResources().getText(R.string.sent_friend_request));
                btnAction_Profile.setClickable(false);
                break;
        }

    }

    private void initData(UserDto userDto) {
        if (Constant.UNKNOWN_STRING.equals(userDto.getGender())) {

        } else {
            if (Constant.MALE_STRING.equals(userDto.getGender())) {
                imvAvatar_Profile.setImageResource(R.drawable.img_male_avatar);
            } else {
                imvAvatar_Profile.setImageResource(R.drawable.img_female_avatar);
            }
        }
        tvName_Profile.setText(userDto.getFullName());
        tvSex_Profile.setText(userDto.getGender());
        tvBirthday_Profile.setText(new SimpleDateFormat("dd-MM-yyyy").format(userDto.getBirthday()));
        tvAddress_Profile.setText(userDto.getAddress());
        tvPhone_Profile.setText(userDto.getPhone());
        tvEmail_Profile.setText(userDto.getEmail());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (activityParent.equals(Constant.SEARCH_USER_ACTIVITY)) {
                ChatAPI chatAPI = Constant.getChatAPI();
                UserDto userDto = new UserDto();
                userDto.setFirstName(resourceUtil.keySearchUserDto);
                userDto.setUserId(resourceUtil.userDto.getUserId());
                Call<List<UserDto>> call = chatAPI.searchUser(userDto);
                call.enqueue(new Callback<List<UserDto>>() {
                    public List<UserDto> arrResult;

                    @Override
                    public void onResponse(Call<List<UserDto>> call, Response<List<UserDto>> response) {
                        if (response.isSuccessful()) {
                            arrResult = response.body();
                            if (arrResult == null || arrResult.size() == 0) {
                                Log.e("searchUser onResponse", "successfull-null");
                            } else {
                                Intent intent = new Intent(getContext(), SearchUserActivity.class);
                                intent.putParcelableArrayListExtra(Constant.SEARCH_USER_RESULT, (ArrayList<UserDto>) arrResult);
                                startActivity(intent);
                                appCompatActivity.finish();
                            }
                        } else {
                            Toast.makeText(getContext(), "Tìm kiếm thất bại", Toast.LENGTH_SHORT).show();
                            Log.e("searchUser onResponse", "not successfull");
                        }
                    }

                    @Override
                    public void onFailure(Call<List<UserDto>> call, Throwable t) {
                        Toast.makeText(getContext(), "Tìm kiếm thất bại", Toast.LENGTH_SHORT).show();
                        Log.e("searchUser onFailure", new Exception(t).getMessage());
                    }
                });
                this.finish();
            } else {
                switch (actionUser) {
                    case Constant.ACTION_NONE:
                        super.onBackPressed();
                        this.finish();
                        break;
                    case Constant.ACTION_CHANGE_PROFILE:
                        Intent intent = new Intent(this, MainActivity.class);
                        intent.putExtra(Constant.REQUEST_ACTION, Constant.REQUEST_RESULT_CHANGE_PROFILE);
                        setResult(RESULT_OK, intent);
                        this.finish();
                        break;
                    case Constant.ACTION_CHAT:
                        intent = new Intent(this, MainActivity.class);
                        intent.putExtra(Constant.REQUEST_ACTION, Constant.REQUEST_RESULT_CONTACT);
                        setResult(RESULT_OK, intent);
                        this.finish();
                        break;
                    case Constant.ACTION_WAIT_FRIEND_REQUEST:
                        intent = new Intent(this, MainActivity.class);
                        intent.putExtra(Constant.REQUEST_ACTION, Constant.REQUEST_RESULT_FRIEND_REQUEST);
                        setResult(RESULT_OK, intent);
                        this.finish();
                        break;
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (activityParent.equals(Constant.SEARCH_USER_ACTIVITY)) {
            ChatAPI chatAPI = Constant.getChatAPI();
            UserDto userDto = new UserDto();
            userDto.setFirstName(resourceUtil.keySearchUserDto);
            userDto.setUserId(resourceUtil.userDto.getUserId());
            Call<List<UserDto>> call = chatAPI.searchUser(userDto);
            call.enqueue(new Callback<List<UserDto>>() {
                public List<UserDto> arrResult;

                @Override
                public void onResponse(Call<List<UserDto>> call, Response<List<UserDto>> response) {
                    if (response.isSuccessful()) {
                        arrResult = response.body();
                        if (arrResult == null || arrResult.size() == 0) {
                            Log.e("searchUser onResponse", "successfull-null");
                        } else {
                            Intent intent = new Intent(getContext(), SearchUserActivity.class);
                            intent.putParcelableArrayListExtra(Constant.SEARCH_USER_RESULT, (ArrayList<UserDto>) arrResult);
                            startActivity(intent);
                            appCompatActivity.finish();
                        }
                    } else {
                        Toast.makeText(getContext(), "Tìm kiếm thất bại", Toast.LENGTH_SHORT).show();
                        Log.e("searchUser onResponse", "not successfull");
                    }
                }

                @Override
                public void onFailure(Call<List<UserDto>> call, Throwable t) {
                    Toast.makeText(getContext(), "Tìm kiếm thất bại", Toast.LENGTH_SHORT).show();
                    Log.e("searchUser onFailure", new Exception(t).getMessage());
                }
            });
            this.finish();
        } else {
            switch (actionUser) {
                case Constant.ACTION_NONE:
                    super.onBackPressed();
                    this.finish();
                    break;
                case Constant.ACTION_CHANGE_PROFILE:
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.putExtra(Constant.REQUEST_ACTION, Constant.REQUEST_RESULT_CHANGE_PROFILE);
                    setResult(RESULT_OK, intent);
                    this.finish();
                    break;
                case Constant.ACTION_CHAT:
                    intent = new Intent(this, MainActivity.class);
                    intent.putExtra(Constant.REQUEST_ACTION, Constant.REQUEST_RESULT_CONTACT);
                    setResult(RESULT_OK, intent);
                    this.finish();
                    break;
                case Constant.ACTION_WAIT_FRIEND_REQUEST:
                    intent = new Intent(this, MainActivity.class);
                    intent.putExtra(Constant.REQUEST_ACTION, Constant.REQUEST_RESULT_FRIEND_REQUEST);
                    setResult(RESULT_OK, intent);
                    this.finish();
                    break;
            }
        }
    }

}
