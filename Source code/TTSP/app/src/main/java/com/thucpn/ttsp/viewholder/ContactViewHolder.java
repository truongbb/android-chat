package com.thucpn.ttsp.viewholder;

import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thucpn.ttsp.R;

import de.hdodenhof.circleimageview.CircleImageView;
import lombok.Getter;

/**
 * Created by thucpn on 23/03/2018.
 */
@Getter
public class ContactViewHolder extends RecyclerView.ViewHolder {
    private CircleImageView avatarItemContact;
    private TextView tvUserItemContact;
    private CheckBox cbSelectUser_NewConversation;

    public ContactViewHolder(View itemView) {
        super(itemView);
        avatarItemContact = itemView.findViewById(R.id.avatarItemContact);
        tvUserItemContact = itemView.findViewById(R.id.tvUserItemContact);
//        cbSelectUser_NewConversation = itemView.findViewById(R.id.cbSelectUser_NewConversation);
    }
}
