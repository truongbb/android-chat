package com.thucpn.ttsp.fragment;

import android.app.DatePickerDialog;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.thucpn.ttsp.R;
import com.thucpn.ttsp.activity.BeginActivity;
import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.model.UserDto;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by thucpn on 10/03/2018.
 */

public class RegisterFragment extends BaseFragment implements View.OnClickListener {
    private EditText edtUsername;
    private EditText edtPassword;
    private EditText edtLastName;
    private EditText edtFirstName;
    private TextView tvBirthday;
    private EditText edtEmail;
    private EditText edtPhone;
    private EditText edtAddress;
    private ImageView imgBirthdayPicker;
    private RadioButton rbMaleRegister;
    private RadioButton rbFemaleRegister;
    private RadioButton rbUnknowRegister;
    private Button btnRegister;
    private TextInputLayout tilUsername_register;
    private TextInputLayout tilPassword_register;
    private TextInputLayout tilLastname_register;
    private TextInputLayout tilFirstname_register;
    private TextInputLayout tilEmail_register;
    private TextInputLayout tilPhone_register;
    private TextInputLayout tilAddress_register;

    private UserDto userDto;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String textBirthDay;
    private Date birthDay;
    private String email;
    private String phone;
    private String address;
    private String gender;
    private long genderId;
    private DatePickerDialog datePickerDialog;

    @Override
    protected int getViewID() {
        return R.layout.layout_register;
    }

    @Override
    protected void init() {
        edtUsername = getActivity().findViewById(R.id.edtUsername_register);
        edtPassword = getActivity().findViewById(R.id.edtPassword_register);
        edtLastName = getActivity().findViewById(R.id.edtLastName_register);
        edtFirstName = getActivity().findViewById(R.id.edtFirstName_register);
        tvBirthday = getActivity().findViewById(R.id.tvBirthday);
        imgBirthdayPicker = getActivity().findViewById(R.id.imgBirthdayPicker);
        rbMaleRegister = getActivity().findViewById(R.id.rbMaleRegister);
        rbFemaleRegister = getActivity().findViewById(R.id.rbFemaleRegister);
        rbUnknowRegister = getActivity().findViewById(R.id.rbUnknowRegister);
        edtEmail = getActivity().findViewById(R.id.edtEmail);
        edtPhone = getActivity().findViewById(R.id.edtPhone);
        edtAddress = getActivity().findViewById(R.id.edtAddress);
        btnRegister = getActivity().findViewById(R.id.btnRegister);
        tilUsername_register = getActivity().findViewById(R.id.tilUsername_register);
        tilPassword_register = getActivity().findViewById(R.id.tilPasswoord_register);
        tilLastname_register = getActivity().findViewById(R.id.tilLastName_register);
        tilFirstname_register = getActivity().findViewById(R.id.tilFirstName_register);
        tilEmail_register = getActivity().findViewById(R.id.tilEmail_register);
        tilPhone_register = getActivity().findViewById(R.id.tilPhone_register);
        tilAddress_register = getActivity().findViewById(R.id.tilAddress_register);

        imgBirthdayPicker.setOnClickListener(this);
        tvBirthday.setOnClickListener(this);
        btnRegister.setOnClickListener(this);

        hideErrorEditText();
    }

    private void loadAnswers() {
        chatAPI = Constant.getChatAPI();
        Call<UserDto> userDtoCall = chatAPI.register(userDto);
        userDtoCall.enqueue(new Callback<UserDto>() {
            @Override
            public void onResponse(Call<UserDto> call, Response<UserDto> response) {
                userDto = response.body();
                if (response.isSuccessful()) {
                    switch (userDto.getUserId() + "") {
                        case Constant.REGISTER_SUCCESSFULLY + "":
                            Toast.makeText(getContext(), "Register successfully!", Toast.LENGTH_SHORT).show();
                            BeginActivity beginActivity=new BeginActivity();
                            beginActivity.switchFragment(beginActivity.getLoginFragment());
                            break;
                        case Constant.EXISTED_ACCOUNT + "":
                            Toast.makeText(getContext(), "Existed account, try again!", Toast.LENGTH_SHORT).show();
                            break;
                        case Constant.EXISTED_PHONE_OR_EMAIL + "":
                            Toast.makeText(getContext(), "Existed phone or email, try again!", Toast.LENGTH_SHORT).show();
                            break;
                        case Constant.INVALID_PASSWORD + "":
                            Toast.makeText(getContext(), "Password must be 8 to 16 chacracter, try again!", Toast.LENGTH_SHORT).show();
                            break;
                        case Constant.ERROR + "":
                            Toast.makeText(getContext(), "There's some error in server, try again after some minutes!", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<UserDto> call, Throwable t) {
                Log.e("Connect api login: ", new Exception(t).getMessage());
                Toast.makeText(getContext(), "error connect api", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBirthdayPicker:
                initDatePickerDialog();
                break;
            case R.id.tvBirthday:
                initDatePickerDialog();
                break;
            case R.id.btnRegister:
                if (!checkTextInput()) {
                    break;
                } else {
                    hideErrorEditText();
                    if (rbUnknowRegister.isChecked()) {
                        gender = "Khác";
                        genderId = Constant.UNKNOWN;
                    }
                    if (rbMaleRegister.isChecked()) {
                        gender = "Nam";
                        genderId = Constant.MALE;
                    } else {
                        gender = "Nữ";
                        genderId = Constant.FEMALE;
                    }
                    userDto = new UserDto();
                    userDto.setUsername(username);
                    userDto.setPassword(password);
                    userDto.setLastName(lastName);
                    userDto.setFirstName(firstName);
                    userDto.setEmail(email);
                    userDto.setPhone(phone);
                    userDto.setAddress(address);
                    userDto.setGender(gender);
                    userDto.setGenderId(genderId);
                    userDto.setBirthday(null);
                    userDto.setBirthdayStr(new SimpleDateFormat("yyyy-MM-dd").format(birthDay));
                    loadAnswers();
                    break;
                }
        }

    }

    private void hideErrorEditText() {
        tilUsername_register.setError(null);
        tilPassword_register.setError(null);
        tilLastname_register.setError(null);
        tilFirstname_register.setError(null);
        tilEmail_register.setError(null);
        tilPhone_register.setError(null);
        tilAddress_register.setError(null);
    }

    private void initDatePickerDialog() {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyy");
        final Calendar calendar = Calendar.getInstance();
        final Calendar now = Calendar.getInstance();
        final Date dateNow = now.getTime();
        datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(year, month, dayOfMonth);
                birthDay = calendar.getTime();
                textBirthDay = dateFormat.format(birthDay);
                tvBirthday.setText(getContext().getResources().getText(R.string.birthday) + ": " + textBirthDay);
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
        datePickerDialog.show();
    }

    private boolean checkTextInput() {
        getTextInput();
        hideErrorEditText();
        //username
        if (username == null || username.length() == 0) {
            tilUsername_register.setError(getResources().getString(R.string.username) + " " + getResources().getString(R.string.not_valid));
            return false;
        }

        //password
        if (password == null || password.length() < 8 || password.length() > 16) {
            tilPassword_register.setError(getResources().getString(R.string.password) + " " + getResources().getString(R.string.not_valid));
            return false;
        }

        //lastName
        if (lastName == null || lastName.length() == 0) {
            tilLastname_register.setError(getResources().getString(R.string.last_name) + " " + getResources().getString(R.string.not_valid));
            return false;
        }

        //firstName
        if (firstName == null || firstName.length() == 0) {
            tilFirstname_register.setError(getResources().getString(R.string.first_name) + " " + getResources().getString(R.string.not_valid));
            return false;
        }

        //textBirthday
        if (textBirthDay == null || textBirthDay.length() == 0 || textBirthDay.equals(getResources().getString(R.string.birthday))) {
            tvBirthday.setText(getResources().getText(R.string.birthday));
            tvBirthday.setTextColor(getResources().getColor(R.color.color_error_edittext));
            return false;
        }

        //Email
        if (!checkInputEmail(email)) {
            tilEmail_register.setError(getResources().getString(R.string.email) + " " + getResources().getString(R.string.not_valid));
            return false;
        }

        //phone
        if (!checkInputPhoneNumber(phone)) {
            tilPhone_register.setError(getResources().getString(R.string.phone) + " " + getResources().getString(R.string.not_valid));
            return false;
        }
        //address
        if (address == null || address.length() == 0) {
            tilAddress_register.setError(getResources().getString(R.string.address) + " " + getResources().getString(R.string.not_valid));
            return false;
        }
        return true;
    }

    private boolean checkInputPhoneNumber(String phone) {
        Pattern pattern = Pattern.compile("^[0-9]*$");
        Matcher matcher = pattern.matcher(phone);
        if (!matcher.matches()) {
            return false;
        } else if (phone.length() == 10 || phone.length() == 11) {
            if (phone.length() == 10) {
                if (phone.substring(0, 2).equals("09")) {
                    return true;
                } else {
                    return false;
                }
            } else if (phone.substring(0, 2).equals("01")) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private boolean checkInputEmail(String email) {
        if (email == null || email.length() == 0) {
            return false;
        }
        String emailPattern = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
        Pattern regex = Pattern.compile(emailPattern);
        Matcher matcher = regex.matcher(email);
        if (matcher.find()) {
            return true;
        }
        return false;
    }

    private void getTextInput() {
        username = edtUsername.getText().toString().trim();
        password = edtPassword.getText().toString().trim();
        firstName = edtFirstName.getText().toString().trim();
        lastName = edtLastName.getText().toString().trim();
        email = edtEmail.getText().toString().trim();
        phone = edtPhone.getText().toString().trim();
        address = edtAddress.getText().toString().trim();
        textBirthDay = tvBirthday.getText().toString().trim();
    }

}
