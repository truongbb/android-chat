package com.thucpn.ttsp.viewholder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.thucpn.ttsp.R;

import de.hdodenhof.circleimageview.CircleImageView;
import lombok.Getter;

@Getter
public class MessageLeftViewHolder extends RecyclerView.ViewHolder {
    private CircleImageView imvAvatar_item_message_left;
    private TextView tvSender_item_message_left;
    private TextView tvContent_item_message_left;
    private TextView tvTime_item_message_left;

    public MessageLeftViewHolder(View itemView) {
        super(itemView);
        imvAvatar_item_message_left = itemView.findViewById(R.id.imvAvatar_item_message_left);
        tvSender_item_message_left = itemView.findViewById(R.id.tvSender_item_message_left);
        tvContent_item_message_left = itemView.findViewById(R.id.tvContent_item_message_left);
        tvTime_item_message_left = itemView.findViewById(R.id.tvTime_item_message_left);
    }

}
