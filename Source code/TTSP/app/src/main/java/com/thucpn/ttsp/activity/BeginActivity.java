package com.thucpn.ttsp.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.thucpn.ttsp.R;
import com.thucpn.ttsp.common.MySharedPreferences;
import com.thucpn.ttsp.fragment.ListConversationFragment;
import com.thucpn.ttsp.fragment.LoginFragment;
import com.thucpn.ttsp.fragment.RegisterFragment;

import lombok.Getter;

/**
 * Created by thucpn on 20/02/2018.
 */
@Getter
public class BeginActivity extends AppCompatActivity implements Runnable {
    private LoginFragment loginFragment;
    private RegisterFragment registerFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_begin);
        Thread thread = new Thread(this);
        initFragment();
    }

    private void initFragment() {
        loginFragment = new LoginFragment();
        registerFragment = new RegisterFragment();

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.begin_layout, loginFragment);
        fragmentTransaction.add(R.id.begin_layout, registerFragment);

        fragmentTransaction.commit();
        MySharedPreferences mySharedPreferences = new MySharedPreferences(getBaseContext());
        switchFragment(loginFragment);
    }

    public void switchFragment(Fragment fragment) {
        hideAllFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        fragmentTransaction.show(fragment);
        fragmentTransaction.commit();
    }

    private void hideAllFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.hide(loginFragment);
        fragmentTransaction.hide(registerFragment);

        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if (loginFragment.isVisible()) {
            System.exit(0);
        } else {
            switchFragment(loginFragment);
        }
    }

    @Override
    public void run() {
        if (!isNetworkAvailable()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setMessage("Bạn chưa bật kết nối Internet")
                    .setNegativeButton("Ok", (dialogInterface, i) -> dialogInterface.dismiss());
            builder.create();
            builder.show();
            return;
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Log.e("Thread internet", e.getMessage());
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
