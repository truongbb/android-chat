package com.thucpn.ttsp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.thucpn.ttsp.R;
import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.common.ResourceUtil;
import com.thucpn.ttsp.model.UserDto;
import com.thucpn.ttsp.viewholder.UserNewConversationViewHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListUserNewConversationAdapter extends RecyclerView.Adapter<UserNewConversationViewHolder> {
    private List<UserDto> arrUserDto;
    private List<UserDto> resultChecked = new ArrayList<>();
    private ResourceUtil resourceUtil = (ResourceUtil) ResourceUtil.getContext();

    @Override
    public UserNewConversationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_user_new_conversation, parent, false);
        return new UserNewConversationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final UserNewConversationViewHolder holder, final int position) {
        UserDto userDto = arrUserDto.get(position);
        holder.getTvUserItem_NewConversation().setText(userDto.getFirstName() + " " + userDto.getLastName());
        if (Constant.UNKNOWN_STRING.equals(userDto.getGender())) {

        } else {
            if (Constant.MALE_STRING.equals(userDto.getGender())) {
                holder.getAvatarUser_NewConversation().setImageResource(R.drawable.img_male_avatar);
            } else {
                holder.getAvatarUser_NewConversation().setImageResource(R.drawable.img_female_avatar);
            }
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < resourceUtil.contactDto.getContactList().size(); i++) {
                    if (resourceUtil.contactDto.getContactList().get(i).getUserId() == arrUserDto.get(position).getUserId()) {
                        if (resourceUtil.contactDto.getContactList().get(i).isChecked()) {
                            resourceUtil.contactDto.getContactList().get(i).setChecked(false);
                            holder.getCbSelectUser_NewConversation().setChecked(false);
                        } else {
                            resourceUtil.contactDto.getContactList().get(i).setChecked(true);
                            holder.getCbSelectUser_NewConversation().setChecked(true);
                        }
                    }
                }
//                if (arrUserDto.get(position).isChecked()) {
//                    arrUserDto.get(position).setChecked(false);
//                    for (int i = 0; i < resultChecked.size(); i++) {
//                        if (arrUserDto.get(position).getUserId() == resultChecked.get(i).getUserId()) {
//                            resultChecked.remove(i);
//                        }
//                    }
//                    Toast.makeText(holder.itemView.getContext(), resultChecked.size() + "", Toast.LENGTH_SHORT).show();
//                } else {
//                    arrUserDto.get(position).setChecked(true);
//                    holder.getCbSelectUser_NewConversation().setChecked(true);
//                    resultChecked.add(arrUserDto.get(position));
//                    Toast.makeText(holder.itemView.getContext(), resultChecked.size() + "", Toast.LENGTH_SHORT).show();
//                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrUserDto.size();
    }
}
