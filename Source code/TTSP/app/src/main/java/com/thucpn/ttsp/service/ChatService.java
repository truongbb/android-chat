package com.thucpn.ttsp.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.common.DatabaseUtil;
import com.thucpn.ttsp.common.ResourceUtil;
import com.thucpn.ttsp.model.ContactDto;
import com.thucpn.ttsp.model.ConversationDto;
import com.thucpn.ttsp.model.FriendRequestDto;
import com.thucpn.ttsp.model.LastSeen;
import com.thucpn.ttsp.model.UserDto;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatService extends Service {
    private Handler handler = new Handler();
    private Runnable runnable = () -> {
        DatabaseUtil databaseUtil = DatabaseUtil.getInstance(getBaseContext());
        ResourceUtil resourceUtil = (ResourceUtil) ResourceUtil.getContext();
        ChatAPI chatAPI = Constant.getChatAPI();
        UserDto dto = new UserDto();
        dto.setUserId(resourceUtil.userDto.getUserId());
        dto.setAccountId(resourceUtil.userDto.getAccountId());
        Call<List<ConversationDto>> callArrConversation = chatAPI.getAllConversationOfUser(dto);
        callArrConversation.enqueue(new Callback<List<ConversationDto>>() {
            @Override
            public void onResponse(Call<List<ConversationDto>> call, Response<List<ConversationDto>> response) {
                if (response.isSuccessful()) {
                    List<ConversationDto> arrConversationDto = response.body();
                    resourceUtil.arrConversationDto = arrConversationDto;
//                    for (int i = resourceUtil.arrConversationDto.size() - 1; i >= 0; i--) {
//                        LastSeen lastSeen = databaseUtil.getLastSeenByConversationId(resourceUtil.arrConversationDto.get(i).getId());
//                        if (lastSeen.getTime() < resourceUtil.arrConversationDto.get(i).getUpdatedTimeLong()) {
//                            resourceUtil.arrConversationDto.get(i).setSeen(false);
//                            resourceUtil.arrConversationDto.add(0, resourceUtil.arrConversationDto.get(i));
//                            resourceUtil.arrConversationDto.remove(i);
//                            lastSeen.setTime(resourceUtil.arrConversationDto.get(i).getUpdatedTimeLong());
//                            databaseUtil.updateLastSeen(lastSeen);
//                        } else {
//                            resourceUtil.arrConversationDto.get(i).setSeen(true);
//                        }
//                    }
                } else {
                    Log.e("Call all conversation", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<ConversationDto>> call, Throwable t) {
                Log.e("ConnectAPI all conver", new Exception(t).getMessage());
            }
        });

        Call<ContactDto> callAllContact = chatAPI.getAllContact(dto);
        callAllContact.enqueue(new Callback<ContactDto>() {
            @Override
            public void onResponse(Call<ContactDto> call, Response<ContactDto> response) {
                if (response.isSuccessful()) {
                    ContactDto contactDto = response.body();
                    resourceUtil.contactDto = contactDto;
                    if (contactDto.getContactList() != null && !contactDto.getContactList().isEmpty()) {
                        for (int i = 0; i < resourceUtil.contactDto.getContactList().size(); i++) {
                            resourceUtil.contactDto.getContactList().get(i).setChecked(false);
                        }
                    }
                } else {
                    Log.e("GetAllCont onResponse", "not successful");
                }
            }

            @Override
            public void onFailure(Call<ContactDto> call, Throwable t) {
                Log.e("GetAllCont onFailure", new Exception(t).getMessage());
            }
        });

        Call<FriendRequestDto> callFriendRequest = chatAPI.getAllFriendRequest(dto);
        callFriendRequest.enqueue(new Callback<FriendRequestDto>() {
            @Override
            public void onResponse(Call<FriendRequestDto> call, Response<FriendRequestDto> response) {
                if (response.isSuccessful()) {
                    FriendRequestDto friendRequestDto = response.body();
                    resourceUtil.friendRequestDto = friendRequestDto;
                } else {
                    Log.e("GetAllFriend onResponse", "not successful");
                }
            }

            @Override
            public void onFailure(Call<FriendRequestDto> call, Throwable t) {
                Log.e("GetAllFriend onFailure", new Exception(t).getMessage());
            }
        });

    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
//        return new MyBinder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handler.postDelayed(runnable, 1000);
        return START_STICKY;
    }

//    public class MyBinder extends Binder {
//        public ChatService getService() {
//            return ChatService.this;
//        }
//    }
}
