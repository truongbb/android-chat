package com.thucpn.ttsp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.thucpn.ttsp.R;
import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.model.UserDto;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddUserAdapter extends ArrayAdapter<UserDto> {
    private LayoutInflater inflater;
    private ArrayList<UserDto> arrUserDto;

    public AddUserAdapter(@NonNull Context context, @NonNull ArrayList<UserDto> objects) {
        super(context, android.R.layout.simple_list_item_multiple_choice, objects);
        arrUserDto = objects;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View v, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if (v == null) {
            v = inflater.inflate(R.layout.item_user_new_conversation, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.avatarUser_NewConversation = v.findViewById(R.id.avatarUser_NewConversation);
            viewHolder.tvUserItem_NewConversation = v.findViewById(R.id.tvUserItem_NewConversation);
            viewHolder.imgChecked_NewConversation = v.findViewById(R.id.imgChecked_NewConversation);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) v.getTag();
        }
        final UserDto userDto = arrUserDto.get(position);
        viewHolder.tvUserItem_NewConversation.setText(userDto.getFullName());
        if (Constant.UNKNOWN_STRING.equals(userDto.getGender())) {

        } else {
            if (Constant.MALE_STRING.equals(userDto.getGender())) {
                viewHolder.avatarUser_NewConversation.setImageResource(R.drawable.img_male_avatar);
            } else {
                viewHolder.avatarUser_NewConversation.setImageResource(R.drawable.img_female_avatar);
            }
        }
        if (userDto.isChecked()) {
            viewHolder.imgChecked_NewConversation.setVisibility(View.VISIBLE);
        } else {
            viewHolder.imgChecked_NewConversation.setVisibility(View.INVISIBLE);
        }
        return v;
    }

    public class ViewHolder {
        public CircleImageView avatarUser_NewConversation;
        public TextView tvUserItem_NewConversation;
        public ImageView imgChecked_NewConversation;
    }
}
