package com.thucpn.ttsp.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConversationHistoryDto {

    @SerializedName("conversationId")
    @Expose
    private Long conversationId;

    @SerializedName("chatTypeId")
    @Expose
    private Long chatTypeId;

    @SerializedName("messageTypeId")
    @Expose
    private Long messageTypeId;

    @SerializedName("senderId")
    @Expose
    private Long senderId;

    @SerializedName("receiverId")
    @Expose
    private Long receiverId;

    @SerializedName("messageContent")
    @Expose
    private String messageContent;

    @SerializedName("attachtmentUrl")
    @Expose
    private String attachtmentUrl;

    @SerializedName("sentTime")
    @Expose
    private java.util.Date sentTime;

    @SerializedName("sentTimeLong")
    @Expose
    private Long sentTimeLong;

    // <editor-fold desc="sent file name">
    @SerializedName("fileName")
    @Expose
    private String fileName;
    // </editor-fold>
}
