package com.thucpn.ttsp.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.thucpn.ttsp.R;
import com.thucpn.ttsp.activity.MainActivity;
import com.thucpn.ttsp.activity.ProfileActivity;
import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.common.ResourceUtil;
import com.thucpn.ttsp.model.ContactDto;
import com.thucpn.ttsp.model.UserDto;
import com.thucpn.ttsp.service.ChatAPI;
import com.thucpn.ttsp.viewholder.ContactViewHolder;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by thucpn on 23/03/2018.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ContactAdapter extends RecyclerView.Adapter<ContactViewHolder> {
    private ContactDto contactDto;
    private Activity context;

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_contact, parent, false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ContactViewHolder holder, final int position) {
        UserDto userDto = contactDto.getContactList().get(position);
        if (Constant.UNKNOWN_STRING.equals(userDto.getGender())) {

        } else {
            if (Constant.MALE_STRING.equals(userDto.getGender())) {
                holder.getAvatarItemContact().setImageResource(R.drawable.img_male_avatar);
            } else {
                holder.getAvatarItemContact().setImageResource(R.drawable.img_female_avatar);
            }
        }
//        if (userDto.getIsActive()) {
//            holder.getAvatarItemContact().setBorderColor(holder.itemView.getResources().getColor(R.color.colorPrimary));
//        } else {
//            holder.getAvatarItemContact().setBorderColor(holder.itemView.getResources().getColor(R.color.colorTextPrimary));
//        }
        holder.getTvUserItemContact().setText(userDto.getFullName());
        holder.itemView.setOnClickListener(v -> {
            UserDto user = contactDto.getContactList().get(position);
            Intent intent = new Intent(holder.itemView.getContext(), ProfileActivity.class);
            intent.putExtra(Constant.USER_SENT, user);
            intent.putExtra(Constant.ACTION, Constant.ACTION_CHAT);
            intent.putExtra(Constant.ACTIVITY_PARENT, Constant.MAIN_ACTIVITY);
            context.startActivityForResult(intent, Constant.REQUEST_CODE);
//                context.finish();
        });

        holder.itemView.setOnLongClickListener(v -> {
            PopupMenu popupMenu = new PopupMenu(holder.itemView.getContext(), holder.itemView);
            popupMenu.getMenuInflater().inflate(R.menu.menu_list_contact, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    final AlertDialog alertDialog = new AlertDialog.Builder(holder.itemView.getContext()).create();
                    alertDialog.setMessage(holder.itemView.getResources().getString(R.string.alert_delete_contact));
                    alertDialog.setCancelable(true);
                    alertDialog.setButton(alertDialog.BUTTON_POSITIVE, holder.itemView.getResources().getString(R.string.delete), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ResourceUtil resourceUtil = (ResourceUtil) holder.itemView.getContext().getApplicationContext();
                            ChatAPI chatAPI = Constant.getChatAPI();
                            ContactDto dto = new ContactDto();
                            dto.setMemberId(userDto.getUserId());
                            dto.setUserId(resourceUtil.userDto.getUserId());
                            Call<Boolean> call = chatAPI.deleteContact(dto);
                            call.enqueue(new Callback<Boolean>() {
                                @Override
                                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                    if (response.isSuccessful()) {
                                        Boolean result = response.body();
                                        if (result == null) {
                                            Toast.makeText(holder.itemView.getContext(), holder.itemView.getResources().getText(R.string.not_success), Toast.LENGTH_SHORT).show();
                                            Log.e("delContact onResponse", " result null");
                                        } else if (result) {
                                            contactDto.getContactList().remove(position);
                                            notifyDataSetChanged();
                                            resourceUtil.contactDto = contactDto;
                                            Toast.makeText(holder.itemView.getContext(), holder.itemView.getResources().getText(R.string.success), Toast.LENGTH_SHORT).show();
                                            alertDialog.dismiss();
                                        } else {
                                            Toast.makeText(holder.itemView.getContext(), holder.itemView.getResources().getText(R.string.not_success), Toast.LENGTH_SHORT).show();
                                            Log.e("delContact onResponse", " result false");
                                        }
                                    } else {
                                        Toast.makeText(holder.itemView.getContext(), holder.itemView.getResources().getText(R.string.not_success), Toast.LENGTH_SHORT).show();
                                        Log.e("delContact onResponse", " not success");
                                    }
                                }

                                @Override
                                public void onFailure(Call<Boolean> call, Throwable t) {
                                    Log.e("delContact onFailure", new Exception(t).getMessage());
                                    Toast.makeText(holder.itemView.getContext(), holder.itemView.getResources().getText(R.string.not_success), Toast.LENGTH_SHORT).show();

                                }
                            });
                        }
                    });
                    alertDialog.setButton(alertDialog.BUTTON_NEGATIVE, holder.itemView.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            alertDialog.dismiss();
                        }
                    });
                    alertDialog.show();
                    return true;
                }
            });
            popupMenu.show();
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return contactDto.getContactList().size();
    }
}
