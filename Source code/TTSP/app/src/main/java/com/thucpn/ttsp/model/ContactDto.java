package com.thucpn.ttsp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ContactDto implements Parcelable {
	@SerializedName("userId")
	@Expose
	private Long userId;

	@SerializedName("accountId")
	@Expose
	private Long accountId;

	@SerializedName("username")
	@Expose
	private String username;

	@SerializedName("memberId")
	@Expose
	private Long memberId;

	@SerializedName("addedTime")
	@Expose
	private String addedTime;

	@SerializedName("contactListString")
	@Expose
	private String contactListString;

	@SerializedName("contactList")
	@Expose
	private List<UserDto> contactList;

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.userId);
		dest.writeValue(this.accountId);
		dest.writeString(this.username);
		dest.writeValue(this.memberId);
		dest.writeString(this.addedTime);
		dest.writeString(this.contactListString);
		dest.writeTypedList(this.contactList);
	}

	protected ContactDto(Parcel in) {
		this.userId = (Long) in.readValue(Long.class.getClassLoader());
		this.accountId = (Long) in.readValue(Long.class.getClassLoader());
		this.username = in.readString();
		this.memberId = (Long) in.readValue(Long.class.getClassLoader());
		this.addedTime = in.readString();
		this.contactListString = in.readString();
		this.contactList = in.createTypedArrayList(UserDto.CREATOR);
	}

	public static final Parcelable.Creator<ContactDto> CREATOR = new Parcelable.Creator<ContactDto>() {
		@Override
		public ContactDto createFromParcel(Parcel source) {
			return new ContactDto(source);
		}

		@Override
		public ContactDto[] newArray(int size) {
			return new ContactDto[size];
		}
	};
}
