package com.thucpn.ttsp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.thucpn.ttsp.R;

import de.hdodenhof.circleimageview.CircleImageView;
import lombok.Getter;

@Getter
public class SearchUserViewHolder extends RecyclerView.ViewHolder {
    private CircleImageView avatarItemUser_SearchResult;
    private TextView tvUserItem_SearchResult;
    private TextView tvPhone_SearchResult;

    public SearchUserViewHolder(View itemView) {
        super(itemView);
        avatarItemUser_SearchResult = itemView.findViewById(R.id.avatarItemUser_SearchResult);
        tvUserItem_SearchResult = itemView.findViewById(R.id.tvUserItem_SearchResult);
        tvPhone_SearchResult = itemView.findViewById(R.id.tvPhone_SearchResult);
    }
}
