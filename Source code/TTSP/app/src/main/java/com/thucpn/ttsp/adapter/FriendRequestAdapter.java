package com.thucpn.ttsp.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.thucpn.ttsp.R;
import com.thucpn.ttsp.activity.ProfileActivity;
import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.common.ResourceUtil;
import com.thucpn.ttsp.model.ContactDto;
import com.thucpn.ttsp.model.FriendRequestDto;
import com.thucpn.ttsp.model.UserDto;
import com.thucpn.ttsp.service.ChatAPI;
import com.thucpn.ttsp.viewholder.FriendRequestViewHolder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Getter
@Setter
public class FriendRequestAdapter extends RecyclerView.Adapter<FriendRequestViewHolder> {
    private List<UserDto> arrFriendRequestDto;
    private android.app.Activity context;

    @Override
    public FriendRequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_friend_request, parent, false);
        return new FriendRequestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FriendRequestViewHolder holder, final int position) {
        ResourceUtil resourceUtil = (ResourceUtil) ResourceUtil.getContext();
        final UserDto userDto_resourceUtil = resourceUtil.userDto;
        arrFriendRequestDto = resourceUtil.friendRequestDto.getRequestedList();
        final UserDto userDto = arrFriendRequestDto.get(position);
        holder.getTvUserItemFriendRequest().setText(userDto.getFullName());
        holder.getTvPhoneFriendRequest().setText(userDto.getAddress());
        if (Constant.UNKNOWN_STRING.equals(userDto.getGender())) {

        } else {
            if (Constant.MALE_STRING.equals(userDto.getGender())) {
                holder.getAvatarItemFriendRequest().setImageResource(R.drawable.img_male_avatar);
            } else {
                holder.getAvatarItemFriendRequest().setImageResource(R.drawable.img_female_avatar);
            }
        }

        holder.getTvUserItemFriendRequest().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), ProfileActivity.class);
                intent.putExtra(Constant.USER_SENT, userDto);
                intent.putExtra(Constant.ACTION, Constant.ACTION_WAIT_FRIEND_REQUEST);
                intent.putExtra(Constant.ACTIVITY_PARENT, Constant.MAIN_ACTIVITY);
                context.startActivityForResult(intent, Constant.REQUEST_CODE);
//                context.finish();
            }
        });

        holder.getBtnAccept_FriendRequest().setOnClickListener(v -> {
            ContactDto contactDto = new ContactDto();
            contactDto.setUserId(userDto_resourceUtil.getUserId());
            contactDto.setAccountId(userDto_resourceUtil.getAccountId());
            contactDto.setUsername(userDto_resourceUtil.getUsername());
            contactDto.setMemberId(userDto.getUserId());
            Date currentDate = new Date(System.currentTimeMillis());
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            contactDto.setAddedTime(dateFormat.format(currentDate));
            ChatAPI chatAPI = Constant.getChatAPI();
            Call<Boolean> call = chatAPI.acceptFriendRequest(contactDto);
            call.enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                    if (response.isSuccessful()) {
                        Boolean result = response.body();
                        if (result) {
                            Toast.makeText(holder.itemView.getContext(), "Thành công", Toast.LENGTH_SHORT).show();
                            arrFriendRequestDto.remove(position);
//                            ResourceUtil resourceUtil1 = (ResourceUtil) ResourceUtil.getContext();
                            resourceUtil.friendRequestDto.setRequestedList(arrFriendRequestDto);
                            resourceUtil.contactDto.getContactList().add(userDto);
                            notifyDataSetChanged();
                        } else {
                            Toast.makeText(holder.itemView.getContext(), "Không thành công", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(holder.itemView.getContext(), "Không thành công", Toast.LENGTH_SHORT).show();
                        Log.e("acceptFriend onResponse", "not successful");
                    }
                }

                @Override
                public void onFailure(Call<Boolean> call, Throwable t) {
                    Toast.makeText(holder.itemView.getContext(), "Không thành công", Toast.LENGTH_SHORT).show();
                    Log.e("acceptFriend onFailure", new Exception(t).getMessage());
                }
            });
        });
        holder.getBtnCancel_FriendRequest().setOnClickListener(v -> {
            FriendRequestDto friendRequestDto = new FriendRequestDto();
            friendRequestDto.setSenderId(userDto.getUserId());
            friendRequestDto.setUserId(userDto_resourceUtil.getUserId());
            friendRequestDto.setAccountId(userDto_resourceUtil.getAccountId());
            ChatAPI chatAPI = Constant.getChatAPI();
            Call<Boolean> call = chatAPI.cancelFriendRequest(friendRequestDto);
            call.enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                    if (response.isSuccessful()) {
                        Boolean result = response.body();
                        if (result) {
                            Toast.makeText(holder.itemView.getContext(), "Thành công", Toast.LENGTH_SHORT).show();
                            arrFriendRequestDto.remove(position);
                            ResourceUtil resourceUtil12 = (ResourceUtil) ResourceUtil.getContext();
                            resourceUtil12.friendRequestDto.setRequestedList(arrFriendRequestDto);
                            notifyDataSetChanged();
                        } else {
                            Toast.makeText(holder.itemView.getContext(), "Không thành công", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(holder.itemView.getContext(), "Không thành công", Toast.LENGTH_SHORT).show();
                        Log.e("cancelFriend onResponse", "not successful");
                    }
                }

                @Override
                public void onFailure(Call<Boolean> call, Throwable t) {
                    Toast.makeText(holder.itemView.getContext(), "Không thành công", Toast.LENGTH_SHORT).show();
                    Log.e("cancelFriend onFailure", new Exception(t).getMessage());
                }
            });
        });

    }

    @Override
    public int getItemCount() {
        return arrFriendRequestDto.size();
    }

}
