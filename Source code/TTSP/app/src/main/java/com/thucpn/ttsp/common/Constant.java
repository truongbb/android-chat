package com.thucpn.ttsp.common;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.thucpn.ttsp.service.ChatAPI;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thucpn on 16/03/2018.
 */

public class Constant {
    public static final String SERVER_ADDRESS = "http://learningcooking.tk/";

    public static final String PATH = Environment.getExternalStorageDirectory().getPath();

    public static final String baseUrl = "http://192.168.0.107:8085/android-chat-app-service/api/";
    public static final String linkUser = "user/";
    public static final String linkChat = "chat/";
    public static final String linkContact = "contact/";
    public static final String linkFriendRequest = "friend/";

    //auto_login
    public static final String AUTO_LOGIN_TRUE = "true";
    public static final String AUTO_LOGIN_FALSE = "false";

    //gender id
    public static final long MALE = 1;
    public static final long FEMALE = 2;
    public static final long UNKNOWN = 3;

    //gender string
    public static final String MALE_STRING = "Nam";
    public static final String FEMALE_STRING = "Nữ";
    public static final String UNKNOWN_STRING = "Không xác định";

    //isContact
    public static final Long IS_CONTACT = 1l;

    //isSent
    public static final Long IS_SENT = 1l;

    //isSent
    public static final Long IS_SENDER = 1l;

    //chatTypeId
    public static final long PAIR = 1;
    public static final long GROUP = 2;

    public static final int ERROR = 4;
    public static final int REGISTER_SUCCESSFULLY = 0;
    public static final int EXISTED_ACCOUNT = 1;
    public static final int EXISTED_PHONE_OR_EMAIL = 2;
    public static final int INVALID_PASSWORD = 3;


    //message type
    public static final int MESSAGE_LEFT = 0;
    public static final int MESSAGE_RIGHT = 1;

    public static final int TEXT_MESSAGE = 1;
    public static final int REQUEST_CHOOSE_IMAGE = 5396;

    //intent put
    public static final String CONVERSATION = "Conversation";
    public static final String USER_SENT = "User sent";
    public static final String SEARCH_USER_RESULT = "search user result";

    //action
    public static final String ACTIVITY_PARENT = "activity parent";
    public static final String SEARCH_USER_ACTIVITY = "search user activity";
    public static final String CHAT_ACTIVITY = "chat activity";
    public static final String MAIN_ACTIVITY = "main activity";

    public static final String ACTION = "Action";
    public static final int ACTION_NONE = 0;
    public static final int ACTION_CHAT = 1;
    public static final int ACTION_FRIEND_REQUEST = 2;
    public static final int ACTION_WAIT_FRIEND_REQUEST = 3;
    public static final int ACTION_CHANGE_PROFILE = 4;
    public static final int ACTION_SENT_FRIEND_REQUEST = 5;

    //status upload file
    public static final int UPLOAD_SUCESSFULLY = 0;
    public static final int UPLOAD_FAIL = 1;
    public static final int EMPTY_FILE = 2;

    //status create conversation
    public static final int EXSITED = 1;
    public static final int NEW_OK = 2;
    public static final int NEW_NOT_OK = 3;

    //sqlite
    public static final String PATH_DB = Environment.getDataDirectory().getPath() + "/data/com.thucpn.ttsp/databases/";
    public static final String DB_NAME = "app_database.sqlite";

    //sqlite user
    public static final String USER_TABLE_NAME = "last_user";
    public static final String USER_ID = "userId";
    public static final String USER_FIRST_NAME = "firstName";
    public static final String USER_LAST_NAME = "lastName";
    public static final String USER_ADDRESS = "address";
    public static final String USER_EMAIL = "email";
    public static final String USER_PHONE = "phone";
    public static final String USER_GENDER_ID = "genderId";
    public static final String USER_GENDER = "gender";
    public static final String USER_BIRTHDAY_STR = "birthdayStr";
    public static final String USER_ACCOUNT_ID = "accountId";
    public static final String USER_USERNAME = "username";
    public static final String USER_PASSWORD = "password";

    //sqlite seen
    public static final String SEEN_TABLE_NAME = "last_seen";
    public static final String SEEN_CONNVERSATION_ID = "conversationId";
    public static final String SEEN_TIME = "time";

    //action service
    public static final String ACTION_SERVICE = "action service";
    public static final int ACTION_SERVICE_LOAD = 1;

    //activity for result
    public static final String REQUEST_ACTION = "request action";
    public static final int REQUEST_CODE = 5396;
    public static final int REQUEST_RESULT_FRIEND_REQUEST = 5397;
    public static final int REQUEST_RESULT_CHANGE_PROFILE = 5398;
    public static final int REQUEST_RESULT_CONTACT = 5399;
    public static final int RESULT_IMG = 4;


    public static final String DOWN_FILE_SUCCESS = "Ok";
    public static int WHAT_ITEM = 9999;

    public static ChatAPI getChatAPI() {
        return BaseRetrofit.getInstance(baseUrl).create(ChatAPI.class);
    }

    public static void checkAndRequestPermissions(Activity context) {
        String[] permissions = new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
        };
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(permission);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(context, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 1);
        }
    }

}
