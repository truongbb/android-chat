package com.thucpn.ttsp.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.thucpn.ttsp.R;
import com.thucpn.ttsp.activity.BeginActivity;
import com.thucpn.ttsp.activity.MainActivity;
import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.common.DatabaseUtil;
import com.thucpn.ttsp.common.MySharedPreferences;
import com.thucpn.ttsp.common.ResourceUtil;
import com.thucpn.ttsp.model.UserDto;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by thucpn on 21/02/2018.
 */

public class LoginFragment extends BaseFragment implements View.OnClickListener {
    private EditText edtUsername;
    private EditText edtPassword;
    private Button btnLogin;
    private TextView tvRegister;
    private AppCompatTextView tvForgetPassword;
    private TextInputLayout tilUsername_login;
    private TextInputLayout tilPassword_login;

    private String username = "";
    private String password = "";
    private ProgressDialog progressDialog;

    @Override
    protected int getViewID() {
        return R.layout.layout_login;
    }

    @Override
    protected void init() {
        mySharedPreferences = new MySharedPreferences(getContext());
        resourceUtil = (ResourceUtil) ResourceUtil.getContext();

        if (!mySharedPreferences.getData(MySharedPreferences.KEY.AUTO_LOGIN).isEmpty() && mySharedPreferences.getData(MySharedPreferences.KEY.AUTO_LOGIN).equals(Constant.AUTO_LOGIN_TRUE)) {
            Intent intent = new Intent(getActivity(), MainActivity.class);
            startActivity(intent);
            getActivity().finish();
        } else {
            edtUsername = getActivity().findViewById(R.id.edtUsernameLogin);
            edtPassword = getActivity().findViewById(R.id.edtPasswordLogin);
            btnLogin = getActivity().findViewById(R.id.btnLogin);
            tvRegister = getActivity().findViewById(R.id.tvRegister);
            tvForgetPassword = getActivity().findViewById(R.id.tvForgetPassword);
            tilUsername_login = getActivity().findViewById(R.id.tilUsername_login);
            tilPassword_login = getActivity().findViewById(R.id.tilPasswoord_login);
            btnLogin.setOnClickListener(this);
            tvRegister.setOnClickListener(this);
        }
    }

    private void loadAnswerLogin() {
        chatAPI = Constant.getChatAPI();
        Call<UserDto> call = chatAPI.login(userDto);
        call.enqueue(new Callback<UserDto>() {
            @Override
            public void onResponse(final Call<UserDto> call, Response<UserDto> response) {
                if (response.isSuccessful()) {
                    userDto = response.body();
                    if (userDto == null || userDto.getUserId() == null || userDto.getUserId() == 0) {
                        new Handler().postDelayed(() -> progressDialog.dismiss(), 3000);

                        Toast.makeText(getContext(), getResources().getText(R.string.login_error), Toast.LENGTH_SHORT).show();
                    } else {
                        resourceUtil.userDto = userDto;
                        mySharedPreferences.putData(MySharedPreferences.KEY.USERNAME, userDto.getUsername());
                        mySharedPreferences.putData(MySharedPreferences.KEY.PASSWORD, userDto.getPassword());
                        mySharedPreferences.putData(MySharedPreferences.KEY.AUTO_LOGIN, Constant.AUTO_LOGIN_TRUE);
                        new Handler().postDelayed(() -> {
                            DatabaseUtil databaseUtil = DatabaseUtil.getInstance(getContext());
                            if (databaseUtil.getLastUser() != null) {
                                databaseUtil.updateUser(userDto);
                            } else {
                                databaseUtil.insertLastUser(userDto);
                            }
                            progressDialog.dismiss();
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        }, 3000);

                    }
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), "Đăng nhập không thành công", Toast.LENGTH_SHORT).show();
                    Log.e("GetAllConv onResponse", "not successful");
                }

            }

            @Override
            public void onFailure(Call<UserDto> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), "Đăng nhập không thành công", Toast.LENGTH_SHORT).show();
                Log.e("Connect api login: ", new Exception(t).getMessage());
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                if (!checkInput()) {
                    break;
                } else {
                    hideErrorEditText();
                    progressDialog = new ProgressDialog(getContext(), R.style.MyTheme);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage(getContext().getResources().getString(R.string.wait_login));
                    progressDialog.show();
                    userDto = new UserDto();
                    userDto.setUsername(username);
                    userDto.setPassword(password);
                    loadAnswerLogin();
                    break;
                }
            case R.id.tvRegister:
                BeginActivity beginActivity = (BeginActivity) getActivity();
                beginActivity.switchFragment(beginActivity.getRegisterFragment());
                break;
            case R.id.tvForgetPassword:
                Toast.makeText(getContext(), "quên mk", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void hideErrorEditText() {
        tilUsername_login.setError(null);
        tilPassword_login.setError(null);
    }

    private boolean checkInput() {
        getInput();
        hideErrorEditText();
        if (username == null || username.length() == 0) {
            tilUsername_login.setError(getResources().getString(R.string.username) + " " + getResources().getString(R.string.not_valid));
            return false;
        }
        if (password == null || password.length() == 0) {
            tilPassword_login.setError(getResources().getString(R.string.password) + " " + getResources().getString(R.string.not_valid));
            return false;
        }
        return true;
    }

    private void getInput() {
        username = edtUsername.getText().toString().trim();
        password = edtPassword.getText().toString().trim();
    }

}
