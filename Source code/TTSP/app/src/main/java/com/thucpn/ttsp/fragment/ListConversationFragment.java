package com.thucpn.ttsp.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.itemanimators.AlphaInAnimator;
import com.thucpn.ttsp.R;
import com.thucpn.ttsp.activity.ChatActivity;
import com.thucpn.ttsp.adapter.AddUserAdapter;
import com.thucpn.ttsp.adapter.ConversationAdapter;
import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.model.ConversationDto;
import com.thucpn.ttsp.model.CreateConversationDto;
import com.thucpn.ttsp.model.ParticipantDto;
import com.thucpn.ttsp.model.UserDto;
import com.thucpn.ttsp.service.ChatAPI;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by thucpn on 22/03/2018.
 */
@Getter
@Setter
public class ListConversationFragment extends BaseFragment {
    private RecyclerView recyclerViewConversation;
    private RelativeLayout layout_none_conversation;
    private List<ConversationDto> arrConversationDto;
    private FloatingActionButton fab_addConversation;
    private Dialog dialogAddUser;

    public ListConversationFragment() {
        arrConversationDto = resourceUtil.arrConversationDto;
    }

    @Override
    protected int getViewID() {
        return R.layout.demo_list_conversation;
    }

    @Override
    protected void init() {
        layout_none_conversation = getActivity().findViewById(R.id.layout_none_conversation);
        recyclerViewConversation = getActivity().findViewById(R.id.recyclerViewConversation);
        fab_addConversation = getActivity().findViewById(R.id.fab_addConversation);
        fab_addConversation.setOnClickListener(view -> {
//            Intent intent = new Intent(getActivity(), CreateNewConversation.class);
//            startActivity(intent);
            initConversation();
        });

        arrConversationDto = resourceUtil.arrConversationDto;
        if (arrConversationDto == null || arrConversationDto.size() == 0) {
            layout_none_conversation.setVisibility(View.VISIBLE);
            recyclerViewConversation.setVisibility(View.GONE);
        } else {
            layout_none_conversation.setVisibility(View.GONE);
            recyclerViewConversation.setVisibility(View.VISIBLE);
            ConversationAdapter conversationAdapter = new ConversationAdapter();
            conversationAdapter.setArrConversation(arrConversationDto);
            conversationAdapter.setContext(getActivity());
            StaggeredGridLayoutManager gridLayoutManager =
                    new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
            recyclerViewConversation.setLayoutManager(gridLayoutManager);
            recyclerViewConversation.setAdapter(conversationAdapter);
            recyclerViewConversation.setItemAnimator(new AlphaInAnimator());
            recyclerViewConversation.setHasFixedSize(true);
        }
    }

    private void initConversation() {

        ArrayList<UserDto> arrUserDtos = (ArrayList<UserDto>) resourceUtil.contactDto.getContactList();
        for (int i = 0; i < arrUserDtos.size(); i++) {
            arrUserDtos.get(i).setChecked(false);
        }
        dialogAddUser = new Dialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog_MinWidth);
        dialogAddUser.setTitle(getResources().getString(R.string.title_creat_conversation));
        dialogAddUser.setCancelable(true);
        dialogAddUser.setContentView(R.layout.layout_dialog_create_conversation);
        TextView tvNameUser_AddUser = dialogAddUser.findViewById(R.id.tvNameUser_AddUser);
        ListView listView_AddUser = dialogAddUser.findViewById(R.id.listView_AddUser);
        Button btnCancel_AddUser = dialogAddUser.findViewById(R.id.btnCancel_AddUser);
        Button btnSave_AddUser = dialogAddUser.findViewById(R.id.btnSave_AddUser);
        AddUserAdapter addUserAdapter = new AddUserAdapter(dialogAddUser.getContext(), arrUserDtos);
        listView_AddUser.setAdapter(addUserAdapter);
        dialogAddUser.show();
        listView_AddUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String userNames = "";
                if (arrUserDtos.get(i).isChecked()) {
                    arrUserDtos.get(i).setChecked(false);
                } else {
                    arrUserDtos.get(i).setChecked(true);
                }
                for (UserDto userDto : arrUserDtos) {
                    if (userDto.isChecked()) {
                        userNames = userNames + ", " + userDto.getFullName();
                    }
                }
                addUserAdapter.notifyDataSetChanged();
                if (userNames != "") {
                    userNames.replaceFirst(",", " ");
                    userNames.trim();
                    tvNameUser_AddUser.setText(userNames);
                }
            }
        });
        btnCancel_AddUser.setOnClickListener(view -> dialogAddUser.dismiss());
        btnSave_AddUser.setOnClickListener(view -> {
            List<ParticipantDto> arrParticipantDto = new ArrayList<>();
            for (int i = 0; i < arrUserDtos.size(); i++) {
                if (arrUserDtos.get(i).isChecked()) {
                    ParticipantDto participantDto = new ParticipantDto();
                    participantDto.setUserId(arrUserDtos.get(i).getUserId());
                    arrParticipantDto.add(participantDto);
                }
            }
            CreateConversationDto createConversationDto = new CreateConversationDto();
            createConversationDto.setCreatorId(resourceUtil.userDto.getUserId());
            createConversationDto.setParticipants(arrParticipantDto);
            createConversationDto.setTitle(null);
            ChatAPI chatAPI = Constant.getChatAPI();
            Call<CreateConversationDto> call = chatAPI.creatConversation(createConversationDto);
            call.enqueue(new Callback<CreateConversationDto>() {
                @Override
                public void onResponse(Call<CreateConversationDto> call, Response<CreateConversationDto> response) {
                    if (response.isSuccessful()) {
                        CreateConversationDto dto = response.body();
                        Intent intent;
                        ConversationDto conversationDto;
                        switch (dto.getStatus()) {
                            case Constant.EXSITED:
                                resourceUtil.arrConversationHistorie = dto.getConversationHistory();
                                intent = new Intent(getActivity(), ChatActivity.class);
                                conversationDto = new ConversationDto();
                                conversationDto.setTitle(dto.getTitle());
                                conversationDto.setChatTypeId(dto.getChatTypeId());
                                conversationDto.setId(dto.getConversationId());
                                intent.putExtra(Constant.CONVERSATION, conversationDto);
                                getActivity().startActivity(intent);
                                getActivity().finish();
                                break;
                            case Constant.NEW_OK:
                                resourceUtil.arrConversationHistorie = null;
                                intent = new Intent(getActivity(), ChatActivity.class);
                                conversationDto = new ConversationDto();
                                conversationDto.setTitle(dto.getTitle());
                                conversationDto.setChatTypeId(dto.getChatTypeId());
                                conversationDto.setId(dto.getConversationId());
                                intent.putExtra(Constant.CONVERSATION, conversationDto);
                                getActivity().startActivity(intent);
                                getActivity().finish();
                                break;
                            case Constant.NEW_NOT_OK:
                                Toast.makeText(getActivity(), getResources().getText(R.string.not_success), Toast.LENGTH_SHORT).show();
                                break;
                            case Constant.ERROR:
                                Toast.makeText(getActivity(), getResources().getText(R.string.not_success), Toast.LENGTH_SHORT).show();
                                break;
                        }

                    } else {
                        Toast.makeText(getActivity(), getResources().getText(R.string.not_success), Toast.LENGTH_SHORT).show();
                        Log.e("creatConv onResponse", "not successful");
                    }
                }

                @Override
                public void onFailure(Call<CreateConversationDto> call, Throwable t) {
                    Toast.makeText(getActivity(), getResources().getText(R.string.not_success), Toast.LENGTH_SHORT).show();
                    Log.e("creatConv onFailure", new Exception(t).getMessage());
                }
            });
            dialogAddUser.dismiss();
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        arrConversationDto = resourceUtil.arrConversationDto;
        if (arrConversationDto == null || arrConversationDto.size() == 0) {
            layout_none_conversation.setVisibility(View.VISIBLE);
            recyclerViewConversation.setVisibility(View.GONE);
        } else {
            layout_none_conversation.setVisibility(View.GONE);
            recyclerViewConversation.setVisibility(View.VISIBLE);
            ConversationAdapter conversationAdapter = new ConversationAdapter();
            conversationAdapter.setArrConversation(arrConversationDto);
            conversationAdapter.setContext(getActivity());
            StaggeredGridLayoutManager gridLayoutManager =
                    new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
            recyclerViewConversation.setLayoutManager(gridLayoutManager);
            recyclerViewConversation.setAdapter(conversationAdapter);
            recyclerViewConversation.setItemAnimator(new AlphaInAnimator());
            recyclerViewConversation.setHasFixedSize(true);
        }
//        init();

    }
}
