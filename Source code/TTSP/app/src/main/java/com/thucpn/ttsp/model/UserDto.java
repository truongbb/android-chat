package com.thucpn.ttsp.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDto implements Parcelable {

    @SerializedName("userId")
    @Expose
    private Long userId;

    @SerializedName("firstName")
    @Expose
    private String firstName;

    @SerializedName("lastName")
    @Expose
    private String lastName;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("genderId")
    @Expose
    private Long genderId;

    @SerializedName("birthdayStr")
    @Expose
    private String birthdayStr;

    @SerializedName("birthday")
    @Expose
    private java.util.Date birthday;

    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("accountId")
    @Expose
    private Long accountId;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("isActive")
    @Expose
    private Boolean isActive;

    @SerializedName("lastOnlineTime")
    @Expose
    private java.util.Date lastOnlineTime;

    @SerializedName("requestedTimeString")
    @Expose
    private String requestedTimeString;

    @SerializedName("requestedTime")
    @Expose
    private java.util.Date requestedTime;

    @SerializedName("addedTimeString")
    @Expose
    private String addedTimeString;

    @SerializedName("addedTime")
    @Expose
    private java.util.Date addedTime;

    private boolean isChecked;

    @SerializedName("isContact")
    @Expose
    private Long isContact;

    @SerializedName("isSent")
    @Expose
    private Long isSent;

    @SerializedName("isSender")
    @Expose
    private Long isSender;

    @NonNull
    public String getFullName() {
        return this.getFirstName() + " " + this.getLastName();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.userId);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.address);
        dest.writeString(this.email);
        dest.writeString(this.phone);
        dest.writeValue(this.genderId);
        dest.writeString(this.birthdayStr);
        dest.writeLong(this.birthday != null ? this.birthday.getTime() : -1);
        dest.writeString(this.gender);
        dest.writeValue(this.accountId);
        dest.writeString(this.username);
        dest.writeString(this.password);
        dest.writeValue(this.isActive);
        dest.writeLong(this.lastOnlineTime != null ? this.lastOnlineTime.getTime() : -1);
        dest.writeString(this.requestedTimeString);
        dest.writeLong(this.requestedTime != null ? this.requestedTime.getTime() : -1);
        dest.writeString(this.addedTimeString);
        dest.writeLong(this.addedTime != null ? this.addedTime.getTime() : -1);
        dest.writeByte(this.isChecked ? (byte) 1 : (byte) 0);
        dest.writeValue(this.isContact);
        dest.writeValue(this.isSent);
        dest.writeValue(this.isSender);
    }

    protected UserDto(Parcel in) {
        this.userId = (Long) in.readValue(Long.class.getClassLoader());
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.address = in.readString();
        this.email = in.readString();
        this.phone = in.readString();
        this.genderId = (Long) in.readValue(Long.class.getClassLoader());
        this.birthdayStr = in.readString();
        long tmpBirthday = in.readLong();
        this.birthday = tmpBirthday == -1 ? null : new Date(tmpBirthday);
        this.gender = in.readString();
        this.accountId = (Long) in.readValue(Long.class.getClassLoader());
        this.username = in.readString();
        this.password = in.readString();
        this.isActive = (Boolean) in.readValue(Boolean.class.getClassLoader());
        long tmpLastOnlineTime = in.readLong();
        this.lastOnlineTime = tmpLastOnlineTime == -1 ? null : new Date(tmpLastOnlineTime);
        this.requestedTimeString = in.readString();
        long tmpRequestedTime = in.readLong();
        this.requestedTime = tmpRequestedTime == -1 ? null : new Date(tmpRequestedTime);
        this.addedTimeString = in.readString();
        long tmpAddedTime = in.readLong();
        this.addedTime = tmpAddedTime == -1 ? null : new Date(tmpAddedTime);
        this.isChecked = in.readByte() != 0;
        this.isContact = (Long) in.readValue(Long.class.getClassLoader());
        this.isSent = (Long) in.readValue(Long.class.getClassLoader());
        this.isSender = (Long) in.readValue(Long.class.getClassLoader());
    }

    public static final Parcelable.Creator<UserDto> CREATOR = new Parcelable.Creator<UserDto>() {
        @Override
        public UserDto createFromParcel(Parcel source) {
            return new UserDto(source);
        }

        @Override
        public UserDto[] newArray(int size) {
            return new UserDto[size];
        }
    };
}
