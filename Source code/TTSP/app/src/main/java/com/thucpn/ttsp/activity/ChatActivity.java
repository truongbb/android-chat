package com.thucpn.ttsp.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.mikepenz.itemanimators.AlphaInAnimator;
import com.thucpn.ttsp.R;
import com.thucpn.ttsp.adapter.MessageAdapter;
import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.common.ResourceUtil;
import com.thucpn.ttsp.handler.UploadFile;
import com.thucpn.ttsp.model.ConversationDto;
import com.thucpn.ttsp.model.ConversationHistoryDto;
import com.thucpn.ttsp.model.ItemImage;
import com.thucpn.ttsp.model.MessageDto;
import com.thucpn.ttsp.model.UserDto;
import com.thucpn.ttsp.service.ChatAPI;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RuntimePermissions
public class ChatActivity extends AppCompatActivity implements View.OnClickListener {
    private ResourceUtil resourceUtil;
    private ConversationDto conversationDto;
    private List<ConversationHistoryDto> arrConversationHistoryDto;
    private String contentMessage;
    private ConversationHistoryDto conversationHistoryDto;
    private UserDto userDto;
    private ChatAPI chatAPI;
    private MessageAdapter messageAdapter;
    private int index = -1;

    private View layout_message_id;
    private RecyclerView recyclerViewMessage_Message;
    private ImageView imgInsertPhoto_Message;
    private EditText edtInputMessage_Message;
    private ImageView imgSend_Message;

    private AppCompatActivity context;
    private String imagePath;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_message);
        init();
    }

    private void init() {
        this.context = this;
        layout_message_id = findViewById(R.id.layout_message_id);
        conversationDto = getIntent().getParcelableExtra(Constant.CONVERSATION);
        resourceUtil = (ResourceUtil) ResourceUtil.getContext();
        arrConversationHistoryDto = resourceUtil.arrConversationHistorie;
        userDto = resourceUtil.userDto;
        for (int i = 0; i < resourceUtil.arrConversationDto.size(); i++) {
            if (conversationDto.getId() == resourceUtil.arrConversationDto.get(i).getId()) {
                break;
            }
        }
        if (index == -1) {
            resourceUtil.arrConversationDto.add(conversationDto);
        }

        if (arrConversationHistoryDto == null) {
            arrConversationHistoryDto = new ArrayList<>();
        }
        recyclerViewMessage_Message = findViewById(R.id.recyclerViewMessage_Message);
        imgInsertPhoto_Message = findViewById(R.id.imgInsertPhoto_Message);
        edtInputMessage_Message = findViewById(R.id.edtInputMessage_Message);
        imgSend_Message = findViewById(R.id.imgSend_Message);

        messageAdapter = new MessageAdapter();
        messageAdapter.setConversationHistories(arrConversationHistoryDto);
        messageAdapter.setChatType(conversationDto.getChatTypeId());
        StaggeredGridLayoutManager gridLayoutManager =
                new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        recyclerViewMessage_Message.setLayoutManager(gridLayoutManager);
        recyclerViewMessage_Message.setAdapter(messageAdapter);
        recyclerViewMessage_Message.setItemAnimator(new AlphaInAnimator());
        recyclerViewMessage_Message.scrollToPosition(messageAdapter.getItemCount() - 1);
        imgInsertPhoto_Message.setOnClickListener(this);
        imgSend_Message.setOnClickListener(this);

        getSupportActionBar().setTitle(conversationDto.getTitle());
        chatAPI = Constant.getChatAPI();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat_activity, menu);
        MenuItem menuItem = menu.findItem(R.id.profile_ChatActivity);
        if (Constant.GROUP == conversationDto.getChatTypeId()) {
//            menuItem.setEnabled(false);
            menuItem.setTitle(getResources().getString(R.string.set_title));
        } else {
            menuItem.setTitle(getResources().getString(R.string.profile));
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.deleteConversation_ChatActivity:
                new AlertDialog.Builder(this)
                        .setMessage(getResources().getString(R.string.alert_delete_conversation))
                        .setPositiveButton(getResources().getText(R.string.delete), (dialog, which) -> {
                            dialog.dismiss();
                            Call<List<ConversationDto>> call = chatAPI.deleteConversation(conversationDto);
                            call.enqueue(new Callback<List<ConversationDto>>() {
                                @Override
                                public void onResponse(Call<List<ConversationDto>> call, Response<List<ConversationDto>> response) {
                                    if (response.isSuccessful()) {
                                        List<ConversationDto> result = response.body();
                                        if (result == null || result.size() == 0) {
                                            Snackbar.make(layout_message_id, getResources().getText(R.string.not_success), Snackbar.LENGTH_SHORT).show();
//                                            Toast.makeText(context, getResources().getText(R.string.not_success), Toast.LENGTH_SHORT).show();
                                            Log.e("delConver onResponse", "not successful");
                                        } else {
                                            resourceUtil.arrConversationDto = result;
                                            Intent intent = new Intent(context, MainActivity.class);
                                            startActivity(intent);
                                            context.finish();
                                        }
                                    } else {
                                        Snackbar.make(layout_message_id, getResources().getText(R.string.not_success), Snackbar.LENGTH_SHORT).show();
//                                        Toast.makeText(context, getResources().getText(R.string.not_success), Toast.LENGTH_SHORT).show();
                                        Log.e("delConver onResponse", "not successful");
                                    }
                                }

                                @Override
                                public void onFailure(Call<List<ConversationDto>> call, Throwable t) {
                                    Snackbar.make(layout_message_id, getResources().getText(R.string.not_success), Snackbar.LENGTH_SHORT).show();
//                                    Toast.makeText(context, getResources().getText(R.string.not_success), Toast.LENGTH_SHORT).show();
                                    Log.e("delConver onFailure", new Exception(t).getMessage());
                                }
                            });
                        })
                        .setNegativeButton(getResources().getText(R.string.cancel), (dialog, which) -> dialog.dismiss())
                        .setCancelable(true)
                        .show();
                break;
            case R.id.profile_ChatActivity:
                if (conversationDto.getChatTypeId() == Constant.GROUP) {
                    Dialog dialog = new Dialog(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth);
                    dialog.setContentView(R.layout.layout_change_title_conversation_dialog);
                    dialog.setCancelable(true);
                    dialog.show();
                    TextInputLayout tilTitle_ChangeTitle = dialog.findViewById(R.id.tilTitle_ChangeTitle);
                    EditText edtTitle_ChangeTitle = dialog.findViewById(R.id.edtTitle_ChangeTitle);
                    edtTitle_ChangeTitle.setText(conversationDto.getTitle());
                    Button btnCancel_ChangeTitle = dialog.findViewById(R.id.btnCancel_ChangeTitle);
                    Button btnChange_ChangeTitle = dialog.findViewById(R.id.btnChange_ChangeTitle);
                    btnCancel_ChangeTitle.setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                    btnChange_ChangeTitle.setOnClickListener(view -> {
                        String title = edtTitle_ChangeTitle.getText().toString().trim();
                        if (title == null | title.isEmpty()) {
                            tilTitle_ChangeTitle.setError(getResources().getString(R.string.information) + " " + getResources().getString(R.string.not_valid));
                        } else {
                            conversationDto.setTitle(title);
                        }
                    });
                } else {
                    for (int i = 0; i < arrConversationHistoryDto.size(); i++) {
                        if (!arrConversationHistoryDto.get(i).getSenderId().equals(userDto.getUserId())) {
                            index = i;
                            break;
                        }
                    }
                    if (index != -1) {
                        UserDto dto = new UserDto();
                        dto.setUserId(arrConversationHistoryDto.get(index).getSenderId());
                        Call<List<UserDto>> call = chatAPI.searchUser(dto);
                        call.enqueue(new Callback<List<UserDto>>() {
                            @Override
                            public void onResponse(Call<List<UserDto>> call, Response<List<UserDto>> response) {
                                if (response.isSuccessful()) {
                                    List<UserDto> result = response.body();
                                    Intent intent = new Intent(context, ProfileActivity.class);
                                    intent.putExtra(Constant.USER_SENT, result.get(0));
                                    intent.putExtra(Constant.ACTION, Constant.ACTION_NONE);
                                    intent.putExtra(Constant.ACTIVITY_PARENT, Constant.CHAT_ACTIVITY);
                                    context.startActivity(intent);
                                } else {
                                    Log.e("getUserById onResponse", "not successful");
                                }
                            }

                            @Override
                            public void onFailure(Call<List<UserDto>> call, Throwable t) {
                                Log.e("getUserById onFailure", new Exception(t).getMessage());

                            }
                        });
                    }
                }
                break;
            case android.R.id.home:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                this.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.imgInsertPhoto_Message:
                ChatActivityPermissionsDispatcher.getImageWithPermissionCheck(this);
                break;
            case R.id.imgSend_Message:
                contentMessage = edtInputMessage_Message.getText().toString().trim();
                if (contentMessage != null && contentMessage.length() != 0) {
                    MessageDto dto = new MessageDto();
                    dto.setSenderId(userDto.getUserId());
                    dto.setMessageContent(contentMessage);
                    dto.setConversationId(conversationDto.getId());
                    dto.setMessageTypeId(Long.valueOf(Constant.TEXT_MESSAGE));
                    dto.setSentTimeLong(new Date().getTime());
                    this.conversationHistoryDto = new ConversationHistoryDto();
                    this.conversationHistoryDto.setSenderId(userDto.getUserId());
                    this.conversationHistoryDto.setMessageContent(contentMessage);
                    this.conversationHistoryDto.setConversationId(conversationDto.getId());
                    this.conversationHistoryDto.setMessageTypeId(Long.valueOf(Constant.TEXT_MESSAGE));
                    this.conversationHistoryDto.setSentTimeLong(new Date().getTime());
                    chatAPI = Constant.getChatAPI();
                    retrofit2.Call<Boolean> call = chatAPI.sendMessage(dto);
                    call.enqueue(new Callback<Boolean>() {
                        @Override
                        public void onResponse(retrofit2.Call<Boolean> call, Response<Boolean> response) {
                            if (response.isSuccessful()) {
                                Boolean result = response.body();
                                if (result != null && result) {
                                    arrConversationHistoryDto.add(ChatActivity.this.conversationHistoryDto);
                                    messageAdapter.notifyItemInserted(arrConversationHistoryDto.size() - 1);
                                    recyclerViewMessage_Message.scrollToPosition(messageAdapter.getItemCount() - 1);
                                    edtInputMessage_Message.setText("");
                                    resourceUtil.arrConversationHistorie = arrConversationHistoryDto;
                                    for (int i = resourceUtil.arrConversationDto.size() - 1; i >= 0; i--) {
                                        if (conversationDto.getId() == resourceUtil.arrConversationDto.get(i).getId()) {
                                            if (index != -1) {
                                                conversationDto = resourceUtil.arrConversationDto.get(index);
                                                conversationDto.setUpdatedTimeLong(conversationHistoryDto.getSentTimeLong());
                                                conversationDto.setLastMessage(contentMessage);
                                                resourceUtil.arrConversationDto.add(0, conversationDto);
                                                resourceUtil.arrConversationDto.remove(i);
                                                break;
                                            }
                                        }
                                    }
//                                    resourceUtil.arrConversationDto.add(0, conversationDto);
//                                    resourceUtil.arrConversationDto.remove(index);
                                } else {
                                    Toast.makeText(v.getContext(), "Chưa gửi được", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(v.getContext(), "Chưa gửi được", Toast.LENGTH_SHORT).show();
                                Log.e("send mess onResponse", "not successful");
                            }
                        }

                        @Override
                        public void onFailure(retrofit2.Call<Boolean> call, Throwable t) {
                            Toast.makeText(v.getContext(), "Chưa gửi được", Toast.LENGTH_SHORT).show();
                            Log.e("send mess onFailure", new Exception(t).getMessage());
                        }
                    });

                } else {
                    Toast.makeText(this, "Chưa nhập tin nhắn", Toast.LENGTH_SHORT).show();
                    return;
                }
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.REQUEST_CHOOSE_IMAGE) {
            if (resultCode == RESULT_OK) {
                try {
                    Uri uriImage = data.getData();
                    sendFile(uriImage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void sendFile(Uri uriImage) {
        if (uriImage == null) {
            return;
        }

        uploadImage(new ItemImage(getRealPathFromURI(uriImage)));

        File file = new File(getRealPathFromURI(uriImage));

        RequestBody requestFile = RequestBody.create(MediaType.parse(getContentResolver().getType(uriImage)), file);
        MultipartBody.Part filePath = MultipartBody.Part.createFormData("uploadfiles", file.getName(), requestFile);

        Call<Integer> call = chatAPI.sendFile(filePath, conversationDto.getId(), userDto.getUserId());
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.isSuccessful()) {
                    if (Constant.UPLOAD_SUCESSFULLY == response.body()) {
                        MessageDto dto = new MessageDto();
                        dto.setSenderId(userDto.getUserId());
                        dto.setMessageContent(file.getName());
                        dto.setConversationId(conversationDto.getId());
                        dto.setMessageTypeId(Long.valueOf(Constant.TEXT_MESSAGE));
                        dto.setSentTimeLong(new Date().getTime());
                        conversationHistoryDto = new ConversationHistoryDto();
                        conversationHistoryDto.setSenderId(userDto.getUserId());
                        conversationHistoryDto.setMessageContent(file.getName());
                        conversationHistoryDto.setConversationId(conversationDto.getId());
                        conversationHistoryDto.setMessageTypeId(Long.valueOf(Constant.TEXT_MESSAGE));
                        conversationHistoryDto.setSentTimeLong(new Date().getTime());
                        arrConversationHistoryDto.add(ChatActivity.this.conversationHistoryDto);
                        messageAdapter.notifyItemInserted(arrConversationHistoryDto.size() - 1);
                        recyclerViewMessage_Message.scrollToPosition(messageAdapter.getItemCount() - 1);
                        edtInputMessage_Message.setText("");
                        for (int i = resourceUtil.arrConversationDto.size() - 1; i > 0; i--) {
                            if (conversationDto.getId() == resourceUtil.arrConversationDto.get(i).getId()) {
                                conversationDto = resourceUtil.arrConversationDto.get(index);
                                conversationDto.setUpdatedTimeLong(conversationHistoryDto.getSentTimeLong());
                                conversationDto.setLastMessage(contentMessage);
                                conversationDto.setFileName(file.getName());
                                resourceUtil.arrConversationDto.add(0, conversationDto);
                                resourceUtil.arrConversationDto.remove(i);
                                break;
                            }
                        }
                        resourceUtil.arrConversationHistorie = arrConversationHistoryDto;
                    } else {
                        if (Constant.UPLOAD_FAIL == response.body()) {
                            Toast.makeText(getApplicationContext(), "Chưa gửi được", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "File trống", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Chưa gửi được", Toast.LENGTH_SHORT).show();
                    Log.e("sendImage onResponse", "not successful");
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Chưa gửi được", Toast.LENGTH_SHORT).show();
                Log.e("sendImage onFailure", new Exception(t).getMessage());

            }
        });
    }

    private void uploadImage(ItemImage itemImage) {
        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == Constant.RESULT_IMG) {
                    String result = msg.obj.toString();
//                    if (result.isEmpty()) {
//                    } else {
//                        tvTenAnh_Add.setText(result);
//                    }
                }
            }
        };
        UploadFile uploadFile = new UploadFile(this, handler);
        uploadFile.execute(itemImage);
    }

    private String getRealPathFromURI(Uri uriImage) {
        Cursor cursor = null;
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            cursor = getContentResolver().query(uriImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int column_index = cursor.getColumnIndexOrThrow(filePathColumn[0]);
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        this.startActivity(intent);
        this.finish();
    }

    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
    void getImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, Constant.REQUEST_CHOOSE_IMAGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ChatActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnShowRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
    void getImageRationale(final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage("Cap quyen doc thu vien")
                .setPositiveButton(getResources().getText(R.string.allow), (dialog, which) -> request.proceed())
                .setNegativeButton(getResources().getText(R.string.deny), (dialog, button) -> request.cancel())
                .setCancelable(false)
                .show();
    }

    @OnPermissionDenied(Manifest.permission.READ_EXTERNAL_STORAGE)
    void getImageDenied() {
        Toast.makeText(this, "Quyen k duoc cap. k gui duoc anh", Toast.LENGTH_SHORT).show();
    }

    @OnNeverAskAgain(Manifest.permission.READ_EXTERNAL_STORAGE)
    void getImageNever() {
        Toast.makeText(this, "Quyen da cap. k hien lai nua", Toast.LENGTH_SHORT).show();
    }

}
