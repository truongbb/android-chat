package com.thucpn.ttsp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
public class ConversationDto implements Parcelable {

    public ConversationDto() {
        this.isSeen = true;
    }

    @SerializedName("id")
    @Expose
    private Long id;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("creatorId")
    @Expose
    private Long creatorId;

    @SerializedName("creatorTime")
    @Expose
    private java.util.Date createdTime;

    @SerializedName("updateTime")
    @Expose
    private java.util.Date updatedTime;

    @SerializedName("updatedTimeLong")
    @Expose
    private Long updatedTimeLong;

    @SerializedName("lastMessage")
    @Expose
    private String lastMessage;

    @SerializedName("lastAttachment")
    @Expose
    private String lastAttachment;

    @SerializedName("fileName")
    @Expose
    private String fileName;

    @SerializedName("chatTypeId")
    @Expose
    private Long chatTypeId;

    @SerializedName("friendGenderId")
    @Expose
    private Long friendGenderId;

    @SerializedName("isSeen")
    @Expose
    private boolean isSeen;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.title);
        dest.writeValue(this.creatorId);
        dest.writeLong(this.createdTime != null ? this.createdTime.getTime() : -1);
        dest.writeLong(this.updatedTime != null ? this.updatedTime.getTime() : -1);
        dest.writeValue(this.updatedTimeLong);
        dest.writeString(this.lastMessage);
        dest.writeString(this.lastAttachment);
        dest.writeString(this.fileName);
        dest.writeValue(this.chatTypeId);
        dest.writeValue(this.friendGenderId);
        dest.writeByte(this.isSeen ? (byte) 1 : (byte) 0);
    }

    protected ConversationDto(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.title = in.readString();
        this.creatorId = (Long) in.readValue(Long.class.getClassLoader());
        long tmpCreatedTime = in.readLong();
        this.createdTime = tmpCreatedTime == -1 ? null : new Date(tmpCreatedTime);
        long tmpUpdatedTime = in.readLong();
        this.updatedTime = tmpUpdatedTime == -1 ? null : new Date(tmpUpdatedTime);
        this.updatedTimeLong = (Long) in.readValue(Long.class.getClassLoader());
        this.lastMessage = in.readString();
        this.lastAttachment = in.readString();
        this.fileName = in.readString();
        this.chatTypeId = (Long) in.readValue(Long.class.getClassLoader());
        this.friendGenderId = (Long) in.readValue(Long.class.getClassLoader());
        this.isSeen = in.readByte() != 0;
    }

    public static final Parcelable.Creator<ConversationDto> CREATOR = new Parcelable.Creator<ConversationDto>() {
        @Override
        public ConversationDto createFromParcel(Parcel source) {
            return new ConversationDto(source);
        }

        @Override
        public ConversationDto[] newArray(int size) {
            return new ConversationDto[size];
        }
    };
}
