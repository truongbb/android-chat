package com.thucpn.ttsp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.mikepenz.itemanimators.SlideUpAlphaAnimator;
import com.thucpn.ttsp.R;
import com.thucpn.ttsp.adapter.ListUserNewConversationAdapter;
import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.common.ResourceUtil;
import com.thucpn.ttsp.model.ConversationDto;
import com.thucpn.ttsp.model.CreateConversationDto;
import com.thucpn.ttsp.model.ParticipantDto;
import com.thucpn.ttsp.model.UserDto;
import com.thucpn.ttsp.service.ChatAPI;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateNewConversation extends AppCompatActivity {
    private AppCompatActivity appCompatActivity;
    private ResourceUtil resourceUtil;
    private List<UserDto> arrUserDto;
    private ListUserNewConversationAdapter listUserNewConversationAdapter;

    private EditText edtSearch_NewConversation;
    private RecyclerView recyclerView_NewConversation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_list_user_new_conversation);
        init();
    }

    private void init() {
        appCompatActivity = CreateNewConversation.this;
        resourceUtil = (ResourceUtil) ResourceUtil.getContext();
        recyclerView_NewConversation = findViewById(R.id.recyclerView_NewConversation);
        edtSearch_NewConversation = findViewById(R.id.edtSearch_NewConversation);
        listUserNewConversationAdapter = new ListUserNewConversationAdapter();
        for (int i = 0; i < resourceUtil.contactDto.getContactList().size(); i++) {
            resourceUtil.getContactDto().getContactList().get(i).setChecked(false);
        }
        listUserNewConversationAdapter.setArrUserDto(resourceUtil.contactDto.getContactList());
        StaggeredGridLayoutManager gridLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        recyclerView_NewConversation.setHasFixedSize(true);
        recyclerView_NewConversation.setLayoutManager(gridLayoutManager);
        recyclerView_NewConversation.setAdapter(listUserNewConversationAdapter);
        recyclerView_NewConversation.setItemAnimator(new SlideUpAlphaAnimator());
        edtSearch_NewConversation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                List<UserDto> arrUser_Filter = new ArrayList<>();
                for (int i = 0; i < resourceUtil.contactDto.getContactList().size(); i++) {
                    String fullname = resourceUtil.contactDto.getContactList().get(i).getFirstName() + " " + resourceUtil.contactDto.getContactList().get(i).getLastName();
                    if (fullname.contains(s)) {
                        arrUser_Filter.add(resourceUtil.contactDto.getContactList().get(i));
                    }
                }
                listUserNewConversationAdapter.setArrUserDto(arrUser_Filter);
                recyclerView_NewConversation.setAdapter(listUserNewConversationAdapter);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.ok_new_message) {
            arrUserDto = new ArrayList<>();
            for (int i = 0; i < resourceUtil.contactDto.getContactList().size(); i++) {
                if (resourceUtil.contactDto.getContactList().get(i).isChecked()) {
                    arrUserDto.add(resourceUtil.contactDto.getContactList().get(i));
                }
            }
//            arrUserDto = listUserNewConversationAdapter.getResultChecked();
            Toast.makeText(this, arrUserDto.size() + "", Toast.LENGTH_SHORT).show();
            List<ParticipantDto> arrParticipantDto = new ArrayList<>();
            for (int i = 0; i < arrUserDto.size(); i++) {
                ParticipantDto participantDto = new ParticipantDto();
                participantDto.setUserId(arrUserDto.get(i).getUserId());
            }
            CreateConversationDto createConversationDto = new CreateConversationDto();
            createConversationDto.setCreatorId(resourceUtil.userDto.getUserId());
            createConversationDto.setParticipants(arrParticipantDto);
            createConversationDto.setTitle(null);
            ChatAPI chatAPI = Constant.getChatAPI();
            Call<CreateConversationDto> call = chatAPI.creatConversation(createConversationDto);
            call.enqueue(new Callback<CreateConversationDto>() {
                @Override
                public void onResponse(Call<CreateConversationDto> call, Response<CreateConversationDto> response) {
                    if (response.isSuccessful()) {
                        CreateConversationDto createConversationDto1 = response.body();
                        Intent intent;
                        ConversationDto conversationDto;
                        switch (createConversationDto.getStatus()) {
                            case Constant.EXSITED:
                                resourceUtil.arrConversationHistorie = createConversationDto.getConversationHistory();
                                intent = new Intent(appCompatActivity, ChatActivity.class);
                                conversationDto = new ConversationDto();
                                intent.putExtra(Constant.CONVERSATION, conversationDto);
                                startActivity(intent);
                                appCompatActivity.finish();
                                break;
                            case Constant.NEW_OK:
                                resourceUtil.arrConversationHistorie = null;
                                intent = new Intent(appCompatActivity, ChatActivity.class);
                                conversationDto = new ConversationDto();
                                intent.putExtra(Constant.CONVERSATION, conversationDto);
                                startActivity(intent);
                                appCompatActivity.finish();
                                break;
                            case Constant.NEW_NOT_OK:
                                Toast.makeText(getApplicationContext(), "Không thành công", Toast.LENGTH_SHORT).show();
                                break;
                            case Constant.ERROR:
                                Toast.makeText(getApplicationContext(), "Không thành công", Toast.LENGTH_SHORT).show();
                                break;
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "Không thành công", Toast.LENGTH_SHORT).show();
                        Log.e("creatConv onResponse", "not successful");
                    }
                }

                @Override
                public void onFailure(Call<CreateConversationDto> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Khong thanh cong", Toast.LENGTH_SHORT).show();
                    Log.e("creatConv onFailure", new Exception(t).getMessage());
                }
            });
        }
        return super.onOptionsItemSelected(item);
    }
}
