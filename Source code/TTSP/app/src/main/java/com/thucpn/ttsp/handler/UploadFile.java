package com.thucpn.ttsp.handler;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;

import com.thucpn.ttsp.common.Config;
import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.model.ItemImage;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class UploadFile extends AsyncTask<ItemImage, String, String> {
    private Context context;
    private Handler handler;

    public UploadFile(Context context, Handler handler) {
        this.context = context;
        this.handler = handler;
    }

    @Override
    protected String doInBackground(ItemImage... itemImages) {
        ItemImage arr = itemImages[0];
        String img = uploadFile(arr.getPath());
        return img;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Message message = new Message();
        message.what = Constant.RESULT_IMG;
        message.obj = s;
        handler.sendMessage(message);
    }

    private String uploadFile(String sourceFileUri) {
        String link = Constant.SERVER_ADDRESS + "upload.php";
        String result = "";
        String fileName = sourceFileUri;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);
        try {
            FileInputStream fileInputStream = new FileInputStream(sourceFile);
            URL url = new URL(link);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true); // Allow Inputs
            conn.setDoOutput(true); // Allow Outputs
            conn.setUseCaches(false); // Don't use a Cached Copy
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("ENCTYPE", "multipart/form-data");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            conn.setRequestProperty("uploaded_file", fileName);
            dos = new DataOutputStream(conn.getOutputStream());
            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=uploaded_file;filename="
                    + fileName + lineEnd);
            dos.writeBytes(lineEnd);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
            int serverResponseCode = conn.getResponseCode();
            if (serverResponseCode == HttpURLConnection.HTTP_OK) {
                InputStreamReader in = new InputStreamReader((InputStream) conn.getContent());
                BufferedReader buff = new BufferedReader(in);
                String line;
                StringBuffer text = new StringBuffer();
                while ((line = buff.readLine()) != null) {
                    text.append(line);
                }
                result = text.toString();
            }
            fileInputStream.close();
            dos.flush();
            dos.close();
        } catch (Exception e) {
            result = "";
            e.printStackTrace();
        }
        return result;
    }

    public String insert(String img) {
        String link = Config.inimg(img);
        try {
            URL url = new URL(link);
            URLConnection connection = url.openConnection();
            connection.setConnectTimeout(5000);
            InputStream inputStream = connection.getInputStream();
            StringBuilder builder = new StringBuilder();
            byte[] b = new byte[1024];
            int count = inputStream.read(b);
            while (count != -1) {
                builder.append(new String(b, 0, count, "UTF-8"));
                count = inputStream.read(b);
            }
            inputStream.close();
            return builder.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
