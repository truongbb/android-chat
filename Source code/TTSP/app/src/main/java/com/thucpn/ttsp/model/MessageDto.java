package com.thucpn.ttsp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MessageDto {

    @SerializedName("id")
    @Expose
    private Long id;

    @SerializedName("conversationId")
    @Expose
    private Long conversationId;

    @SerializedName("senderId")
    @Expose
    private Long senderId;

//    @SerializedName("participantId")
//    @Expose
//    private Long participantId;

    @SerializedName("messageTypeId")
    @Expose
    private Long messageTypeId;

    @SerializedName("messageContent")
    @Expose
    private String messageContent;

    @SerializedName("attatchmenUrl")
    @Expose
    private String attatchmenUrl;

    @SerializedName("sentTime")
    @Expose
    private java.util.Date sentTime;

    @SerializedName("sentTimeLong")
    @Expose
    private Long sentTimeLong;

}
