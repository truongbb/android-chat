package com.thucpn.ttsp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.thucpn.ttsp.R;

import de.hdodenhof.circleimageview.CircleImageView;
import lombok.Getter;

/**
 * Created by thucpn on 22/03/2018.
 */
@Getter
public class ConversationViewHolder extends RecyclerView.ViewHolder {

    private CircleImageView avatarItemConversation;
    private TextView tvTitleConversation;
    private TextView tvTimeConversation;
    private TextView tvLastContentConversation;

    public ConversationViewHolder(View itemView) {
        super(itemView);
        avatarItemConversation = itemView.findViewById(R.id.avatarItemConversation);
        tvTitleConversation = itemView.findViewById(R.id.tvTitleConversation);
        tvTimeConversation = itemView.findViewById(R.id.tvTimeConversation);
        tvLastContentConversation = itemView.findViewById(R.id.tvLastContentConversation);

    }

}
