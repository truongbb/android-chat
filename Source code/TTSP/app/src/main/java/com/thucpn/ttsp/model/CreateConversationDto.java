package com.thucpn.ttsp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateConversationDto {

    @SerializedName("creatorId")
    @Expose
    private Long creatorId;

    @SerializedName("participants")
    @Expose
    private List<ParticipantDto> participants;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("createdTime")
    @Expose
    private java.util.Date createdTime;

    @SerializedName("createdTimeString")
    @Expose
    private String createdTimeString;

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("conversationHistory")
    @Expose
    private List<ConversationHistoryDto> conversationHistory;

    @SerializedName("conversationId")
    @Expose
    private Long conversationId;

    @SerializedName("chatTypeId")
    @Expose
    private Long chatTypeId;

}
