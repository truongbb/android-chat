package com.thucpn.ttsp.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.thucpn.ttsp.R;
import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.common.ResourceUtil;
import com.thucpn.ttsp.handler.DownloadFile;
import com.thucpn.ttsp.model.ConversationHistoryDto;
import com.thucpn.ttsp.model.MessageDto;
import com.thucpn.ttsp.model.UserDto;
import com.thucpn.ttsp.service.ChatAPI;
import com.thucpn.ttsp.viewholder.MessageLeftViewHolder;
import com.thucpn.ttsp.viewholder.MessageRightViewHolder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.thucpn.ttsp.common.Constant.WHAT_ITEM;

@Getter
@Setter
public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ConversationHistoryDto> conversationHistories;
    private ResourceUtil resourceUtil = (ResourceUtil) ResourceUtil.getContext();
    private UserDto userDto = resourceUtil.userDto;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
    private Long chatType;
    private Activity activity;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        if (viewType == Constant.MESSAGE_LEFT) {
            view = inflater.inflate(R.layout.item_message_left, parent, false);
            viewHolder = new MessageLeftViewHolder(view);
        } else {
            view = inflater.inflate(R.layout.item_message_right, parent, false);
            viewHolder = new MessageRightViewHolder(view);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        activity = getActivity();
        if (holder != null) {
            if (holder.getItemViewType() == Constant.MESSAGE_LEFT) {
                MessageLeftViewHolder leftViewHolder = (MessageLeftViewHolder) holder;
                bindMessageLeftViewHolder(leftViewHolder, position);
            } else {
                MessageRightViewHolder rightViewHolder = (MessageRightViewHolder) holder;
                bindMessageRightViewHolder(rightViewHolder, position);
            }
        }
    }

    private void bindMessageLeftViewHolder(MessageLeftViewHolder leftViewHolder, int position) {
        ConversationHistoryDto conversationHistory = conversationHistories.get(position);
        if (conversationHistory.getMessageContent() != null) {
            leftViewHolder.getTvContent_item_message_left().setText(conversationHistory.getMessageContent());
        } else {
            leftViewHolder.getTvContent_item_message_left().setTextColor(leftViewHolder.itemView.getResources().getColor(R.color.colorIconText));
            leftViewHolder.getTvContent_item_message_left().setText(conversationHistory.getFileName());
            leftViewHolder.itemView.setOnClickListener(view -> {
                Dialog dialog = new Dialog(leftViewHolder.itemView.getContext(), android.R.style.Theme_Holo_Dialog_NoActionBar);
                @SuppressLint("HandlerLeak") Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        if (msg.what == WHAT_ITEM) {
                            if (msg.obj.equals(Constant.DOWN_FILE_SUCCESS)) {
                                dialog.dismiss();
                            }
                            Toast.makeText(dialog.getContext(), "Đã lưu", Toast.LENGTH_LONG).show();
                        }
                    }
                };
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.layout_dialog_image_message);
                ImageView imgMessage_DialogMessage = dialog.findViewById(R.id.imgMessage_DialogMessage);
                Button btnExit_DialogMessage = dialog.findViewById(R.id.btnExit_DialogMessage);
                Button btnDown_DialogMessage = dialog.findViewById(R.id.btnDown_DialogMessage);
                Glide.with(leftViewHolder.itemView.getContext()).load(Constant.SERVER_ADDRESS + "img/" + conversationHistory.getFileName()).placeholder(R.drawable.image_loading).error(R.drawable.no_image).into(imgMessage_DialogMessage);
                dialog.show();

                btnExit_DialogMessage.setOnClickListener(view1 -> dialog.dismiss());
                btnDown_DialogMessage.setOnClickListener(view12 -> {
                    DownloadFile downloadFile = new DownloadFile(dialog.getContext(), handler);
                    downloadFile.execute(conversationHistory.getFileName());
                });
            });
        }
        leftViewHolder.getTvTime_item_message_left().setText(dateFormat.format(conversationHistory.getSentTimeLong()).toString());

    }

    private void bindMessageRightViewHolder(MessageRightViewHolder rightViewHolder, int position) {
        ConversationHistoryDto conversationHistory = conversationHistories.get(position);
        if (conversationHistory.getMessageContent() != null) {
            rightViewHolder.getTvContent_item_message_right().setText(conversationHistory.getMessageContent());
        } else {
            rightViewHolder.getTvContent_item_message_right().setTextColor(rightViewHolder.itemView.getResources().getColor(R.color.colorIconText));
            rightViewHolder.getTvContent_item_message_right().setText(conversationHistory.getFileName());
            rightViewHolder.itemView.setOnClickListener(view -> {
                Dialog dialog = new Dialog(rightViewHolder.itemView.getContext(), android.R.style.Theme_Holo_Dialog_NoActionBar);
                @SuppressLint("HandlerLeak") Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        if (msg.what == WHAT_ITEM) {
                            if (msg.obj.equals(Constant.DOWN_FILE_SUCCESS)) {
                                dialog.dismiss();
                            }
                            Toast.makeText(dialog.getContext(), "Đã lưu", Toast.LENGTH_LONG).show();
                        }
                    }
                };
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.layout_dialog_image_message);
                ImageView imgMessage_DialogMessage = dialog.findViewById(R.id.imgMessage_DialogMessage);
                Button btnExit_DialogMessage = dialog.findViewById(R.id.btnExit_DialogMessage);
                Button btnDown_DialogMessage = dialog.findViewById(R.id.btnDown_DialogMessage);
                Glide.with(rightViewHolder.itemView.getContext()).load(Constant.SERVER_ADDRESS + "img/" + conversationHistory.getFileName()).placeholder(R.drawable.image_loading).error(R.drawable.no_image).into(imgMessage_DialogMessage);
                dialog.show();
                btnExit_DialogMessage.setOnClickListener(view1 -> dialog.dismiss());
                btnDown_DialogMessage.setOnClickListener(view12 -> {
                    DownloadFile downloadFile = new DownloadFile(dialog.getContext(), handler);
                    downloadFile.execute(conversationHistory.getFileName());
                });
            });
        }
        rightViewHolder.getTvTime_item_message_right().setText(dateFormat.format(conversationHistory.getSentTimeLong()).toString());
    }


    @Override
    public int getItemCount() {
        return conversationHistories.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (conversationHistories.get(position).getSenderId() != userDto.getUserId()) {
            return Constant.MESSAGE_LEFT;
        } else {
            return Constant.MESSAGE_RIGHT;
        }
    }
}
