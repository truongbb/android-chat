package com.thucpn.ttsp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.thucpn.ttsp.R;

import de.hdodenhof.circleimageview.CircleImageView;
import lombok.Getter;

@Getter
public class FriendRequestViewHolder extends RecyclerView.ViewHolder {
    private CircleImageView avatarItemFriendRequest;
    private TextView tvUserItemFriendRequest;
    private TextView tvPhoneFriendRequest;
    private Button btnAccept_FriendRequest;
    private Button btnCancel_FriendRequest;

    public FriendRequestViewHolder(final View itemView) {
        super(itemView);
        avatarItemFriendRequest = itemView.findViewById(R.id.avatarItemFriendRequest);
        tvUserItemFriendRequest = itemView.findViewById(R.id.tvUserItemFriendRequest);
        tvPhoneFriendRequest = itemView.findViewById(R.id.tvPhoneFriendRequest);
        btnAccept_FriendRequest = itemView.findViewById(R.id.btnAccept_FriendRequest);
        btnCancel_FriendRequest = itemView.findViewById(R.id.btnCancel_FriendRequest);
    }

}
