package com.thucpn.ttsp.adapter;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thucpn.ttsp.R;
import com.thucpn.ttsp.activity.ProfileActivity;
import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.common.ResourceUtil;
import com.thucpn.ttsp.model.UserDto;
import com.thucpn.ttsp.viewholder.SearchUserViewHolder;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchUserAdapter extends RecyclerView.Adapter<SearchUserViewHolder> {
    private List<UserDto> arrUserDto;
    private AppCompatActivity context;
    private ResourceUtil resourceUtil = (ResourceUtil) ResourceUtil.getContext();

    @Override
    public SearchUserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_search_user, parent, false);
        return new SearchUserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SearchUserViewHolder holder, int position) {
        final UserDto userDto = arrUserDto.get(position);
        holder.getTvUserItem_SearchResult().setText(userDto.getFullName());
        holder.getTvPhone_SearchResult().setText(userDto.getAddress());

        int checkLength = checkLengthPixel(holder.getTvPhone_SearchResult(), userDto.getAddress());
        if (checkLength != -1) {
            holder.getTvPhone_SearchResult().setText(ellipsize(userDto.getAddress(), checkLength));
        }

        if (userDto.getGenderId() == Constant.UNKNOWN) {

        } else {
            if (userDto.getGenderId() == Constant.MALE) {
                holder.getAvatarItemUser_SearchResult().setImageResource(R.drawable.img_male_avatar);
            } else {
                holder.getAvatarItemUser_SearchResult().setImageResource(R.drawable.img_female_avatar);
            }
        }

        holder.itemView.setOnClickListener(v -> {
            int action = Constant.ACTION_CHAT;
            if (userDto.getIsContact() == Constant.IS_CONTACT) {
                action = Constant.ACTION_CHAT;
            } else if (userDto.getIsSent() != Constant.IS_SENT) {
                action = Constant.ACTION_FRIEND_REQUEST;
            } else if (userDto.getIsSent() == Constant.IS_SENT && userDto.getIsSender() == Constant.IS_SENDER) {
                action = Constant.ACTION_WAIT_FRIEND_REQUEST;
            } else if (userDto.getIsSent() == Constant.IS_SENT && userDto.getIsSender() != Constant.IS_SENDER) {
                action = Constant.ACTION_SENT_FRIEND_REQUEST;
            }
            Intent intent = new Intent(holder.itemView.getContext(), ProfileActivity.class);
            intent.putExtra(Constant.USER_SENT, userDto);
            intent.putExtra(Constant.ACTION, action);
            intent.putExtra(Constant.ACTIVITY_PARENT, Constant.SEARCH_USER_ACTIVITY);
            holder.itemView.getContext().startActivity(intent);
            context.finish();
        });
    }

    private int checkLengthPixel(TextView textView, String input) {
        Rect bounds = new Rect();
        Paint textPaint = textView.getPaint();
        textPaint.getTextBounds(input, 0, input.length(), bounds);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int maxLength = -1;
        int boundWidth = bounds.width();
        int displayWidth = (int) (displayMetrics.widthPixels * 0.7);
        if (boundWidth > displayWidth) {
            int i = input.length() - 1;
            while (i >= 0) {
                Rect newBounds = new Rect();
                Paint newTextPain = textView.getPaint();
                newTextPain.getTextBounds(input, 0, i, newBounds);
                if (newBounds.width() <= displayMetrics.widthPixels) {
                    maxLength = i;
                    break;
                } else {
                    i--;
                }
            }
        }
        return maxLength;
    }

    private String ellipsize(String input, int maxLength) {
        if (input == null || input.length() <= maxLength) {
            return input;
        }
        return input.substring(0, maxLength - 3) + "...";
    }

    @Override
    public int getItemCount() {
        return arrUserDto.size();
    }
}
