package com.thucpn.ttsp.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.thucpn.ttsp.R;
import com.thucpn.ttsp.activity.ChatActivity;
import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.common.DatabaseUtil;
import com.thucpn.ttsp.common.ResourceUtil;
import com.thucpn.ttsp.model.ConversationDto;
import com.thucpn.ttsp.model.ConversationHistoryDto;
import com.thucpn.ttsp.model.LastSeen;
import com.thucpn.ttsp.service.ChatAPI;
import com.thucpn.ttsp.viewholder.ConversationViewHolder;

import java.text.SimpleDateFormat;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by thucpn on 22/03/2018.
 */
@Getter
@Setter
public class ConversationAdapter extends RecyclerView.Adapter<ConversationViewHolder> {
    private List<ConversationDto> arrConversation;
    private Activity context;

    @Override
    public ConversationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_conversation, parent, false);
        return new ConversationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ConversationViewHolder holder, final int position) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
        final ConversationDto conversationDto = arrConversation.get(position);
//        if (conversationDto.isSeen()) {
//            holder.getTvTitleConversation().setTypeface(null, Typeface.BOLD);
//            holder.getTvLastContentConversation().setTextColor(holder.itemView.getResources().getColor(R.color.colorPrimary));
//        }
        if (conversationDto.getChatTypeId() == Constant.PAIR) {
            switch (conversationDto.getFriendGenderId() + "") {
                case Constant.MALE + "":
                    holder.getAvatarItemConversation().setImageResource(R.drawable.img_male_avatar);
                    break;
                case Constant.FEMALE + "":
                    holder.getAvatarItemConversation().setImageResource(R.drawable.img_female_avatar);
                    break;
            }
        } else {
            holder.getAvatarItemConversation().setImageResource(R.drawable.ic_group_dark);
        }
        holder.getTvTitleConversation().setText(conversationDto.getTitle());
        holder.getTvTimeConversation().setText(dateFormat.format(conversationDto.getUpdatedTimeLong()));
        if (conversationDto.getLastMessage() != null) {
            holder.getTvLastContentConversation().setText(conversationDto.getLastMessage());
        } else {
            holder.getTvLastContentConversation().setText(conversationDto.getFileName());
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrConversation.get(position).setSeen(true);
                conversationDto.setId(conversationDto.getId());
                ChatAPI chatAPI = Constant.getChatAPI();
                Call<List<ConversationHistoryDto>> call = chatAPI.getAllMessage(conversationDto);
                call.enqueue(new Callback<List<ConversationHistoryDto>>() {
                    @Override
                    public void onResponse(Call<List<ConversationHistoryDto>> call, Response<List<ConversationHistoryDto>> response) {
                        if (response.isSuccessful()) {
                            DatabaseUtil databaseUtil = DatabaseUtil.getInstance(holder.itemView.getContext());
                            LastSeen lastSeen = new LastSeen();
                            lastSeen.setConversationId(arrConversation.get(position).getId());
                            lastSeen.setTime(System.currentTimeMillis());
                            if (databaseUtil.getLastSeenByConversationId(lastSeen.getConversationId()) != null) {
                                databaseUtil.updateLastSeen(lastSeen);
                            } else {
                                databaseUtil.insertLastSeen(lastSeen);
                            }
                            List<ConversationHistoryDto> conversationHistories = response.body();
                            ResourceUtil resourceUtil = (ResourceUtil) ResourceUtil.getContext();
                            resourceUtil.arrConversationHistorie = conversationHistories;
                            Intent intent = new Intent(holder.itemView.getContext(), ChatActivity.class);
                            intent.putExtra(Constant.CONVERSATION, conversationDto);
                            holder.itemView.getContext().startActivity(intent);
//                            context.finish();
                        } else {
                            Log.e("getAllMess onResponse", "not successfull");
                        }
                    }

                    @Override
                    public void onFailure(Call<List<ConversationHistoryDto>> call, Throwable t) {
                        Log.e("getAllMess onFailure", new Exception(t).getMessage());
                    }
                });
                Intent intent = new Intent(holder.itemView.getContext(), ChatActivity.class);
                intent.putExtra(Constant.CONVERSATION, conversationDto);
                holder.itemView.getContext().startActivity(intent);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                PopupMenu popupMenu = new PopupMenu(holder.itemView.getContext(), holder.itemView);
                popupMenu.getMenuInflater().inflate(R.menu.menu_list_conversation_popup, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(item -> {
                    final AlertDialog alertDialog = new AlertDialog.Builder(holder.itemView.getContext()).create();
                    alertDialog.setMessage(holder.itemView.getResources().getString(R.string.alert_delete_conversation));
                    alertDialog.setCancelable(true);
                    alertDialog.setButton(alertDialog.BUTTON_POSITIVE, holder.itemView.getResources().getString(R.string.delete), (dialog, which) -> {
                        ConversationDto remove = arrConversation.remove(position);
                        notifyDataSetChanged();
                        ChatAPI chatAPI = Constant.getChatAPI();
                        Call<List<ConversationDto>> call = chatAPI.deleteConversation(remove);
                        call.enqueue(new Callback<List<ConversationDto>>() {
                            @Override
                            public void onResponse(Call<List<ConversationDto>> call, Response<List<ConversationDto>> response) {
                                if (response.isSuccessful()) {
                                    DatabaseUtil databaseUtil = DatabaseUtil.getInstance(holder.itemView.getContext());
                                    LastSeen lastSeen = new LastSeen();
                                    lastSeen.setConversationId(remove.getId());
                                    databaseUtil.deleteOneLastSeen(lastSeen);
                                    List<ConversationDto> arrConversationDto = response.body();
                                    ResourceUtil resourceUtil = (ResourceUtil) holder.itemView.getContext().getApplicationContext();
                                    resourceUtil.setArrConversationDto(arrConversationDto);
                                    Snackbar snackbar = Snackbar.make(holder.itemView, holder.itemView.getResources().getText(R.string.confirm_delete), Snackbar.LENGTH_SHORT);
                                    snackbar.show();
                                } else {
                                    Log.e("Call del conversation", response.message());
                                }
                            }

                            @Override
                            public void onFailure(Call<List<ConversationDto>> call, Throwable t) {
                                Log.e("ConnectAPI del conv", new Exception(t).getMessage());
                            }
                        });
                        Toast.makeText(holder.itemView.getContext(), "xoas", Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();
                    });
                    alertDialog.setButton(alertDialog.BUTTON_NEGATIVE, holder.itemView.getResources().getString(R.string.cancel), (dialog, which) -> alertDialog.dismiss());
                    alertDialog.show();
                    return true;
                });
                popupMenu.show();
//                Toast.makeText(holder.itemView.getContext(), "long click", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrConversation.size();
    }

}
