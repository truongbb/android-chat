package com.thucpn.ttsp.handler;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.model.ConversationDto;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadFile extends AsyncTask<String, Void, String> {
    private Context context;
    private Handler handler;

    public DownloadFile(Context context, Handler handler) {
        this.context = context;
        this.handler = handler;
    }

    @Override
    protected String doInBackground(String... params) {
        InputStream input = null;
        FileOutputStream output = null;
        HttpURLConnection connection = null;
        String fileName = params[0];
        try {
            URL url = new URL(Constant.SERVER_ADDRESS + "/img" + fileName);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            String path = Constant.PATH + "/" + fileName;
            File file = new File(path);
            file.getParentFile().mkdirs();
            file.createNewFile();

            input = connection.getInputStream();
            output = new FileOutputStream(file);

            byte data[] = new byte[1024];
            int count = input.read(data);
            while ((count) != -1) {
                if (isCancelled()) {
                    input.close();
                    return null;
                }
                output.write(data, 0, count);
                count = input.read(data);
            }
            output.close();
            input.close();
            return Constant.DOWN_FILE_SUCCESS;
        } catch (Exception e) {
            Log.e("xxxx", e.toString());
            return null;
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Message message = new Message();
        message.what = Constant.WHAT_ITEM;
        message.obj = s;
        handler.sendMessage(message);
    }
}
