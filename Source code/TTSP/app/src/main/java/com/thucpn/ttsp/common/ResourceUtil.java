package com.thucpn.ttsp.common;

import android.app.Application;
import android.content.Context;

import com.thucpn.ttsp.model.ContactDto;
import com.thucpn.ttsp.model.ConversationDto;
import com.thucpn.ttsp.model.ConversationHistoryDto;
import com.thucpn.ttsp.model.FriendRequestDto;
import com.thucpn.ttsp.model.MessageDto;
import com.thucpn.ttsp.model.UserDto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by thucpn on 21/03/2018.
 */
@Getter
@Setter
public class ResourceUtil extends Application {
    public UserDto userDto;
    public ContactDto contactDto;
    public FriendRequestDto friendRequestDto;
    public List<ConversationDto> arrConversationDto;
    public List<MessageDto> arrMessageDto;
    public List<ConversationHistoryDto> arrConversationHistorie;

    public String keySearchUserDto;

    private static Context mContext;
    public List<UserDto> arrSearchUserResult;
    public ConversationDto conversationDto = new ConversationDto();

    public static Context getContext() {
        return mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }
}
