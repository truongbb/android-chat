package com.thucpn.ttsp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thucpn.ttsp.common.MySharedPreferences;
import com.thucpn.ttsp.common.ResourceUtil;
import com.thucpn.ttsp.model.UserDto;
import com.thucpn.ttsp.service.ChatAPI;

/**
 * Created by thucpn on 21/02/2018.
 */

public abstract class BaseFragment extends Fragment {
    protected int viewID;
    protected ResourceUtil resourceUtil = (ResourceUtil) ResourceUtil.getContext();
    protected MySharedPreferences mySharedPreferences;
    protected UserDto userDto;
    protected ChatAPI chatAPI;
    protected AppCompatActivity activity = (AppCompatActivity) this.getActivity();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getViewID(), container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        init();
    }

    protected abstract int getViewID();

    protected abstract void init();

}
