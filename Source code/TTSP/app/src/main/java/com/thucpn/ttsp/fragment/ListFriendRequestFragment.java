package com.thucpn.ttsp.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mikepenz.itemanimators.SlideUpAlphaAnimator;
import com.thucpn.ttsp.R;
import com.thucpn.ttsp.activity.SearchUserActivity;
import com.thucpn.ttsp.adapter.FriendRequestAdapter;
import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.model.UserDto;
import com.thucpn.ttsp.service.ChatAPI;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListFriendRequestFragment extends BaseFragment {
    private RelativeLayout layout_none_friend_request;
    private RecyclerView recyclerViewFriendRequest;
    private List<UserDto> arrFriendRequestDto;
    private FloatingActionButton fab_addFriend;
    private Dialog dialogSearchUser;
    private List<UserDto> arrResult;

    public ListFriendRequestFragment() {
        initData();
    }

    private void initData() {
        if (resourceUtil.friendRequestDto != null) {
            arrFriendRequestDto = resourceUtil.friendRequestDto.getRequestedList();
        }
    }

    @Override
    protected int getViewID() {
        return R.layout.demo_list_friend_request;
    }

    @Override
    protected void init() {
        initData();
        layout_none_friend_request = getActivity().findViewById(R.id.layout_none_friend_request);
        recyclerViewFriendRequest = getActivity().findViewById(R.id.recyclerViewFriendRequest);
        fab_addFriend = getActivity().findViewById(R.id.fab_addFriend);
        fab_addFriend.setOnClickListener(view -> {
            initDialog();
        });

        if (arrFriendRequestDto == null || arrFriendRequestDto.size() == 0) {
            layout_none_friend_request.setVisibility(View.VISIBLE);
            recyclerViewFriendRequest.setVisibility(View.GONE);
        } else {
            layout_none_friend_request.setVisibility(View.GONE);
            recyclerViewFriendRequest.setVisibility(View.VISIBLE);
            FriendRequestAdapter friendRequestAdapter = new FriendRequestAdapter();
            friendRequestAdapter.setArrFriendRequestDto(arrFriendRequestDto);
            friendRequestAdapter.setContext(getActivity());
            StaggeredGridLayoutManager gridLayoutManager =
                    new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
            recyclerViewFriendRequest.setLayoutManager(gridLayoutManager);
            recyclerViewFriendRequest.setAdapter(friendRequestAdapter);
            recyclerViewFriendRequest.setItemAnimator(new SlideUpAlphaAnimator());
            recyclerViewFriendRequest.setHasFixedSize(true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        init();
    }

    private void initDialog() {
        dialogSearchUser = new Dialog(getContext(), android.R.style.Theme_Material_Dialog);
        dialogSearchUser.setContentView(R.layout.layout_search_user_dialog);
        dialogSearchUser.setCancelable(true);
        dialogSearchUser.setTitle(getResources().getString(R.string.search_by_name));
        final TextInputLayout tilUserInfo_SearchUser = dialogSearchUser.findViewById(R.id.tilUserInfo_SearchUser);
        final EditText edtUserInfo_SearchUser = dialogSearchUser.findViewById(R.id.edtUserInfo_SearchUser);
        Button btnCancel_SearchUser = dialogSearchUser.findViewById(R.id.btnCancel_SearchUser);
        Button btnSearch_SearchUser = dialogSearchUser.findViewById(R.id.btnSearch_SearchUser);
        btnCancel_SearchUser.setOnClickListener(v -> {
            dialogSearchUser.dismiss();
        });

        btnSearch_SearchUser.setOnClickListener(new View.OnClickListener() {
            String search;

            @Override
            public void onClick(final View v) {
                search = edtUserInfo_SearchUser.getText().toString().trim();
                if (search == null || search.length() == 0) {
                    tilUserInfo_SearchUser.setError(getResources().getString(R.string.information) + " " + getResources().getString(R.string.not_valid));
                } else {
                    resourceUtil.keySearchUserDto = search;
                    ChatAPI chatAPI = Constant.getChatAPI();
                    UserDto userDto = new UserDto();
                    userDto.setFirstName(search);
                    userDto.setUserId(resourceUtil.userDto.getUserId());
                    Call<List<UserDto>> call = chatAPI.searchUser(userDto);
                    call.enqueue(new Callback<List<UserDto>>() {
                        @Override
                        public void onResponse(Call<List<UserDto>> call, Response<List<UserDto>> response) {
                            if (response.isSuccessful()) {
                                arrResult = response.body();
                                if (arrResult == null || arrResult.size() == 0) {
                                    dialogSearchUser.dismiss();
                                    Toast.makeText(getContext(), "Không có kết quả", Toast.LENGTH_SHORT).show();
                                    Log.e("searchUser onResponse", "successfull-null");
                                } else {
                                    dialogSearchUser.dismiss();
                                    Intent intent = new Intent(getActivity(), SearchUserActivity.class);
                                    intent.putParcelableArrayListExtra(Constant.SEARCH_USER_RESULT, (ArrayList<UserDto>) arrResult);
                                    startActivity(intent);
                                }
                            } else {
                                dialogSearchUser.dismiss();
                                Toast.makeText(getContext(), "Không có kết quả", Toast.LENGTH_SHORT).show();
                                Log.e("searchUser onResponse", "not successfull");
                            }
                        }

                        @Override
                        public void onFailure(Call<List<UserDto>> call, Throwable t) {
                            dialogSearchUser.dismiss();
                            Toast.makeText(getContext(), "Không có kết quả", Toast.LENGTH_SHORT).show();
                            Log.e("searchUser onFailure", new Exception(t).getMessage());
                        }
                    });
                }
            }
        });
        dialogSearchUser.show();
    }
}
