package com.thucpn.ttsp.common;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.thucpn.ttsp.model.LastSeen;
import com.thucpn.ttsp.model.UserDto;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DatabaseUtil {
    private Context context;
    private SQLiteDatabase sqLiteDatabase;

    private static DatabaseUtil DatabaseUtil = null;

    private DatabaseUtil(Context context) {
        this.context = context;
        copyFileToDevice();
    }

    public static DatabaseUtil getInstance(Context context) {
        if (DatabaseUtil == null) {
            DatabaseUtil = new DatabaseUtil(context);
        }
        return DatabaseUtil;
    }

    private void copyFileToDevice() {
        File file = new File(Constant.PATH_DB + Constant.DB_NAME);
        if (!file.exists()) {
            File parent = file.getParentFile();
            parent.mkdirs();
            try {
                InputStream inputStream = context.getAssets().open(Constant.DB_NAME);
                FileOutputStream outputStream = new FileOutputStream(file);
                byte[] b = new byte[1024];
                int count = inputStream.read(b);
                while (count != -1) {
                    outputStream.write(b, 0, count);
                    count = inputStream.read(b);
                }
                inputStream.close();
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void openDatabase() {
        sqLiteDatabase = context.openOrCreateDatabase(Constant.DB_NAME, context.MODE_PRIVATE, null);
    }

    private void closeDatabase() {
        sqLiteDatabase.close();
    }

    public UserDto getLastUser() {
        UserDto userDto = null;
        openDatabase();
        Cursor cursor = sqLiteDatabase.query(Constant.USER_TABLE_NAME, null, null, null, null, null, null);
        cursor.moveToFirst();
        int indexID = cursor.getColumnIndex(Constant.USER_ID);
        int indexFirstName = cursor.getColumnIndex(Constant.USER_FIRST_NAME);
        int indexLastName = cursor.getColumnIndex(Constant.USER_LAST_NAME);
        int indexAddress = cursor.getColumnIndex(Constant.USER_ADDRESS);
        int indexEmail = cursor.getColumnIndex(Constant.USER_EMAIL);
        int indexPhone = cursor.getColumnIndex(Constant.USER_PHONE);
        int indexGenderId = cursor.getColumnIndex(Constant.USER_GENDER_ID);
        int indexGender = cursor.getColumnIndex(Constant.USER_GENDER);
        int indexBirthdayStr = cursor.getColumnIndex(Constant.USER_BIRTHDAY_STR);
        int indexAccountId = cursor.getColumnIndex(Constant.USER_ACCOUNT_ID);
        int indexUsername = cursor.getColumnIndex(Constant.USER_USERNAME);
        int indexPassword = cursor.getColumnIndex(Constant.USER_PASSWORD);

        while (!cursor.isAfterLast()) {
            int id = cursor.getInt(indexID);
            String firstName = cursor.getString(indexFirstName);
            String lastName = cursor.getString(indexLastName);
            String address = cursor.getString(indexAddress);
            String email = cursor.getString(indexEmail);
            String phone = cursor.getString(indexPhone);
            int genderId = cursor.getInt(indexGenderId);
            String gender = cursor.getString(indexGender);
            String birthdayStr = cursor.getString(indexBirthdayStr);
            int accountId = cursor.getInt(indexAccountId);
            String username = cursor.getString(indexUsername);
            String password = cursor.getString(indexPassword);
            userDto = new UserDto();
            userDto.setUserId((long) id);
            userDto.setFirstName(firstName);
            userDto.setLastName(lastName);
            userDto.setAddress(address);
            userDto.setEmail(email);
            userDto.setPhone(phone);
            userDto.setGenderId((long) genderId);
            userDto.setGender(gender);
            userDto.setBirthdayStr(birthdayStr);
            try {
                Date birthday = new SimpleDateFormat("dd-MM-yyyy").parse(birthdayStr);
                userDto.setBirthday(birthday);
            } catch (ParseException e) {
                return null;
            }
            userDto.setAccountId((long) accountId);
            userDto.setUsername(username);
            userDto.setPassword(password);
            cursor.moveToNext();
        }

        closeDatabase();
        return userDto;
    }

    public boolean insertLastUser(UserDto userDto) {
        openDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constant.USER_ID, userDto.getUserId());
        contentValues.put(Constant.USER_FIRST_NAME, userDto.getFirstName());
        contentValues.put(Constant.USER_LAST_NAME, userDto.getLastName());
        contentValues.put(Constant.USER_ADDRESS, userDto.getAddress());
        contentValues.put(Constant.USER_EMAIL, userDto.getEmail());
        contentValues.put(Constant.USER_PHONE, userDto.getPhone());
        contentValues.put(Constant.USER_GENDER_ID, userDto.getGenderId());
        contentValues.put(Constant.USER_GENDER, userDto.getGender());
        contentValues.put(Constant.USER_BIRTHDAY_STR, new SimpleDateFormat("dd-MM-yyyy").format(userDto.getBirthday()));
        contentValues.put(Constant.USER_ACCOUNT_ID, userDto.getAccountId());
        contentValues.put(Constant.USER_USERNAME, userDto.getUsername());
        contentValues.put(Constant.USER_PASSWORD, userDto.getPassword());
        long insert = sqLiteDatabase.insert(Constant.USER_TABLE_NAME, null, contentValues);
        closeDatabase();
        if (insert == 0L) {
            return false;
        } else {
            return true;
        }
    }

    public boolean updateUser(UserDto userDto) {
        openDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constant.USER_FIRST_NAME, userDto.getFirstName());
        contentValues.put(Constant.USER_LAST_NAME, userDto.getLastName());
        contentValues.put(Constant.USER_ADDRESS, userDto.getAddress());
        contentValues.put(Constant.USER_EMAIL, userDto.getEmail());
        contentValues.put(Constant.USER_PHONE, userDto.getPhone());
        contentValues.put(Constant.USER_GENDER_ID, userDto.getGenderId());
        contentValues.put(Constant.USER_GENDER, userDto.getGender());
        contentValues.put(Constant.USER_BIRTHDAY_STR, userDto.getBirthdayStr());
        contentValues.put(Constant.USER_ACCOUNT_ID, userDto.getAccountId());
        contentValues.put(Constant.USER_USERNAME, userDto.getUsername());
        contentValues.put(Constant.USER_PASSWORD, userDto.getPassword());
        String where = Constant.USER_ID + " = ?";
        String[] whereAgrs = {userDto.getUserId() + ""};
        int update = sqLiteDatabase.update(Constant.USER_TABLE_NAME, contentValues, where, whereAgrs);
        closeDatabase();
        if (update == 0) {
            return false;
        } else {
            return true;
        }
    }

    public boolean deleteUser() {
        openDatabase();
//        String where = Constant.USER_ID + " = ?";
//        String[] whereAgrs = {userId + ""};
        String where = null;
        String[] whereAgrs = null;
        int delete = sqLiteDatabase.delete(Constant.USER_TABLE_NAME, where, whereAgrs);
        closeDatabase();
        if (delete == 0) {
            return false;
        } else {
            return true;
        }
    }

    public ArrayList<LastSeen> getAllLastSeen() {
        ArrayList<LastSeen> arrLastSeen = null;
        openDatabase();
        Cursor cursor = sqLiteDatabase.query(Constant.SEEN_TABLE_NAME, null, null, null, null, null, null);
        cursor.moveToFirst();
        int indexConversationId = cursor.getColumnIndex(Constant.SEEN_CONNVERSATION_ID);
        int indexTime = cursor.getColumnIndex(Constant.SEEN_TIME);
        while (!cursor.isAfterLast()) {
            arrLastSeen = new ArrayList<>();
            long conversationId = cursor.getInt(indexConversationId);
            long time = cursor.getInt(indexTime);
            LastSeen lastSeen = new LastSeen(conversationId, time);
            arrLastSeen.add(lastSeen);
            cursor.moveToNext();
        }
        closeDatabase();
        return arrLastSeen;
    }

    public LastSeen getLastSeenByConversationId(Long id) {
        LastSeen lastSeen = null;
        openDatabase();
        Cursor cursor = sqLiteDatabase.query(Constant.SEEN_TABLE_NAME, null, null, null, null, null, null);
        cursor.moveToFirst();
        int indexConversationId = cursor.getColumnIndex(Constant.SEEN_CONNVERSATION_ID);
        int indexTime = cursor.getColumnIndex(Constant.SEEN_TIME);
        while (!cursor.isAfterLast()) {
            long conversationId = cursor.getInt(indexConversationId);
            long time = cursor.getInt(indexTime);
            if (conversationId == id) {
                lastSeen = new LastSeen();
                lastSeen.setConversationId(conversationId);
                lastSeen.setTime(time);
                break;
            }
            cursor.moveToNext();
        }
        closeDatabase();
        return lastSeen;
    }

    public boolean insertLastSeen(LastSeen lastSeen) {
        openDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constant.SEEN_CONNVERSATION_ID, lastSeen.getConversationId());
        contentValues.put(Constant.SEEN_TIME, lastSeen.getTime());
        long insert = sqLiteDatabase.insert(Constant.SEEN_TABLE_NAME, null, contentValues);
        openDatabase();
        if (insert == 0L) {
            return false;
        } else {
            return true;
        }
    }

    public boolean updateLastSeen(LastSeen lastSeen) {
        openDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constant.SEEN_TIME, lastSeen.getTime());
        String where = Constant.SEEN_CONNVERSATION_ID + " = ?";
        String[] whereAgrs = {lastSeen.getConversationId() + ""};
        int update = sqLiteDatabase.update(Constant.SEEN_TABLE_NAME, contentValues, where, whereAgrs);
        closeDatabase();
        if (update == 0) {
            return false;
        } else {
            return true;
        }
    }

    public boolean deleteOneLastSeen(LastSeen lastSeen) {
        openDatabase();
        String where = Constant.SEEN_CONNVERSATION_ID + " = ?";
        String[] whereAgrs = {lastSeen.getConversationId() + ""};
        int delete = sqLiteDatabase.delete(Constant.SEEN_TABLE_NAME, where, whereAgrs);
        closeDatabase();
        if (delete == 0) {
            return false;
        } else {
            return true;
        }
    }

    public boolean deleteAllLastSeen() {
        openDatabase();
        int delete = sqLiteDatabase.delete(Constant.SEEN_TABLE_NAME, null, null);
        closeDatabase();
        if (delete == 0) {
            return false;
        } else {
            return true;
        }
    }

}
