package com.thucpn.ttsp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.thucpn.ttsp.R;

import de.hdodenhof.circleimageview.CircleImageView;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserNewConversationViewHolder extends RecyclerView.ViewHolder {
    private CircleImageView avatarUser_NewConversation;
    private TextView tvUserItem_NewConversation;
    private CheckBox cbSelectUser_NewConversation;

    public UserNewConversationViewHolder(View itemView) {
        super(itemView);
        avatarUser_NewConversation = itemView.findViewById(R.id.avatarUser_NewConversation);
        tvUserItem_NewConversation = itemView.findViewById(R.id.tvUserItem_NewConversation);
//        cbSelectUser_NewConversation = itemView.findViewById(R.id.cbSelectUser_NewConversation);
    }
}
