package com.thucpn.ttsp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FriendRequestDto {

    @SerializedName("userId")
    @Expose
    private Long userId;

    @SerializedName("accountId")
    @Expose
    private Long accountId;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("senderId")
    @Expose
    private Long senderId;

    @SerializedName("requestedTime")
    @Expose
    private String requestedTime;

    @SerializedName("requestedListString")
    @Expose
    private String requestedListString;

    @SerializedName("requestedList")
    @Expose
    private List<UserDto> requestedList;
}
