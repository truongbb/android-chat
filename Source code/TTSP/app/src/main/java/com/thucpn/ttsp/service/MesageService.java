package com.thucpn.ttsp.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.thucpn.ttsp.common.Constant;
import com.thucpn.ttsp.common.DatabaseUtil;
import com.thucpn.ttsp.common.ResourceUtil;
import com.thucpn.ttsp.model.ContactDto;
import com.thucpn.ttsp.model.ConversationDto;
import com.thucpn.ttsp.model.ConversationHistoryDto;
import com.thucpn.ttsp.model.FriendRequestDto;
import com.thucpn.ttsp.model.LastSeen;
import com.thucpn.ttsp.model.UserDto;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MesageService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private ConversationDto conversationDto;
    private Handler handler = new Handler();
    private Runnable runnable = () -> {
        ResourceUtil resourceUtil = (ResourceUtil) ResourceUtil.getContext();
        ChatAPI chatAPI = Constant.getChatAPI();
        Call<List<ConversationHistoryDto>> listCall=chatAPI.getAllMessage(conversationDto);
        listCall.enqueue(new Callback<List<ConversationHistoryDto>>() {
            @Override
            public void onResponse(Call<List<ConversationHistoryDto>> call, Response<List<ConversationHistoryDto>> response) {

            }

            @Override
            public void onFailure(Call<List<ConversationHistoryDto>> call, Throwable t) {

            }
        });

    };
}
