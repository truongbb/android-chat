create sequence gender_seq start with 1 increment by 1 cache 1000;
create sequence users_seq start with 1 increment by 1 cache 1000;
create sequence account_seq start with 1 increment by 1 cache 1000;
create sequence contact_seq start with 1 increment by 1 cache 1000;
create sequence friend_seq start with 1 increment by 1 cache 1000;
create sequence conversation_seq start with 1 increment by 1 cache 1000;
create sequence participant_seq start with 1 increment by 1 cache 1000;
create sequence chat_type_seq start with 1 increment by 1 cache 1000;
create sequence message_seq start with 1 increment by 1 cache 1000;
create sequence message_type_seq start with 1 increment by 1 cache 1000;
commit;