--gender
insert into gender (id, gender) values (gender_seq.nextval, 'Nam');
insert into gender (id, gender) values (gender_seq.nextval, 'Nữ');
insert into gender (id, gender) values (gender_seq.nextval, 'Không xác định');

--chat_type
insert into chat_type (id, type) values (chat_type_seq.nextval, 'Pair');
insert into chat_type (id, type) values (chat_type_seq.nextval, 'Group');

--message_type
insert into message_type (id, type) values (message_type_seq.nextval, 'Text');
insert into message_type (id, type) values (message_type_seq.nextval, 'File');

--account
insert into account (id, username, password) values (account_seq.nextval, 'truongbb', 'admin');
insert into account (id, username, password) values (account_seq.nextval, 'demo', '123');
insert into account (id, username, password) values (account_seq.nextval, 'android', '123');

--users
insert into users (id, first_name, last_name, address, email, phone, gender_id, birthday, account_id)
    values (users_seq.nextval ,'Bùi', 'Trường', 'Số 16 ngõ 5,Ao Sen,Mộ Lao,Hà Đông,Hà Nội', 'zhangzen96@outlook.com' , '01648954110', 1, to_date('19960108', 'YYYYMMDD'), 1);
    
insert into users (id, first_name, last_name, address, email, phone, gender_id, birthday, account_id)
    values (users_seq.nextval ,'Android', 'Demo', 'Số 67,Triều Khúc,Nguyễn Trãi,Hà Đông,Hà Nội', 'abc@gmail.com', '0147852369', 2, to_date('19970525', 'YYYYMMDD'), 2);

insert into users (id, first_name, last_name, address, email, phone, gender_id, birthday, account_id)
    values (users_seq.nextval ,'Android', '17', 'Số 17,Phùng Khoang,Nguyễn Trãi,Hà Đông,Hà Nội', 'abc@gmail.com', '0963258741', 1, to_date('19940418', 'YYYYMMDD'), 3);

-- friend_request
insert into friend_request (id, sender_id, requested_time) values (friend_seq.nextval, 2, '20180101 13:27:23' || '#' || 2 || '#' || 1);
insert into friend_request (id, sender_id, requested_time) values (friend_seq.nextval, 3, '20180101 13:27:25' || '#' || 3 || '#' || 1);
insert into friend_request (id, sender_id, requested_time) values (friend_seq.nextval, 4, '20180101 13:25:02' || '#' || 4 || '#' || 2);
insert into friend_request (id, sender_id, requested_time) values (friend_seq.nextval, 2, '20180101 13:21:23' || '#' || 2 || '#' || 3);

--account_friend_request
insert into users_friend_request (user_id, friend_request_id) values (1, 1);
insert into users_friend_request (user_id, friend_request_id) values (1, 2);
insert into users_friend_request (user_id, friend_request_id) values (2, 3);
insert into users_friend_request (user_id, friend_request_id) values (3, 4);

--
insert into contact(id, user_id, added_time) values (contact_seq.nextval, 4, '20180225#1#4');
insert into contact(id, user_id, added_time) values (contact_seq.nextval, 4, '20180225#2#4');
insert into contact(id, user_id, added_time) values (contact_seq.nextval, 3, '20180225#1#3');
insert into contact(id, user_id, added_time) values (contact_seq.nextval, 3, '20180225#2#3');


insert into users_contact (user_id, contact_id) values (1, 1);
insert into users_contact (user_id, contact_id) values (2, 2);
insert into users_contact (user_id, contact_id) values (1, 3);
insert into users_contact (user_id, contact_id) values (2, 3);

commit;