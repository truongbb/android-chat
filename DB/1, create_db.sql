create table gender(
    id int primary key,
    gender nvarchar2(50) not null
);

create table chat_type(
    id int primary key,
    type nvarchar2(100) not null
);

create table message_type(
    id int primary key,
    type nvarchar2(200) not null
);

create table account(
    id int primary key,
    username nvarchar2(50) not null,
    password nvarchar2(16) not null,
    is_active number(1,0),
    last_online_time date
);

create table users(
    id int primary key,
    first_name nvarchar2(50) not null,
    last_name nvarchar2(50) not null,
    address nvarchar2(200) not null,
	email nvarchar2(300) not null,
    phone nvarchar2(15) not null,
    gender_id int not null constraint gender_fk references gender(id) on delete cascade,
    birthday date not null,
    account_id int not null constraint account_fk references account(id) on delete cascade
);

create table contact(
    id int primary key,
    user_id int not null constraint user_fk_for_contact_table references users(id) on delete cascade,
	added_time nvarchar2(200) not null
);

create table users_contact(
    user_id int not null constraint user_fk_for_contact references users(id) on delete cascade,
    contact_id int not null constraint contact_fk references contact(id) on delete cascade
);

create table friend_request(
    id int primary key,
    sender_id int not null constraint user_fk_for_friend_table references users(id) on delete cascade,
	requested_time nvarchar2(200) not null
);

create table users_friend_request(
    user_id int not null constraint user_fk_for_friend references users(id) on delete cascade,
    friend_request_id int not null constraint friend_fk references friend_request(id) on delete cascade
);

create table conversation(
    id int primary key,
    title nvarchar2(200),
    creator_id int not null constraint user_fk_conversation references users(id) on delete cascade,
    created_time date not null,
    updated_time date not null
);

create table participant(
    id int primary key,
    conversation_id int not null constraint conversation_fk references conversation(id) on delete cascade,
    user_id int not null constraint user_fk_for_participant references users(id) on delete cascade,
    chat_type_id int not null constraint chat_type_fk references chat_type(id) on delete cascade
);

create table message(
    id int primary key,
    conversation_id int not null constraint conversation_fk_for_message references conversation(id) on delete cascade,
    sender_id int not null constraint user_fk_for_message references users(id) on delete cascade,
    message_type_id int not null constraint message_type_fk_for_message references message_type(id) on delete cascade,
    message_content nvarchar2(2000),
    attachment_url nvarchar2(500),
    sent_time date not null
);

commit;